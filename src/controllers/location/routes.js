'use strict';

export default function routes($routeProvider) {
  $routeProvider.when('/location', { template: require('./location.html'), controller: 'LocationController' });
  $routeProvider.when('/streams', { template: require('./streams.html'), controller: 'StreamsController' });
  $routeProvider.when('/users', { template: require('./users.html'), controller: 'UsersController' });
}

routes.$inject = ['$routeProvider'];
