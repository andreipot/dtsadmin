'use strict';

export default
  ['AccessManager', 'DialogManager', 'LocationManager', 'ShellManager', 'UserModel',
  (AccessManager, DialogManager, LocationManager, ShellManager, UserModel) => {
  return function(authenticate) {
    return Promise.all([
      ShellManager.initialize(),
      DialogManager.initialize(),
      LocationManager.initialize(),
      AccessManager.initialize()
    ])
    .then(() => {
      if (authenticate && !AccessManager.isAccessValid) {
        LocationManager.saveLocation();
        LocationManager.load('login');
        return 'unauthorized';
      }
    });
  };
}];
