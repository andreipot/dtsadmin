'use strict';

export default class ProgressController {
  constructor($scope, $timeout, $element, DialogManager, close) {
    this.$$timeout = $timeout;
    this.$$element = $element;
    this.$$close = close;

    this._DialogManager = DialogManager;

    $scope.progress = 100;

    if (!DialogManager.hasJobs) {
      this._close();
      return;
    }

    function onProgress(progress) {
      $timeout(() => {
        $scope.progress = isNaN(progress) ? 100 : Math.round(progress * 100);
      })
    }

    DialogManager.jobProgressChanged.add(onProgress);

    DialogManager.jobEnded.addOnce(() => this._close());

    $scope.$on('destroy', () => {
      DialogManager.jobProgressChanged.remove(onProgress);
    })
  }

  _close() {
    this.$$timeout(() => {
      this.$$element.modal('hide');
      this.$$close(null, 500);
    }, 500);
  }
}

ProgressController.$inject = ['$scope', '$timeout', '$element', 'DialogManager', 'close'];
