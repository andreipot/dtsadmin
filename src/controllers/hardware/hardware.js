'use strict';

export default class HardwareController {
  constructor($scope, $timeout, $location, LegacyApi, DialogManager, ShellManager, UserModel) {
    var getData = function() {
      if (!UserModel.location) {
        return;
      }

      var job = DialogManager.startJob();

      var failure = function(e) {
        DialogManager.endJob(job);
        DialogManager.alert(e.message);
      };

      LegacyApi.location.getSeats().then(function(seats) {
        LegacyApi.hardware.getDevices().then(function(devices) {
          DialogManager.endJob(job);
          $timeout(function() {
            $scope.seats = seats;
            $scope.snap_devices = devices.filter(function(device) { return device.role === 1; });
            $scope.oms_devices = devices.filter(function(device) { return device.role === 2; });
          });
        }, failure);
      }, failure);
    };

    UserModel.locationChanged.add(getData);
    getData();

    $scope.$on('$destroy', () => UserModel.locationChanged.remove(getData));

    $scope.editSeat = function(seat) {
      DialogManager.showDialog(
        'SeatEditController',
        require('./seatedit.html'), {
          seat: jQuery.extend({}, seat)
        })
      .then(function(result) {
        if (result) {
          getData();
        }
      });
    };

    $scope.editDevice = function(device) {
      DialogManager.showDialog(
        'DeviceEditController',
        require('./deviceedit.html'), {
          device: jQuery.extend({}, device)
        })
      .then(function(result) {
        if (result) {
          getData();
        }
      });
    };

    $scope.addSeat = function() {
      DialogManager.showDialog(
        'SeatEditController',
        require('./seatedit.html'), {
          seat: {}
        })
      .then(function(result) {
        if (result) {
          getData();
        }
      });
    };

    $scope.getSeatName = function(seats, seat_token) {
      return (seats || [])
        .filter(function(seat) { return seat.token === seat_token; })
        .map(function(seat) { return seat.name; })[0];
    };

    $scope.getDeviceCount = function(devices, seat_token) {
      return (devices || [])
        .filter(function(device) { return device.seat_token === seat_token; }).length;
    };
  }
}

HardwareController.$inject = ['$scope', '$timeout', '$location', 'LegacyApi', 'DialogManager', 'ShellManager', 'UserModel'];
