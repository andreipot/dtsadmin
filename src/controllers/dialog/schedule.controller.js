'use strict';

import moment from 'moment';
import later from 'later';
later.date.localTime();

import $ from 'jquery';

export default class ScheduleController {
  constructor($scope, $element, close, schedule) {
    this.$scope = $scope;

    const settings = {
      showInputs: false,
      modalBackdrop: true,
      explicitMode: true,
      appendWidgetTo: $element
    };

    this._picker1 = $('#schedule-input-start', $element);
    this._picker1.timepicker(settings);
    this._picker1.on('changeTime.timepicker', e => this._onTime('start', e));

    this._picker2 = $('#schedule-input-end', $element);
    this._picker2.timepicker(settings);
    this._picker2.on('changeTime.timepicker', e => this._onTime('end', e));

    if (!schedule) {
      this.$scope.schedule = {
        start: '0 12 * * *',
        end: '0 16 * * *'
      };
    }
    else {
      this.$scope.schedule = {
        start: schedule.start,
        end: schedule.end
      };
    }

    if (this.$scope.schedule.start) {
      this._setTime(this._picker1, this.$scope.schedule.start);
    }

    if (this.$scope.schedule.end) {
      this._setTime(this._picker2,this.$scope.schedule.end);
    }

    $scope.save = () => {
      $element.modal('hide');
      close(this.$scope.schedule, 500);
    };

    $scope.cancel = () => {
      $element.modal('hide');
      close(schedule, 500);
    };

    $scope.clear = () => {
      $element.modal('hide');
      close(null, 500);
    };
  }

  _onTime(type, e) {
    const time = moment(`2027-04-04 ${e.time.hours}:${e.time.minutes} ${e.time.meridian}`, 'YYYY-MM-DD hh:mm A');
    this.$scope.schedule[type] = `${e.time.minutes} ${time.hour()} * * *`;
  }

  _setTime(picker, schedule) {
    const cron = later.parse.cron(schedule);
    const next = moment(later.schedule(cron).next(1)).format('hh:mm A');

    picker.timepicker('setTime', next);
  }
}

ScheduleController.$inject = ['$scope', '$element', 'close', 'schedule'];
