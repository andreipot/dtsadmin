'use strict';

export default class SlideController {
  constructor($scope, $timeout, $location, $routeParams, LegacyApi, DialogManager, ShellManager, UserModel) {
    var id = $routeParams.id;

    function getData() {
      if (!UserModel.location) {
        return;
      }

      $scope.initialized = false;

      var job = DialogManager.startJob();

      LegacyApi.advertisement.getPresentations().then(presentations => {
        LegacyApi.hardware.getDevices().then(devices => {
          DialogManager.endJob(job);
          $timeout(() => {
            $scope.initialized = true;
            $scope.devices = devices;
            $scope.presentation = presentations.filter(x => x.token === id)[0];
          });
        });
      })
      .catch(e => {
        DialogManager.endJob(job);
        DialogManager.alert(e.message);
      });
    }

    UserModel.locationChanged.add(getData);
    getData();

    $scope.$on('$destroy', () => UserModel.locationChanged.remove(getData));

    $scope.add = () => {
      DialogManager.showDialog(
        'SlideEditController',
        require('./slideedit.html'), {
          entity: {
            layout: '4',
            transition: 'fade',
            transition_delay: 30000
          }
        })
      .then(result => {
        if (result) {
          result.transition_delay = parseInt(result.transition_delay) || 0;
          $timeout(() => $scope.presentation.slides.push(result));
        }
      });
    };

    $scope.edit = slide => {
      let i = $scope.presentation.slides.filter(x => x.token === slide.token);

      DialogManager.showDialog(
        'SlideEditController',
        require('./slideedit.html'), {
          entity: slide
        })
      .then(result => {
        if (result === 'delete') {
          $timeout(() => $scope.presentation.slides = $scope.presentation.slides.filter((x, j) => i !== j));
        }
        else if (result) {
          result.transition_delay = parseInt(result.transition_delay) || 0;
          $timeout(() => $scope.presentation.slides[i] = result);
        }
      });
    };

    $scope.remove = slide => {
      DialogManager.confirm('Are you sure that you want to remove the slide?').then(() => {
        let i = $scope.presentation.slides.indexOf(slide);
        $timeout(() => $scope.presentation.slides = $scope.presentation.slides.filter((x, j) => i !== j));
      });
    };

    $scope.save = () => {
      let job = DialogManager.startJob();

      LegacyApi.advertisement.savePresentation(id, $scope.presentation)
      .then(() => {
        DialogManager.endJob(job);
      })
      .catch(e => {
        DialogManager.endJob(job);
        DialogManager.alert(e.message);
      });
    };

    $scope.delete = () => {
      DialogManager.confirm('Are you sure that you want to remove the presentation?').then(() => {
        let job = DialogManager.startJob();

        LegacyApi.advertisement.deletePresentation(id)
        .then(() => {
          DialogManager.endJob(job);
          $timeout(() => $location.path('/presentation'));
        })
        .catch(e => {
          DialogManager.endJob(job);
          DialogManager.alert(e.message);
        });
      });
    };

    $scope.onBackgroundSelect = ($files, slide) => {
      if ($files.length !== 1) {
        return;
      }

      let job = DialogManager.startJob();

      LegacyApi.upload.uploadMediaFile($files[0])
      .then(ticket => {
        DialogManager.endJob(job);
        $timeout(() => slide.background = ticket.token);
      })
      .catch(e => {
        DialogManager.endJob(job);
        DialogManager.alert(e.message);
      });
    };
  }
}

SlideController.$inject = ['$scope', '$timeout', '$location', '$routeParams', 'LegacyApi', 'DialogManager', 'ShellManager', 'UserModel'];
