'use strict';

import angular from 'angular';
import angular_route from 'angular-route';

import routes from './routes';
import BackgroundEditController from './backgroundedit';
import CustomizationController from './customization';
import ElementEditController from './elementedit';
import ScreensaverEditController from './screensaveredit';

export default angular.module('TouchAdmin.controllers.customization', [ angular_route ])
  .config(routes)
  .controller('BackgroundEditController', BackgroundEditController)
  .controller('CustomizationController', CustomizationController)
  .controller('ElementEditController', ElementEditController)
  .controller('ScreensaverEditController', ScreensaverEditController)
  .name;
