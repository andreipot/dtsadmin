'use strict';

import angular from 'angular';
import angular_route from 'angular-route';

import routes from './routes';
import MenuController from './menu';
import MenuSetController from './menu/set';
import MenuSetEditController from './menu/set/edit';
import MenuSetDuplicateController from './menu/set/duplicate';
import MenuPromoController from './menu/promo';
import MenuCategoryController from './menu/category';
import MenuCategoryEditController from './menu/category/edit';
import MenuItemController from './menu/item';
import MenuItemEditController from './menu/item/edit';
import MenuModifiersController from './modifier';
import MenuModifierCategoryController from './modifier/category';
import MenuModifierCategoryEditController from './modifier/category/edit';
import MenuModifierItemEditController from './modifier/item/edit';
import MenuAllergenController from './allergen';
import MenuAllergenEditController from './allergen/edit';
import MenuSpreadsheetController from './spreadsheet';

export default angular.module('TouchAdmin.controllers.menu', [ angular_route ])
  .config(routes)
  .controller('MenuController', MenuController)
  .controller('MenuSetController', MenuSetController)
  .controller('MenuSetEditController', MenuSetEditController)
  .controller('MenuSetDuplicateController', MenuSetDuplicateController)
  .controller('MenuPromoController', MenuPromoController)
  .controller('MenuCategoryController', MenuCategoryController)
  .controller('MenuCategoryEditController', MenuCategoryEditController)
  .controller('MenuItemController', MenuItemController)
  .controller('MenuItemEditController', MenuItemEditController)
  .controller('MenuModifiersController', MenuModifiersController)
  .controller('MenuModifierCategoryController', MenuModifierCategoryController)
  .controller('MenuModifierCategoryEditController', MenuModifierCategoryEditController)
  .controller('MenuModifierItemEditController', MenuModifierItemEditController)
  .controller('MenuAllergenController', MenuAllergenController)
  .controller('MenuAllergenEditController', MenuAllergenEditController)
  .controller('MenuSpreadsheetController', MenuSpreadsheetController)
  .name;
