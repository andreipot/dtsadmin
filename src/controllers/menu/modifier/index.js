import Controller from '../../controller';

export default class MenuModifiersController extends Controller {
  constructor($scope, $timeout, $location, DialogManager, Logger, MenuManager, UserModel) {
    super($scope, $timeout, DialogManager, Logger, UserModel);

    this.$location = $location;
    this.MenuManager = MenuManager;

    this._defineMethod('createModifier');
    this._defineMethod('getItems');
    this._defineMethod('editModifier');
  }

  async getData() {
    const modifiers = await this.MenuManager.loadModifiers();

    this.$timeout(() => {
      this.$scope.modifiers = modifiers;
    });
  }

  async createModifier() {
    const result = await this.DialogManager.showDialog(
      'MenuModifierCategoryEditController',
      require('./category/edit/template.html'), {
        data: {
          modifier: {
            limit: 1
          }
        }
      }
    );

    if (result) {
      this.$timeout(() => this.$location.path('/menu/modifier/' + result.token));
    }
  }

  getItems(modifier) {
    return (modifier.items || [])
    .filter(item => item.image)
    .filter((item, i) => i < 6);
  }

  editModifier(modifier) {
    this.$location.path('/menu/modifier/' + modifier.token);
  }
}

MenuModifiersController.$inject = ['$scope', '$timeout', '$location', 'DialogManager', 'Logger', 'MenuManager', 'UserModel'];
