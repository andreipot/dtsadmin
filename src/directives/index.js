'use strict';

import angular from 'angular';

import components from './components';
import modal from './modal';
import navigation from './navigation';

export default angular.module('TouchAdmin.directives', [
    components,
    modal,
    navigation
  ])
  .name;
