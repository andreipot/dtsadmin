import signals from 'signals';

export default class AbstractModel {
  constructor(StorageProvider) {
    this._StorageProvider = StorageProvider;
    this._properties = {};
  }

  _defineProperty(name, storeName, defaultValue, restoreFunction) {
    var self = this,
        property = this._properties[name] = { name: '_' + name };

    if (storeName) {
      property.store = this._StorageProvider.getStorage(storeName);
    }

    if (restoreFunction) {
      property.restore = restoreFunction;
    }

    this[name + 'Changed'] = property.signal = new signals.Signal();
    this[property.name] = property.defaultValue = defaultValue;

    Object.defineProperty(this, name, {
      get: function() {
        return self[property.name];
      },
      set: function(value) {
        // if (value === self[property.name]) {
        //   return;
        // }

        self[property.name] = value;

        if (property.store) {
          property.store.write(value).then(() => {
            property.signal.dispatch(value);
          });
        }
        else {
          property.signal.dispatch(value);
        }
      }
    });

    if (property.store) {
      let value = property.store.readSync();

      value = value || property.defaultValue;

      if (property.restore) {
        value = property.restore(value);
      }

      this[property.name] = value;
    }
  }

  save(properties) {
    return Promise.all((properties || Object.keys(this._properties))
      .filter(key => this._properties[key].store)
      .map(key => this._properties[key].store.write(this[key])));
  }

  clear() {
    return Promise.all(Object.keys(this._properties)
      .filter(key => this._properties[key].store)
      .map(key => this._properties[key].store.clear()));
  }

  _propertyChanged(name) {
    var property = this._properties[name],
        value = this[property.name];

    if (property.store) {
      property.store.write(value).then(() => {
        property.signal.dispatch(value);
      });
    }
    else {
      property.signal.dispatch(value);
    }
  }
};
