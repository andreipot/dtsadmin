import Controller from '../../controller';

export default class MenuController extends Controller {
  constructor($scope, $timeout, $location, AssetManager, DtsApi, DialogManager, Logger, MenuManager, UserModel) {
    super($scope, $timeout, DialogManager, Logger, UserModel);

    this.$location = $location;
    this.AssetManager = AssetManager;
    this.DtsApi = DtsApi;
    this.MenuManager = MenuManager;

    this.$scope.can_import_simplicity = false;

    this._defineMethod('onMenuDrag');
    this._defineMethod('onMenuDragComplete');
    this._defineMethod('addMenu');
    this._defineMethod('addMenuLoader');
    this._defineMethod('importFromSimplicity');
    this._defineMethod('subscribe');
    this._defineMethod('duplicateMenu');
  }

  async getData() {
    const menus = await this.MenuManager.loadMenus();

    this.$timeout(() => {
      this.$scope.menus = menus;
    });
  }

  async addMenu() {
    const menu = await this.DialogManager.showDialog(
      'MenuSetEditController',
      require('./set/edit/template.html'), {
        data: {
          menu: {}
        }
      }
    );

    if (menu) {
      this.$timeout(() => this.$location.path('/menu/set/' + menu.token));
    }
  }

  async addMenuLoader() {
    this.$timeout(() => this.$location.path('/menu/loader'));
  }

  async importFromSimplicity() {
    const confirmed = await this.DialogManager.confirm('Are you sure that you want to import a Simplicity POS menu?');

    if (!confirmed) {
      return;
    }
  }

  async subscribe(token) {
    const confirmed = await this.DialogManager.confirm('Are you sure that you want to add the menu to the current location?');

    if (!confirmed) {
      return;
    }
  }

  async duplicateMenu() {
    const menu = await this.DialogManager.showDialog(
      'MenuSetDuplicateController',
      require('./set/duplicate/template.html'), {
        data: {}
      }
    );

    if (!menu) {
      return;
    }

    await this.getData();
  }
}

MenuController.$inject = ['$scope', '$timeout', '$location', 'AssetManager', 'DtsApi', 'DialogManager', 'Logger', 'MenuManager', 'UserModel'];
