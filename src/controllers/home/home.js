const pkg = require('../../../package.json');


export default class HomeController {
  constructor($scope) {
    $scope.version = pkg.version;
  }
}

HomeController.$inject = ['$scope'];
