'use strict';

export default class ConfirmController {
  constructor($scope, $element, close, data) {
    $scope.data = data;

    $scope.yes = () => {
      $element.modal('hide');
      close(true, 500);
    };

    $scope.no = () => {
      $element.modal('hide');
      close(false, 500);
    };
  }
}

ConfirmController.$inject = ['$scope', '$element', 'close', 'data'];
