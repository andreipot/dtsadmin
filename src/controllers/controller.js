export default class Controller {
  constructor($scope, $timeout, DialogManager, Logger, UserModel) {
    this.$scope = $scope;
    this.$timeout = $timeout;
    this.DialogManager = DialogManager;
    this.Logger = Logger;
    this.UserModel = UserModel;

    const onDone = () => {};
    const onError = e => {
      this.Logger.warn('Controller data error.', e);
      this.DialogManager.alert(e.message || 'Oops! My bits are fiddled. Please try again later.');
    };

    const getData = () => {
      const location = this.UserModel.location;

      if (!location || !this.getData) {
        return;
      }

      let res = this.getData();

      if (!res || !res.then) {
        return;
      }

      var job;

      const timer = this.$timeout(() => {
        job = this.DialogManager.startJob();
      }, 500);

      res.then(onDone)
        .catch(onError)
        .then(() => {
          if (job) {
            this.DialogManager.endJob(job);
            job = null;
          }
          else {
            this.$timeout.cancel(timer);
          }
        });
    };

    if (this.UserModel) {
      this.UserModel.locationChanged.add(getData);
      this.$scope.$on('$destroy', () => {
        this.UserModel.locationChanged.remove(getData);
      });
    }
    $timeout(getData);
  }

  _defineMethod(name) {
    this.$scope[name] = (...args) => {
      try {
        let res = this[name].apply(this, args);

        if (!res || !res.then) {
          return res;
        }

        Promise.resolve(res)
        .then(() => {})
        .catch(e => {
          this.Logger.error('Controller error.', e);
        });
      }
      catch (e) {
        this.Logger.error('Controller error.', e);
      }
    };
  }

  _sort(a, b) {
    if (a.position < b.position) {
      return -1;
    }

    if (a.position > b.position) {
      return 1;
    }

    return 0;
  }
};
