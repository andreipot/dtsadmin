'use strict';

export default class MenuPromoController {
  constructor($scope, $timeout, $location, $routeParams, LegacyApi, DialogManager, UserModel) {
    var id = $routeParams.id;

    var getData = function() {
      if (!UserModel.location) {
        return;
      }

      var job = DialogManager.startJob();

      var failure = function(e) {
        DialogManager.endJob(job);
        DialogManager.alert(e.message);
      };

      LegacyApi.menu.getMenuPromos(id).then(function(promos) {
        DialogManager.endJob(job);
        $timeout(function() {
          $scope.promos = promos;
          $scope.is_editable = true;
        });
      }, failure);
    };

    UserModel.locationChanged.add(getData);
    getData();

    $scope.$on('$destroy', () => UserModel.locationChanged.remove(getData));

    $scope.onPromoDrag = function(element, index) {
      var promo = element.promo;
      promo.index = index;
    };

    $scope.onPromoDragComplete = function() {
      if (!$scope.is_editable) {
        return;
      }

      var tokens = $scope.promos.sort(function(a, b) {
        if (a.index > b.index) {
          return 1;
        }
        else if (a.index < b.index) {
          return -1;
        }

        return 0;
      }).map(function(promo) {
        return promo.token;
      });

      var job = DialogManager.startJob();

      LegacyApi.menu.saveMenuPromoOrder(tokens, id).then(function() {
        DialogManager.endJob(job);
      }, function(e) {
        DialogManager.endJob(job);
        DialogManager.alert(e.message);
      });
    };

    $scope.deletePromo = function(promo) {
      DialogManager.confirm('Are you sure that you want to remove the promo?').then(function() {
        var job = DialogManager.startJob();

        LegacyApi.menu.removePromo(promo.token).then(function() {
          DialogManager.endJob(job);
          $timeout(function() {
            $scope.promos = $scope.promos.filter(function(p) {
              return p !== promo;
            });
          });
        }, function(e) {
          DialogManager.endJob(job);
          DialogManager.alert(e.message);
        });
      });
    };
  }
}

MenuPromoController.$inject = ['$scope', '$timeout', '$location', '$routeParams', 'LegacyApi', 'DialogManager', 'UserModel'];
