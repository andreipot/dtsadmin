import AbstractManager from './abstract';
import signals from 'signals';

export default class AccessManager extends AbstractManager {
  constructor(DtsApi, DtsApiManager, Applications, StorageProvider, Logger) {
    super(Logger);

    this._Api = DtsApi;
    this._Applications = Applications;
    this._Manager = DtsApiManager;

    this._locationStore = StorageProvider.getStorage('current_location');

    this.accessDenied = new signals.Signal();
  }

  initialize() {
    return super.initialize().then(() => {
      let application = this._Applications.main;
      this._Manager.tokenExpiring.add((token, application) => this._onTokenExpiring(token, application));

      let token = this._Manager.getToken(application);

      if (token && this._Manager.isAccessValid(application)) {
        this._Logger.debug(`Restoring authentication for ${application.security}`);
        this._Api.authorize(application, token);
      }
    });
  }

  login(username, password) {
    this._Logger.debug('Authorizing user...');

    let application = this._Applications.main;

    return this._Api.api.request('oauth2', 'grantToken', {
      grant_type: 'password',
      client_id: application.client_id,
      username: username,
      password: password
    }).then(token => {
      this._Logger.debug('User has been authorized successfully.');
      this._Manager.saveToken(application, token);

      return this._Api.authorize(application, token);
    })
    .catch(e => {
      this._Manager.removeToken(application);

      if (e.error) {
        this._Logger.debug(`Unable to authorize user: ${e.error}`);
        return Promise.reject(e.error);
      }

      this._Logger.debug(`Unable to authorize user: ${e.message || e}`);
      return Promise.reject(e);
    });
  }

  reauthenticate(location) {
    if (location) {
      this._locationStore.write(location);
      this._Logger.debug(`Requesting new access token for location ${location}`);
    }
    else {
      this._Logger.debug(`Requesting new access token`);
    }

    let application = this._Applications.main;
    let token = this._Manager.getToken(application);

    return this._Api.api.request('oauth2', 'grantToken', {
      grant_type: 'refresh_token',
      client_id: application.client_id,
      refresh_token: token.refresh_token,
      location_id: location
    }).then(token => {
      this._Manager.saveToken(application, token);
      this._Api.authorize(application, token);
      this._Logger.debug(`New access token received: ${JSON.stringify(this.activeCredentials)}`);
    });
  }

  logout() {
    this._Logger.debug('Deauthorizing user...');

    let application = this._Applications.main;

    this._Manager.removeToken(application);
    this._Api.deauthorize(application);

    this._locationStore.clear();

    return Promise.resolve();
  }

  get isAccessValid() {
    let application = this._Applications.main;
    return this._Manager.isAccessValid(application);
  }

  get activeCredentials() {
    let application = this._Applications.main;
    let token = this._Manager.getToken(application);

    return token ? this._Manager.getTokenMetadata(token) : null;
  }

  _onTokenExpiring(token, application) {
    this._Logger.debug('Access token is expiring. Trying to refresh...');

    let location = this._locationStore.read();

    this.reauthenticate(location).catch(() => this.accessDenied.dispatch());
  }
};
