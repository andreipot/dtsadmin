'use strict';
import moment from 'moment';
import Controller from '../controller';

Object.defineProperty(Array.prototype, 'group', {
    enumerable: false,
    value: function (key) {
        var map = {};
        this.forEach(function (e) {
            var k = key(e);
            map[k] = map[k] || [];
            map[k].push(e);
        });
        return Object.keys(map).map(function (k) {
            return {key: k, data: map[k]};
        });
    }
});

export default class MediaGalleryController extends Controller {
    constructor($scope, $timeout, $location, $routeParams,AssetManager, DialogManager, Logger, DtsApi, UserModel, MediaManager) {
        super($scope, $timeout, DialogManager, Logger, UserModel,MediaManager);

        this.$location = $location;
        this.$routeParams = $routeParams;
        this.AssetManager = AssetManager;
        this.MediaManager = MediaManager;
        this.UserModel = UserModel;
        this.DtsApi = DtsApi;
        this.$scope = $scope;

        $scope.selectedMedias = [];

        this._defineMethod('deleteMedias');
        this._defineMethod('deleteMedia');

        $scope.uploadFiles= function () {
            DialogManager.showDialog(
                'MediaGalleryMultiUploadController',
                require('./multiupload/template.html'), {
                    data: {
                    }
                }
            ).then(getData);
        };

        $scope.select = function (media) {
            var index = $scope.selectedMedias.indexOf(media.token);
            if (index == -1 ){
                $scope.selectedMedias.push(media.token);
            }else{
                $scope.selectedMedias.splice(index, 1);
            }
        }
        $scope.isSelected = function (media){
            var index = $scope.selectedMedias.indexOf(media.token);
            if (index == -1 ){
                return '';
            }else{
                return 'selected';
            }
        }

        var getData = function() {
            //check user medias
            const existing = UserModel.medias;
            if (existing && existing.length > 0) {
                return $timeout(() => $scope.medias = existing);
            }
            //request Media via DTS
            $timeout( () => {
                DtsApi.api.request('media', 'mediaList')
                    .then(medias => $timeout(() => {

                        $scope.medias = UserModel.medias = medias
                            /*.filter(m => m.mime.startsWith('/image'))*/
                            .sort((m1, m2) => {
                                m1 = moment(m1.created);
                                m2 = moment(m2.created);
                                if (m1.isAfter(m2)) {
                                    return -1;
                                }
                                if (m2.isAfter(m1)) {
                                    return 1;
                                }
                                return 0;
                            });

                        $scope.medias = $scope.medias.group(function(item){
                            if(item.category){
                                return item.category;
                            }else{
                                return 'Not Categorized';
                            }
                        })

                        console.log($scope.medias);
                    }));
            });
        };
        this.$scope.getData = getData;
        //load gallery
        getData();
    }

    async deleteMedias(tokens) {
        const confirmed = await this.DialogManager.confirm('Are you sure that you want to delete the selected medias?');

        if (!confirmed) {
            return;
        }

        const job = this.DialogManager.startJob();

        try {
            //delete medias
            await this.MediaManager.deleteMedias(tokens);
            this.$scope.getData();
        }
        finally {
            this.DialogManager.endJob(job);
        }
    }
    async deleteMedia(token) {
        const confirmed = await this.DialogManager.confirm('Are you sure that you want to delete this media?');

        if (!confirmed) {
            return;
        }

        const job = this.DialogManager.startJob();

        try {
            //delete medias
            //await this.DtsApi.api.request('media','mediaDelete', {token: token});
            await this.MediaManager.deleteMedia({token: token});
            this.$scope.getData();
        }
        finally {
            this.DialogManager.endJob(job);
        }
    }
}

MediaGalleryController.$inject = ['$scope', '$timeout', '$location', '$routeParams','AssetManager', 'DialogManager', 'Logger', 'DtsApi', 'UserModel' ,'MediaManager'];
