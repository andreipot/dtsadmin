import Controller from '../../controller';

export default class DeploymentController extends Controller {
  constructor($scope, $timeout, $location, DeploymentManager, DialogManager, Logger, UserModel) {
    super($scope, $timeout, DialogManager, Logger, UserModel);

    this.$location = $location;
    this.DeploymentManager = DeploymentManager;
  }

  async getData() {
    const deployments = await this.DeploymentManager.loadDeployments();
    const activeDeployments = await Promise.all(deployments
      .filter(data => data.status === 'active')
      .map(data => {
        return this.DeploymentManager.loadDeployment(data.token)
        .then(content => {
          return {
            data: data,
            content: content
          };
        });
      }));

    this.$timeout(() => {
      this.$scope.activeDeployments = activeDeployments;
    });
  }
}

DeploymentController.$inject = ['$scope', '$timeout', '$location', 'DeploymentManager', 'DialogManager', 'Logger', 'UserModel'];
