import Controller from '../controller';

export default class ModifiersEditController extends Controller {
  constructor($scope, $timeout, DialogManager, Logger, MenuManager, UserModel, data, close) {
    super($scope, $timeout, DialogManager, Logger, UserModel);

    this.MenuManager = MenuManager;
    this.$scope.modifiers = data.modifiers;
    this.$scope.item = data.item;
    this.close = close;

    this.$scope.allModifiers = [];
    this.$scope.selectedModifiers = (this.$scope.item.modifiers || [])
      .map((token, i) => {
        return {
          modifier: (this.$scope.modifiers || []).filter(x => x).filter(x => x.token === token)[0],
          position: i
        };
      })
      .filter(x => x.modifier)

    this._refreshList();

    this._defineMethod('add');
    this._defineMethod('remove');
    this._defineMethod('save');
    this._defineMethod('cancel');

    this._defineMethod('onElementDrag');
    this._defineMethod('onElementDragComplete');
  }

  async save() {
    this.$scope.item.modifiers = this.$scope.selectedModifiers.map(x => x.modifier.token);

    this.DialogManager.closeDialog(this.close, this.$scope.item);
  }

  cancel() {
    this.DialogManager.closeDialog(this.close);
  };

  toggle(item) {
    item.selected = !item.selected;
  }

  add() {
    const token = this.$scope.modifier,
          modifier = this.$scope.modifiers.filter(x => x.token === token)[0];

    if (modifier) {
      this.$scope.selectedModifiers.push({
        modifier: modifier,
        position: this.$scope.selectedModifiers.length
      });
      this._refreshList();
    }
  }

  remove(entity) {
    this.$scope.selectedModifiers = this.$scope.selectedModifiers.filter(x => x !== entity);
    this._refreshList();
  }

  onElementDrag(el, index) {
    el.entity.position = index;
  };

  onElementDragComplete() {
    this.$timeout(() => this._refreshList());
  }

  _refreshList() {
    this.$scope.allModifiers = this.$scope.modifiers.filter(modifier => {
      return this.$scope.selectedModifiers.map(x => x.modifier.token).indexOf(modifier.token) === -1;
    });
    this.$scope.modifier = (this.$scope.allModifiers[0] || {}).token;

    this.$scope.selectedModifiers = this.$scope.selectedModifiers
      .sort(this._sort)
      .map((entity, i) => {
        entity.position = i;
        return entity;
      });
  }
}

ModifiersEditController.$inject = ['$scope', '$timeout', 'DialogManager', 'Logger', 'MenuManager', 'UserModel', 'data', 'close'];
