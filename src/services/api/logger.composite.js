import Logger from './logger';

export default class CompositeLogger extends Logger {
  constructor(loggers) {
    super();
    this.loggers = loggers;
  }

  debug(msg) {
    this.loggers.forEach(logger => logger.debug(msg));
  }

  info(msg) {
    this.loggers.forEach(logger => logger.info(msg));
  }

  warn(msg, e) {
    this.loggers.forEach(logger => logger.warn(msg, e));
  }

  error(msg, e) {
    this.loggers.forEach(logger => logger.error(msg, e));
  }

  fatal(msg, e) {
    this.loggers.forEach(logger => logger.fatal(msg, e));
  }

  setUser(location, device) {
    this.loggers.forEach(logger => logger.setUser(location, device));
  }
}
