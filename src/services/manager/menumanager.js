import AbstractManager from './abstract';
import ObjectId from './../model/objectid';

export default class MenuManager extends AbstractManager {
  constructor(DtsApi, Logger, MenuModel) {
    super(Logger);

    this._DtsApi = DtsApi;
    this._MenuModel = MenuModel;
  }

  async initialize() {
    await super.initialize();
  }

  async reset() {
    this.model.menus = {};
    this.model.streams = [];
  }

  get model() {
    return this._MenuModel;
  }

  generateID() {
    return (new ObjectId()).toString();
  }

  //-----------------------------------------------
  //    Menu methods
  //-----------------------------------------------

  async loadMenus() {
    return await this._DtsApi.api.request('content', 'contentMenuList');
  }

  async loadMenu(token) {
    const menu = this.model.menus[token] = this.model.menus[token] ||
      (this.model.menus[token] = await this._DtsApi.api.request('content', 'contentMenuRead', { token: token }));

    return menu;
  }



  async createMenu(menu) {
    this._Logger.debug(`Creating menu...`);

    if (menu.items) {
      await this._syncEntries(menu.items, []);
    }

    try {
      let result = await this._DtsApi.api.request('content', 'contentMenuCreate', { body: menu });
      this.model.menus[result.token] = result;

      return result;
    }
    catch (e) {
      this._Logger.warn(`Unable to create menu.`, e);
      throw e;
    }
  }

  async updateMenu(menu) {
    this._Logger.debug(`Updating menu ${menu.token}...`);

    if (menu.items) {
      await this._syncEntries(menu.items, []);
    }

    try {
      let result = await this._DtsApi.api.request('content', 'contentMenuUpdate', { body: menu, token: menu.token });
      this.model.menus[result.token] = result;

      return result;
    }
    catch (e) {
      this._Logger.warn(`Unable to update menu ${menu.token}.`, e);
      throw e;
    }
  }

  async duplicateMenu(menu) {
    this._Logger.debug(`Duplicating menu ${menu.token}...`);

    let copy = $.extend(true, {}, menu);

    delete copy.token;

    return copy;
  }

  async deleteMenu(menu) {
    this._Logger.debug(`Deleting menu ${menu.token}...`);

    try {
      let items = (menu.items || []).map(x => this.deleteMenuItem(menu, x));
      let childs = (menu.categories || []).map(x => this.deleteMenuCategory(menu, x));

      await Promise.all(childs.concat(items));
    }
    catch (e) {
      this._Logger.warn(`Unable to delete childs of menu ${menu.token}.`, e);
      throw e;
    }

    try {
      await this._DtsApi.api.request('content', 'contentMenuDelete', { token: menu.token });
      delete this.model.menus[menu.token];
    }
    catch (e) {
      this._Logger.warn(`Unable to delete menu ${menu.token}.`, e);
      throw e;
    }
  }

  //-----------------------------------------------
  //    Streams methods
  //-----------------------------------------------

  async loadStreams() {
    const streams = this.model.streams.length > 0 ? this.model.streams :
      (this.model.streams = await this._DtsApi.api.request('waiter', 'waiterStreamList'));

    if (!streams) {
      throw new Error('Streams not found');
    }

    return streams;
  }

  //-----------------------------------------------
  //    Menu category methods
  //-----------------------------------------------

  async createMenuCategory(menu, category) {
    this._Logger.debug(`Creating menu category from menu ${menu.token}...`);

    try {
      let result = await this._DtsApi.api.request('content', 'contentMenuCategoryCreate', {
        body: category,
        menu : menu.token
      });

      menu.categories.push(result);

      return result;
    }
    catch (e) {
      this._Logger.warn(`Unable to create menu category from menu ${menu.token}.`, e);
      throw e;
    }
  }

  async updateMenuCategory(menu, category) {
    this._Logger.debug(`Updating menu category ${category.token} from menu ${menu.token}...`);

    try {
      let result = await this._DtsApi.api.request('content', 'contentMenuCategoryUpdate', {
        body: category,
        menu : menu.token,
        token: category.token
      });

      let i = menu.categories.reduce((res, item, i) => {
        if (item.token === result.token) {
          res = i;
        }

        return res;
      }, -1);

      if (i !== -1) {
        menu.categories[i] = result;
      }

      return result;
    }
    catch (e) {
      this._Logger.warn(`Unable to update menu category ${category.token} from menu ${menu.token}.`, e);
      throw e;
    }
  }

  async deleteMenuCategory(menu, category) {
    this._Logger.debug(`Deleting menu category ${category.token} from menu ${menu.token}...`);

    try {
      let items = menu.items.filter(x => x.parent === category.token).map(x => this.deleteMenuItem(menu, x));
      let childs = menu.categories.filter(x => x.parent === category.token).map(x => this.deleteMenuCategory(menu, x));

      await Promise.all(childs.concat(items));
    }
    catch (e) {
      this._Logger.warn(`Unable to delete childs of menu category ${category.token} from menu ${menu.token}.`, e);
      throw e;
    }

    try {
      await this._DtsApi.api.request('content', 'contentMenuCategoryDelete', { menu: menu.token, token: category.token });
      this.model.menus[menu.token].categories = this.model.menus[menu.token].categories.filter(i => i.token !== category.token);
    }
    catch (e) {
      this._Logger.warn(`Unable to delete menu category ${category.token} from menu ${menu.token}.`, e);
      throw e;
    }
  }

  //-----------------------------------------------
  //    Menu item methods
  //-----------------------------------------------

  async createMenuItem(menu, item) {
    this._Logger.debug(`Creating menu item from menu ${menu.token}...`);

    await this._syncEntries([ item ], []);

    try {
      let result = await this._DtsApi.api.request('content', 'contentMenuItemCreate', {
        body: item,
        menu : menu.token
      });

      menu.items.push(result);

      return result;
    }
    catch (e) {
      this._Logger.warn(`Unable to create new menu item from menu ${menu.token}.`, e);
      throw e;
    }
  }

  async updateMenuItem(menu, item) {
    const menuitem = menu.items.filter(x => x.token === item.token)[0];

    if (!menuitem) {
      throw new Error(`Menu item ${item.token} not found`);
    }

    this._Logger.debug(`Updating menu item ${item.token} from menu ${menu.token}...`);

    await this._syncEntries([ item ], [ menuitem ]);

    try {
      let result = await this._DtsApi.api.request('content', 'contentMenuItemUpdate', {
        menu: menu.token,
        token: item.token,
        body: item
      });

      let i = menu.items.reduce((res, item, i) => {
        if (item.token === result.token) {
          res = i;
        }

        return res;
      }, -1);

      if (i !== -1) {
        menu.items[i] = result;
      }

      return result;
    }
    catch (e) {
      this._Logger.warn(`Unable to update menu item ${item.token} from menu ${menu.token}.`, e);
      throw e;
    }
  }

  async deleteMenuItem(menu, item) {
    this._Logger.debug(`Deleting menu item ${item.token} from menu ${menu.token}...`);

    try {
      await this._DtsApi.api.request('content', 'contentMenuItemDelete', { menu: menu.token, token: item.token });
      this.model.menus[menu.token].items = this.model.menus[menu.token].items.filter(i => i.token !== item.token);
    }
    catch (e) {
      this._Logger.warn(`Unable to delete menu ${item.token}.`, e);
      throw e;
    }
  }

  //-----------------------------------------------
  //    Allergen methods
  //-----------------------------------------------

  async loadAllergens() {
    if (this.model.allergens && this.model.allergens.length > 0) {
      return this.model.allergens;
    }

    return this.model.allergens = await this._DtsApi.api.request('content', 'contentAllergenList');
  }

  async createAllergen(allergen) {
    this._Logger.debug(`Creating allergen...`);

    try {
      let result = await this._DtsApi.api.request('content', 'contentAllergenCreate', { body: allergen });
      this.model.allergens.push(result);

      return result;
    }
    catch (e) {
      this._Logger.warn(`Unable to create allergen.`, e);
      throw e;
    }
  }

  async updateAllergen(allergen) {
    this._Logger.debug(`Updating allergen ${allergen.token}...`);

    try {
      let result = await this._DtsApi.api.request('content', 'contentAllergenUpdate', { body: allergen, token: allergen.token });

      let i = this.model.allergens.reduce((res, item, i) => {
        if (item.token === result.token) {
          res = i;
        }

        return res;
      }, -1);

      if (i !== -1) {
        this.model.allergens[i] = result;
      }

      return result;
    }
    catch (e) {
      this._Logger.warn(`Unable to update allergen ${allergen.token}.`, e);
      throw e;
    }
  }

  async deleteAllergen(allergen) {
    this._Logger.debug(`Deleting allergen ${allergen.token}...`);

    try {
      await this._DtsApi.api.request('content', 'contentAllergenDelete', { token: allergen.token });

      let i = this.model.allergens.reduce((res, item, i) => {
        if (item.token === allergen.token) {
          res = i;
        }

        return res;
      }, -1);

      if (i !== -1) {
        this.model.allergens.splice(i, 1);
      }
    }
    catch (e) {
      this._Logger.warn(`Unable to delete allergen ${allergen.token}.`, e);
      throw e;
    }
  }

  //-----------------------------------------------
  //    Modifier methods
  //-----------------------------------------------

  async loadModifiers() {
    if (this.model.modifiers && this.model.modifiers.length > 0) {
      return this.model.modifiers;
    }

    return this.model.modifiers = await this._DtsApi.api.request('content', 'contentModifierList');
  }

  async createModifier(modifier) {
    this._Logger.debug(`Creating modifier...`);

    if (modifier.items) {
      await this._syncEntries(modifier.items, []);
    }

    try {
      let result = await this._DtsApi.api.request('content', 'contentModifierCreate', { body: modifier });

      this.model.modifiers.push(result);

      return result;
    }
    catch (e) {
      this._Logger.warn(`Unable to create modifier.`, e);
      throw e;
    }
  }

  async updateModifier(modifier) {
    this._Logger.debug(`Updating modifier ${modifier.token}...`);

    if (modifier.items) {
      const oldmodifier = this.model.modifiers.filter(x => x.token === modifier.token)[0];

      if (!oldmodifier) {
        throw new Error('Modifier not found: ' + modifier.token);
      }

      await this._syncEntries(modifier.items, oldmodifier.items);
    }

    try {
      let result = await this._DtsApi.api.request('content', 'contentModifierUpdate', { body: modifier, token: modifier.token });

      let i = this.model.modifiers.reduce((res, item, i) => {
        if (item.token === result.token) {
          res = i;
        }

        return res;
      }, -1);

      if (i !== -1) {
        this.model.modifiers[i] = result;
      }
      else {
        this._Logger.warn(`Modifier ${result.token} not found in cache.`);
      }

      return result;
    }
    catch (e) {
      this._Logger.warn(`Unable to update modifier ${modifier.token}.`, e);
      throw e;
    }
  }

  async deleteModifier(modifier) {
    this._Logger.debug(`Deleting modifier ${modifier.token}...`);

    try {
      await this._DtsApi.api.request('content', 'contentModifierDelete', { token: modifier.token });

      let i = this.model.modifiers.reduce((res, item, i) => {
        if (item.token === modifier.token) {
          res = i;
        }

        return res;
      }, -1);

      if (i !== -1) {
        this.model.modifiers.splice(i, 1);
      }
      else {
        this._Logger.warn(`Modifier ${modifier.token} not found in cache.`);
      }
    }
    catch (e) {
      this._Logger.warn(`Unable to delete modifier ${modifier.token}.`, e);
      throw e;
    }
  }

  //-----------------------------------------------
  //    Helper methods
  //-----------------------------------------------

  async _syncEntries(newitems, olditems) {
    const pending = [];

    const sync = (item, olditem, field) => {
      if (!item[field]) {
        return;
      }

      if (!item[field].token) {
        this._Logger.debug(`Creating new entry for item ${item.token}: ` + item[field].price + '...');

        pending.push({
          item: item,
          field: field,
          order: {
            name: item[field].name,
            price: item[field].price || 0
          }
        });
      }
      else if (olditem && olditem[field]){
        if (Math.abs(olditem[field].price - item[field].price) >= 0.01) {
          this._Logger.debug(`Switching entry for item ${item.token}: ` + olditem[field].price + '->' + item[field].price + '...');

          pending.push({
            item: item,
            field: field,
            order: {
              name: item[field].name,
              price: item[field].price || 0
            }
          });
        }
      }
    }

    newitems.forEach(item => {
      const olditem = item.token ? olditems.filter(x => x.token === item.token)[0] : null;

      if (!item.order) {
        item.order = { name: item.title, price: 0};
      }

      sync(item, olditem, 'order');
      sync(item, olditem, 'order_refill');
    });

    if (pending.length > 0) {
      try {
        let entries = await this._DtsApi.api.request('asset', 'assetEntryCreate', { body: pending.map(x => x.order) });
        entries.forEach((entry, i) => {
          let old = pending[i].item[pending[i].field];

          if (old && old.stream) {
            entry.stream = old.stream;
          }

          pending[i].item[pending[i].field] = entry;
        });
      }
      catch (e) {
        this._Logger.warn(`Unable to create new entries.`, e);
        throw e;
      }
    }
  }
};
