# SNAP Admin

## Initial configuration

``npm run init``

## Setup

## Development

`npm run dev`, then go to `http://localhost:8081/login.html`

## Deployment

Run `npm run stage` or `npm run production` to deploy to S3, `npm run release` - to deploy to production and bump. Always commit all your changes before deploying.