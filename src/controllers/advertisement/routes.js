'use strict';

export default function routes($routeProvider) {
  $routeProvider.when('/advertisement', { template: require('./advertisement.html'), controller: 'AdvertisementController' });
}

routes.$inject = ['$routeProvider'];
