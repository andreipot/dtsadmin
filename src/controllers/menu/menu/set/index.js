import Controller from '../../../controller';

export default class MenuSetController extends Controller {
  constructor($scope, $timeout, $location, $routeParams, AssetManager, DtsApi, DialogManager, Logger, MenuManager, UserModel) {
    super($scope, $timeout, DialogManager, Logger, UserModel);

    this.$location = $location;
    this.$routeParams = $routeParams;
    this.AssetManager = AssetManager;
    this.DtsApi = DtsApi;
    this.MenuManager = MenuManager;

    this.$scope.can_sync_menu = false;
    this.$scope.breadcrumbs = [];

    $scope.onElementDrag = (el, index) => {
      //el.element.entity.position = index;
    };

    this._defineMethod('onElementDragComplete');

    this._defineMethod('deleteSet');
    this._defineMethod('editSet');
    this._defineMethod('addCategory');
    this._defineMethod('addItem');

    $scope.showPromos = () => {
      this.$location.path('/menu/set/' + id + '/promos');
    };

    this._defineMethod('unsubscribe');
    this._defineMethod('syncMenu');
    this._defineMethod('openMenuLoader');
    this._defineMethod('getDeviceName');
    this._defineMethod('editElement');
  }

  async getData() {
    const token = this.$routeParams.id;
    const location = this.UserModel.location.token;
    const menu = await this.MenuManager.loadMenu(token);
    const devices = await this.AssetManager.loadHardware();

    this.$timeout(() => {
      this.$scope.menu = menu;
      this.$scope.devices = devices;

      this.$scope.is_editable = menu.location === location;
      this.$scope.is_subscribed = menu.parent != null && menu.location === location;
      //this.$scope.can_sync_menu = menu.kind !== 1;

      const categories = (menu.categories || [])
        .filter(category => !category.parent)
        .sort(this._sort);
      const items = (menu.items || [])
        .filter(item => !item.parent)
        .sort(this._sort);

      this.$scope.elements = categories
      .map(entity => {
        return {
          type: 'category',
          entity: entity
        }
      })
      .concat(
        items.map(entity => {
          return {
            type: 'item',
            entity: entity
          }
        })
      )
      .sort((a,b) => this._sort(a.entity, b.entity));

      this.$scope.breadcrumbs = [{
        title: menu.title
      }];
    });
  }

  async onElementDragComplete() {
    // if (!this.$scope.is_editable) {
    //   return;
    // }
    //
    // const tokens = this.$scope.elements
    //   .sort((a,b) => this._sort(a.entity, b.entity))
    //   .map(element => element.entity.token);
  }

  async deleteSet() {
    const confirmed = await this.DialogManager.confirm('Are you sure that you want to remove the menu?');

    if (!confirmed) {
      return;
    }

    const job = this.DialogManager.startJob();

    try {
      await this.MenuManager.deleteMenu(this.$scope.menu);
      this.$location.path('/menu/set');
    }
    finally {
      this.DialogManager.endJob(job);
    }
  }

  async editSet() {
    const menu = await this.DialogManager.showDialog(
      'MenuSetEditController',
      require('./edit/template.html'),
      {
        data: {
          menu: Object.assign({}, this.$scope.menu)
        }
      });

    if (menu) {
      this.$timeout(() => this.$scope.menu = menu);
    }
  }

  async addItem() {
    const item = await this.DialogManager.showDialog(
      'MenuItemEditController',
      require('../item/edit/template.html'),
      {
        data: {
          menu: this.$scope.menu,
          item: {
            order: { price: 0 }
          }
        }
      }
    );

    if (item) {
      await this.getData();
    }
  }

  async addCategory() {
    const category = await this.DialogManager.showDialog(
      'MenuCategoryEditController',
      require('../category/edit/template.html'),
      {
        data: {
          menu: this.$scope.menu,
          category: {}
        }
      }
    );

    if (category) {
      await this.getData();
    }
  }

  async unsubscribe() {
    const confirmed = this.DialogManager.confirm('Are you sure that you want to remove the menu from the current location?');

    if (!confirmed) {
      return;
    }
  }

  async syncMenu() {
    const confirmed = this.DialogManager.confirm('Are you sure that you want to synchronize the menu with Simplicity?');

    if (!confirmed) {
      return;
    }
  }

  openMenuLoader() {
    this.$location.path(`/menu/loader/${this.$scope.menu.token}`);
  }

  getDeviceName(element) {
    if (!element || !this.$scope.devices) {
      return;
    }

    return this.$scope.devices
      .filter(x => x.token === element.entity.dmc_device)
      .map(x => x.name)[0] || 'not defined';
  }

  editElement(element) {
    if (!element || !element.type || !this.$scope.menu) {
      return;
    }

    this.$location.path(`/menu/set/${this.$scope.menu.token}/${element.type}/${element.entity.token}`);
  }
}

MenuSetController.$inject = ['$scope', '$timeout', '$location', '$routeParams', 'AssetManager', 'DtsApi', 'DialogManager', 'Logger', 'MenuManager', 'UserModel'];
