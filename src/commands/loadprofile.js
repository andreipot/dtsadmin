'use strict';

export default
  ['AccessManager', 'LegacyApi', 'UserModel',
  (AccessManager, LegacyApi, UserModel) => {
  return function() {
    return LegacyApi.admin.getUserProfile()
    .then(profile => {
      LegacyApi.admin.getUserLocations()
      .then(locations => {
        let credentials = AccessManager.activeCredentials;

        UserModel.profile = profile;
        UserModel.locations = locations;
        UserModel.location = locations.filter(loc => loc.token === credentials.location)[0];
      });
    });
  };
}];
