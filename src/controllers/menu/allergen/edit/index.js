import Controller from '../../../controller';

export default class MenuAllergenEditController extends Controller {
  constructor($scope, $timeout, DialogManager, Logger, MenuManager, UserModel, data, close) {
    super($scope, $timeout, DialogManager, Logger, UserModel);

    this.MenuManager = MenuManager;
    this.$scope.allergen = data.allergen;
    this.close = close;

    this._defineMethod('save');
    this._defineMethod('cancel');
    this._defineMethod('delete');
  }

  async save() {
    const job = this.DialogManager.startJob();

    try {
      if (this.$scope.allergen.token) {
        let result = await this.MenuManager.updateAllergen(this.$scope.allergen);
        this.DialogManager.closeDialog(this.close, result);
      }
      else {
        let result = await this.MenuManager.createAllergen(this.$scope.allergen);
        this.DialogManager.closeDialog(this.close, result);
      }
    }
    finally {
      this.DialogManager.endJob(job);
    }
  }

  async cancel() {
    this.DialogManager.closeDialog(this.close);
  }

  async delete() {
    const confirmed = await this.DialogManager.confirm('Are you sure that you want to delete this allergen?');

    if (!confirmed) {
      return;
    }

    const job = this.DialogManager.startJob();

    try {
      await this.MenuManager.deleteAllergen(this.$scope.allergen);
      this.DialogManager.closeDialog(this.close, this.$scope.allergen);
    }
    finally {
      this.DialogManager.endJob(job);
    }
  }
}

MenuAllergenEditController.$inject = ['$scope', '$timeout', 'DialogManager', 'Logger', 'MenuManager', 'UserModel', 'data', 'close'];
