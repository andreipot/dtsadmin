'use strict';

import angular from 'angular';

import ModalDirective from './modal';

export default angular.module('TouchAdmin.directives.modal', [])
  .directive('adminmodal', ModalDirective)
  .name;
