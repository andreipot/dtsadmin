'use strict';

export default function() {
  return (value, flag) => {
    flag = parseInt(flag) || 0;
    return value && value.indexOf !== undefined ? value.indexOf(flag) !== -1 : value & flag === 1;
  };
};
