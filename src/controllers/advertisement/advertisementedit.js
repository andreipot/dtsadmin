'use strict';

export default class HomeController {
  constructor($scope, $timeout, $location, close, DialogManager, LegacyApi, advertisement) {
    $scope.advertisement = advertisement;

    switch (advertisement.type) {
      case 1:
        $scope.mediawidth = 970;
        $scope.mediaheight = 90;
        break;
      case 2:
        $scope.mediawidth = 300;
        $scope.mediaheight = 250;
        break;
      default:
        $scope.mediawidth = 300;
        $scope.mediaheight = 300;
        break;
    }

    $scope.imageSelected = function(token) {
      $scope.advertisement.image = token;
    };

    $scope.cancel = function() {
      DialogManager.closeDialog(close);
    };

    $scope.save = function() {
      var job = DialogManager.startJob();

      var task = advertisement.token ?
        LegacyApi.advertisement.updateBanner($scope.advertisement, advertisement.token) :
        LegacyApi.advertisement.saveBanner($scope.advertisement);

      task.then(function() {
        DialogManager.endJob(job);
        DialogManager.closeDialog(close, $scope.advertisement);
      }, function(e) {
        DialogManager.endJob(job);
        DialogManager.alert(e.message);
      });
    };

    $scope.delete = function() {
      DialogManager.confirm('Are you sure that you want to delete the advertisement?').then(function() {
        var job = DialogManager.startJob();

        LegacyApi.advertisement.deleteBanner(advertisement.token).then(function() {
          DialogManager.endJob(job);
          DialogManager.closeDialog(close);
          $timeout(function() {
            $location.path('/advertisement');
          });
        }, function(e) {
          DialogManager.endJob(job);
          DialogManager.alert(e.message);
        });
      });
    };
  }
}

HomeController.$inject = ['$scope', '$timeout', '$location', 'close', 'DialogManager', 'LegacyApi', 'advertisement'];
