'use strict';

import Application from './application';

import controllers_signup from '../controllers/signup/';
import controllers_hardware from '../controllers/hardware/';

class SignupApplication extends Application {
  constructor(options) {
    super([
      controllers_signup,
      controllers_hardware
    ], options);
  }
};

export default new SignupApplication({ app: 'oms' });
