'use strict';

export default class DeviceEditController {
  constructor($scope, $timeout, close, DialogManager, UserModel, LegacyApi, device) {
    var getData = function() {
      var job = DialogManager.startJob();

      LegacyApi.location.getSeats().then(function(seats) {
        DialogManager.endJob(job);
        $timeout(function() {
          $scope.seats = seats;
          $scope.device = device;
        });
      }, function(e) {
        DialogManager.endJob(job);
        DialogManager.alert(e.message);
      });
    };
    getData();

    $scope.cancel = function() {
      DialogManager.closeDialog(close);
    };

    $scope.save = function() {
      var job = DialogManager.startJob();

      var task = $scope.device.token ?
        LegacyApi.hardware.updateDevice($scope.device.token, $scope.device) :
        undefined;

      task.then(function() {
        DialogManager.endJob(job);
        DialogManager.closeDialog(close, $scope.device);
      }, function(e) {
        DialogManager.endJob(job);
        DialogManager.alert(e.message);
      });
    };

    $scope.delete = function() {
      DialogManager.confirm('Are you sure that you want to delete the device?').then(function() {
        var job = DialogManager.startJob();

        LegacyApi.hardware.deleteDevice($scope.device.token).then(function() {
          DialogManager.endJob(job);
          DialogManager.closeDialog(close, $scope.device);
        }, function(e) {
          DialogManager.endJob(job);
          DialogManager.alert(e.message);
        });
      });
    };
  }
}

DeviceEditController.$inject = ['$scope', '$timeout', 'close', 'DialogManager', 'UserModel', 'LegacyApi', 'device'];
