'use strict';

import Application from './application';

import controllers_advertisement from '../controllers/advertisement';
import controllers_customization from '../controllers/customization';
import controllers_deployment from '../controllers/deployment';
import controllers_hardware from '../controllers/hardware';
import controllers_home from '../controllers/home';
import controllers_location from '../controllers/location';
import controllers_menu from '../controllers/menu';
import controllers_player from '../controllers/player';
import controllers_survey from '../controllers/survey';
import controllers_mediagallery from '../controllers/mediagallery';

class PanelApplication extends Application {
  constructor() {
    super([
      controllers_advertisement,
      controllers_customization,
      controllers_deployment,
      controllers_hardware,
      controllers_home,
      controllers_location,
      controllers_menu,
      controllers_player,
      controllers_survey,
      controllers_mediagallery
    ]);
  }
};

export default new PanelApplication();
