import AbstractModel from './abstract';

export default class UserModel extends AbstractModel {
  constructor(StorageProvider) {
    super(StorageProvider);

    this._defineProperty('profile');
    this._defineProperty('locations', undefined, []);
    this._defineProperty('location');
    this._defineProperty('medias', undefined, []);
  }
};
