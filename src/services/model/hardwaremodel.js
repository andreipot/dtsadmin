import AbstractModel from './abstract';
import URI from 'urijs';

export default class HardwareModel extends AbstractModel {
  constructor(DtsApi, LegacyApi, StorageProvider) {
    super(StorageProvider);

    this._backend = DtsApi;
    this._api = LegacyApi;

    this._defineProperty('seats', undefined, []);
    this._defineProperty('devices', undefined, []);
    this._defineProperty('devices2', undefined, []);
    this._defineProperty('currentDevice');

    this._parameters = URI(window.location).search(true);
  }

  loadSeats() {
    return this._api.location.getSeats()
    .then(seats => this.seats = seats);
  }

  loadDevices() {
    return this._api.hardware.getDevices()
    .then(devices => this.devices = devices);
  }

  registerDevice() {
    let device = this.currentDevice;

    if (!device || device.token) {
      return Promise.reject('Current device is not set or invalid.');
    }

    return this._api.hardware.registerDevice(device)
    .then(result => {
      this.currentDevice.token = result.token;
      this.devices = this.devices.concat([ this.currentDevice ]);
    });
  }

  confirmAccess() {
    let device = this.currentDevice;

    if (!device || !device.token || !device.password) {
      return Promise.reject('Current device is not set or not registered.');
    }

    let client_id = this._parameters.client_id;

    if (!client_id) {
      return Promise.reject('Client ID is not provided.');
    }

    return this._backend.api.request('oauth2', 'grantToken', {
      grant_type: 'password',
      client_id: client_id,
      username: device.token,
      password: device.password,
      seat_id: device.seat_token
    })
    .then(token => {
      let url = this._getAuthUrl(token);
      return this._redirect(url);
    });
  }

  rejectAccess(token) {
    let error = {
      error: token.error || 'unknown_error',
      error_description: token.error_description || 'Unknown error'
    };

    let url = this._getAuthUrl(error);
    return this._redirect(url);
  };

  _getAuthUrl(token) {
    let baseUrl = URI(this._parameters.redirect_uri);
    let url = Object.keys(this._parameters)
    .reduce((uri, key) => uri.addSearch(key, this._parameters[key]), URI());

    Object.keys(token).forEach(key => url.addSearch(key, token[key]));

    return baseUrl.fragment(url.query()).toString();
  }

  _redirect(url) {
    window.location = url;
    return Promise.resolve();
  }
};
