import AbstractModel from './abstract';

export default class MenuModel extends AbstractModel {
  constructor(StorageProvider) {
    super(StorageProvider);

    this._defineProperty('currency', undefined, '$');
    this._defineProperty('allergens', undefined, []);
    this._defineProperty('menus', undefined, {});
    this._defineProperty('modifiers', undefined, []);
    this._defineProperty('streams', undefined, []);
  }
};
