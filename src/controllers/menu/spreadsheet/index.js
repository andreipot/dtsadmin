import Controller from '../../controller';

import Awesomplete from 'awesomplete';

export default class MenuSpreadsheetController extends Controller {
  constructor($scope, $timeout, $location, $routeParams, AssetManager, DialogManager, MenuManager, ShellManager, UserModel, Logger) {
    super($scope, $timeout, DialogManager, Logger, UserModel);

    this.id = $routeParams.id;

    this.$location = $location;
    this.AssetManager = AssetManager;
    this.MenuManager = MenuManager;

    this.defaultMenu = {
      items: [],
      categories: []
    };

    this._defineMethod('saveMenu');
    this._defineMethod('createItem');
    this._defineMethod('removeItem');
    this._defineMethod('createCategory');
    this._defineMethod('removeCategory');
    this._defineMethod('editMedia');
    this._defineMethod('editAllergenInfo');
    this._defineMethod('editModifiers');
  }

  async getData () {
    const props = await Promise.all([
      this.getMenu(),
      this.getHardware(),
      this.getModifiers(),
      this.getAllergens()
    ]);

    this.$timeout(() => {
      const menu = props[0];

      if (!menu.token) {
        menu.type = 'orderable';
      }

      if (!menu.type) {
        menu.type = '';
      }

      this.$scope.menu = menu;
      this.$scope.devices = props[1];
      this.$scope.modifiers = props[2];
      this.$scope.allergens = props[3];
    });

    this.library = {
      hints: [],
      items: []
    };

    this.MenuManager.loadMenus().then(menus => {
      Promise.all(menus
        .filter(menu => menu.token !== this.$scope.menu.token)
        .map(menu => this.MenuManager.loadMenu(menu.token))
      )
      .then(menus => {
        menus.forEach(menu => (menu.items || []).forEach(item => {
          if (this.library.hints.indexOf(item.title) !== -1) {
            return;
          }

          let data = Object.assign({}, item);
          delete data.token;
          delete data.parent;
          delete data.position;

          this.library.hints.push(data.title);
          this.library.items.push(data);
        }));
      });
    });
  }

  async getMenu() {
    return this.id ? await this.MenuManager.loadMenu(this.id) : this.defaultMenu;
  }

  async getHardware() {
    const data = await this.AssetManager.loadHardware();
    return data.filter(x => x.role === 'snap');
  }

  async getModifiers() {
    return await this.MenuManager.loadModifiers();
  }

  async getAllergens() {
    return await this.MenuManager.loadAllergens();
  }

  async editMedia(item) {
    const media = await this.showMediaEditDialog(item.image);
    this.$timeout(() => item.image = media);
  }

  async editAllergenInfo(item) {
    const result = await this.showAllergensEditDialog(item);

    if (result) {
      item.allergens = result.allergens;
    }
  }

  async editModifiers(item) {
    const result = await this.showModifiersEditDialog(item);

    if (result) {
      item.modifiers = result.modifiers;
    }
  }

  async showMediaEditDialog(media) {
    return this.DialogManager.showDialog('MediaEditController', require('../../dialog/mediaedit.html'), {
      options: {
        media: media,
        mediawidth: 470,
        mediaheight: 410
      }
    });
  }

  async showModifiersEditDialog(item) {
    return this.DialogManager.showDialog('ModifiersEditController', require('../../dialog/modifiersedit.html'), {
      data: {
        modifiers: this.$scope.modifiers,
        item: $.extend(true, { modifiers: [] }, item)
      }
    });
  }

  async showAllergensEditDialog(item) {
    return this.DialogManager.showDialog('AllergensEditController', require('../../dialog/allergensedit.html'), {
      data: {
        allergens: this.$scope.allergens,
        item: $.extend(true, { allergens: [] }, item)
      }
    });
  }

  async saveMenu() {
    const confirmed = this.$scope.menu.token ?
      await this.DialogManager.confirm('Are you sure that you want to save this menu?') :
      await this.DialogManager.confirm('Are you sure that you want to create this menu?');

    if (!confirmed) {
      return;
    }

    const job = this.DialogManager.startJob();

    try {
      const menu = this.$scope.menu;

      menu.categories.forEach((category, i) => {
        if (category.parent === '') {
          delete category.parent;
        }
      });

      menu.items.forEach((item, i) => {
        if (item.dmc_device === '') {
          delete item.dmc_device;
        }

        if (item.parent === '' || menu.categories.map(x => x.token).indexOf(item.parent) === -1) {
          delete item.parent;
        }

        if (!item.order) {
          item.order = {
            price: 0,
            name: item.title
          };
        }

        if (item.type === '') {
          item.type = undefined;
        }

        if (menu.type === 'dmc') {
          delete item.parent;
        }
      });



      if (menu.type === 'dmc') {
        menu.categories = [];
      }

      if (this.$scope.menu.token) {
        let result = await this.MenuManager.updateMenu(menu);
        this.$location.path('/menu/set/' + result.token);
      }
      else {
        let result = await this.MenuManager.createMenu(menu);
        this.$location.path('/menu/set/' + result.token);
      }
    }
    finally {
      this.DialogManager.endJob(job);
    }
  }

  createItem() {
    const payload = {
      token: this.MenuManager.generateID()
    };

    this.$scope.menu.items.push(payload);

    this.$timeout(() => {
      if (this.library.hints.length > 0) {
        const element = $('.spreadsheet-item-title').get(this.$scope.menu.items.length - 1);

        new Awesomplete(element, {
        	list: this.library.hints
        });

        element.addEventListener('awesomplete-select', (e) => {
          const i = this.library.hints.indexOf(e.text);

          if (i !== -1) {
            let item = this.library.items[i];

            this.$timeout(() => Object.assign(payload, item));
          }
        }, false);
      }
    });
  }

  removeItem(item) {
    this.$scope.menu.items = this.$scope.menu.items.filter(x => x !== item);
  }

  createCategory() {
    const payload = {
      token: this.MenuManager.generateID()
    };

    this.$scope.menu.categories.push(payload);
  }

  removeCategory(category) {
    this.$scope.menu.categories = this.$scope.menu.categories.filter(x => x !== category);
  }
}

MenuSpreadsheetController.$inject = ['$scope', '$timeout', '$location', '$routeParams', 'AssetManager', 'DialogManager', 'MenuManager', 'ShellManager', 'UserModel', 'Logger'];
