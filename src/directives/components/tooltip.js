export default [() => {
  return {
    restrict: 'A',
    link: (scope, element, attrs) => {
      $(element).tooltip({
        title: attrs['tooltip'],
        container: 'body'
      });

      $(element).click(() => {
        $(element).tooltip('hide');
      });
    }
  };
}];
