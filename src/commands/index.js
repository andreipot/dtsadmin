'use strict';

import angular from 'angular';

import CommandLoadProfile from './loadprofile';
import CommandLogout from './logout';
import CommandSwitchLocation from './switchlocation';
import CommandStartup from './startup';
import CommandSubmitSnap from './submitsnap';

export default angular.module('TouchAdmin.commands', [])
  .service('CommandLoadProfile', CommandLoadProfile)
  .service('CommandLogout', CommandLogout)
  .service('CommandSwitchLocation', CommandSwitchLocation)
  .service('CommandStartup', CommandStartup)
  .service('CommandSubmitSnap', CommandSubmitSnap)
  .name;
