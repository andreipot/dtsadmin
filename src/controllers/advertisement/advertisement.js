'use strict';

export default class HomeController {
  constructor($scope, $timeout, LegacyApi, DialogManager, ShellManager, UserModel) {
    var getData = function() {
      if (!UserModel.location) {
        return;
      }

      var job = DialogManager.startJob();

      var failure = function(e) {
        DialogManager.endJob(job);
        DialogManager.alert(e.message);
      };

      LegacyApi.advertisement.getBanners().then((advertisements) => {
        DialogManager.endJob(job);
        $timeout(function() {
          $scope.advertisements_main = advertisements.filter(x => x.type === 1);
          $scope.advertisements_misc = advertisements.filter(x => x.type === 2);
        });
      }, failure);
    };

    UserModel.locationChanged.add(getData);
    getData();

    $scope.$on('$destroy', () => UserModel.locationChanged.remove(getData));

    $scope.onAdvertisementDrag = function(element, index) {
      var advertisement = element.advertisement;
      advertisement.index = index;
    };

    $scope.onAdvertisementDragComplete = function() {
      var tokens = $scope.advertisements_main
        .concat($scope.advertisements_misc)
        .sort((a, b) => {
          if (a.index > b.index) {
            return 1;
          }
          else if (a.index < b.index) {
            return -1;
          }

          return 0;
        })
        .map(ad => ad.token);

      var job = DialogManager.startJob();

      LegacyApi.advertisement.sortBanners(tokens).then(function() {
        DialogManager.endJob(job);
      }, function(e) {
        DialogManager.endJob(job);
        DialogManager.alert(e.message);
      });
    };

    $scope.editAdvertisement = function(ad) {
      DialogManager.showDialog(
        'AdvertisementEditController',
        require('./advertisementedit.html'), {
          advertisement: jQuery.extend({}, ad)
        })
      .then(getData);
    };

    $scope.addAdvertisement = function(type) {
      DialogManager.showDialog(
        'AdvertisementEditController',
        require('./advertisementedit.html'), {
          advertisement: {
            type: type
          }
        })
      .then(getData);
    };
  }
}

HomeController.$inject = ['$scope', '$timeout', 'LegacyApi', 'DialogManager', 'ShellManager', 'UserModel'];
