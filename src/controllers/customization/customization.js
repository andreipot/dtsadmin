'use strict';

export default class CustomizationController {
  constructor($scope, $timeout, LegacyApi, DialogManager, ShellManager, UserModel) {
    var getData = function() {
      if (!UserModel.location) {
        return;
      }

      var job = DialogManager.startJob();

      var failure = function(e) {
        DialogManager.endJob(job);
        DialogManager.alert(e.message);
      };

      LegacyApi.customization.getBackgrounds().then(function(backgrounds) {
        LegacyApi.customization.getScreensavers().then(function(screensavers) {
          LegacyApi.customization.getElements().then(function(elements) {
            DialogManager.endJob(job);
            $timeout(function() {
              $scope.backgrounds = backgrounds;
              $scope.screensavers = screensavers;
              $scope.elements = elements;
            });
          }, failure);
        }, failure);
      }, failure);
    };

    UserModel.locationChanged.add(getData);
    getData();

    $scope.$on('$destroy', () => UserModel.locationChanged.remove(getData));

    var getSortedTokens = function(items) {
      return items.sort(function(a, b) {
        if (a.index > b.index) {
          return 1;
        }
        else if (a.index < b.index) {
          return -1;
        }

        return 0;
      }).map(function(item) {
        return item.token;
      });
    };

    $scope.onDrag = function(element, index) {
      element.item.index = index;
    };

    $scope.onBackgroundDragComplete = function() {
      var tokens = getSortedTokens($scope.backgrounds);
      var job = DialogManager.startJob();

      LegacyApi.customization.sortBackgrounds(tokens).then(function() {
        DialogManager.endJob(job);
      }, function(e) {
        DialogManager.endJob(job);
        DialogManager.alert(e.message);
      });
    };

    $scope.onScreensaverDragComplete = function() {
      var tokens = getSortedTokens($scope.screensavers);
      var job = DialogManager.startJob();

      LegacyApi.customization.sortScreensavers(tokens).then(function() {
        DialogManager.endJob(job);
      }, function(e) {
        DialogManager.endJob(job);
        DialogManager.alert(e.message);
      });
    };

    $scope.addBackground = function() {
      DialogManager.showDialog(
        'BackgroundEditController',
        require('./backgroundedit.html'), {
          background: {
            type: 1
          }
        })
      .then(getData);
    };

    $scope.editBackground = function(item) {
      DialogManager.showDialog(
        'BackgroundEditController',
        require('./backgroundedit.html'), {
          background: jQuery.extend({}, item)
        })
      .then(getData);
    };

    $scope.addScreensaver = function() {
      DialogManager.showDialog(
        'ScreensaverEditController',
        require('./screensaveredit.html'), {
          screensaver: {
            type: 2
          }
        })
      .then(getData);
    };

    $scope.editScreensaver = function(item) {
      DialogManager.showDialog(
        'ScreensaverEditController',
        require('./screensaveredit.html'), {
          screensaver: jQuery.extend({}, item)
        })
      .then(getData);
    };

    $scope.addElement = function() {
    DialogManager.showDialog(
      'ElementEditController',
      require('./elementedit.html'), {
      element: {
      }
      })
    .then(getData);
    };

    $scope.editElement = function(item) {
      DialogManager.showDialog(
      'ElementEditController',
      require('./elementedit.html'), {
        element: jQuery.extend({}, item)
      })
    .then(getData);
    };

    $scope.getElementName = function(type) {
      switch (type) {
        case 1:
          return 'Home Button';
        case 2:
          return 'Back Button';
        case 3:
          return 'Cart Button';
        case 4:
          return 'Rotate Button';
        case 5:
          return 'Request Waiter Button';
        case 6:
          return 'Request Check Button';
        case 7:
          return 'Survey Button';
        case 8:
          return 'Chat Button';
        case 9:
          return 'Location Logo';
        case 10:
          return 'Location Map';
        case 11:
          return 'Settings Button';
        case 12:
          return 'Welcome Tile';
        default:
          return '';
      }
    };
  }
}

CustomizationController.$inject = ['$scope', '$timeout', 'LegacyApi', 'DialogManager', 'ShellManager', 'UserModel'];
