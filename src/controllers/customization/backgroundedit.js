'use strict';

export default class BackgroundEditController {
  constructor($scope, close, DialogManager, LegacyApi, background) {
    $scope.background = background;

    $scope.imageSelected = function(token) {
      $scope.background.image = token;
    };

    $scope.videoSelected = function(token) {
      $scope.background.video = token;
    };

    $scope.cancel = function() {
      DialogManager.closeDialog(close);
    };

    $scope.save = function() {
      var job = DialogManager.startJob();

      var task = background.token ?
        LegacyApi.customization.updateBackground($scope.background, background.token) :
        LegacyApi.customization.saveBackground($scope.background);

      task.then(function() {
        DialogManager.endJob(job);
        DialogManager.closeDialog(close, $scope.background);
      }, function(e) {
        DialogManager.endJob(job);
        DialogManager.alert(e.message);
      });
    };

    $scope.delete = function() {
      DialogManager.confirm('Are you sure that you want to delete the background?').then(function() {
        var job = DialogManager.startJob();

        LegacyApi.customization.deleteBackground(background.token).then(function() {
          DialogManager.endJob(job);
          DialogManager.closeDialog(close);
        }, function(e) {
          DialogManager.endJob(job);
          DialogManager.alert(e.message);
        });
      });
    };
  }
}

BackgroundEditController.$inject = ['$scope', 'close', 'DialogManager', 'LegacyApi', 'background'];
