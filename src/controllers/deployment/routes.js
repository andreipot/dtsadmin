'use strict';

export default function routes($routeProvider) {
  $routeProvider.when('/deployment', { template: require('./list/template.html'), controller: 'DeploymentController' });
}

routes.$inject = ['$routeProvider'];
