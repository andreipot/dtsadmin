import Controller from '../../../../controller';

export default class MenuItemEditController extends Controller {
  constructor($scope, $timeout, $element, DialogManager, DtsApi, Logger, MenuManager, UserModel, data, close) {
    super($scope, $timeout, DialogManager, Logger, UserModel);

    this.DtsApi = DtsApi;
    this.MenuManager = MenuManager;
    this.$scope.item = data.item;
    this.$scope.menu = data.menu;
    this.close = close;

    this.$scope.currency = this.MenuManager.model.currency;
    this.$scope.item.type = this.$scope.item.type || this.$scope.menu.type || 'orderable';

    this.$scope.cancel = () => {
      this.DialogManager.closeDialog(close);
    };

    this._defineMethod('save');
  }

  async save() {
    const job = this.DialogManager.startJob();

    const item = $.extend(true, {}, this.$scope.item);

    try {
      if (!item.order.name) {
        item.order.name = item.title;
      }

      if (item.token) {
        let result = await this.MenuManager.updateMenuItem(this.$scope.menu, item);
        this.DialogManager.closeDialog(this.close, result);
      }
      else {
        let result = await this.MenuManager.createMenuItem(this.$scope.menu, item);
        this.DialogManager.closeDialog(this.close, result);
      }
    }
    finally {
      this.DialogManager.endJob(job);
    }
  }
}

MenuItemEditController.$inject = ['$scope', '$timeout', '$element', 'DialogManager', 'DtsApi', 'Logger', 'MenuManager', 'UserModel', 'data', 'close'];
