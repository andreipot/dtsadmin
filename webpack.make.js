'use strict';

//require('babel-core/register');

var webpack = require('webpack');

var BowerWebpackPlugin = require('bower-webpack-plugin');
var CommonsChunkPlugin = require("webpack/lib/optimize/CommonsChunkPlugin");
var HtmlWebpackPlugin = require('html-webpack-plugin');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var SplitByPathPlugin = require('webpack-split-by-path');

var autoprefixer = require('autoprefixer');
var jade = require('jade');
var path = require('path');

var pkg = require('./package.json');

module.exports = function makeWebpackConfig(options) {
  var BUILD = !!options.BUILD;
  var TEST = !!options.TEST;

  var config = {};
  var dist = BUILD ? `dist/${pkg.version}` : 'dist/dev';

  if (TEST) {
    config.entry = {}
  } else {
    config.entry = {
      panel: ['babel-polyfill', path.resolve(__dirname, './src/apps/panel.js')],
      login: ['babel-polyfill', path.resolve(__dirname, './src/apps/login.js')],
      signup_snap: ['babel-polyfill', path.resolve(__dirname, './src/apps/signup_snap.js')],
      signup_oms: ['babel-polyfill', path.resolve(__dirname, './src/apps/signup_oms.js')]
    }
  }

  if (TEST) {
    config.output = {}
  } else {
    config.output = {
      path: path.resolve(__dirname, 'public'),
      publicPath: BUILD ? '/' : 'http://localhost:8081/',
      filename: BUILD ? `${dist}/[name].js` : `${dist}/[hash].[name].js`,
      chunkFilename: BUILD ? `${dist}/[name].js` : `${dist}/[hash].[name].js`
    }
  }

  if (TEST) {
    config.devtool = 'inline-source-map';
  } else if (BUILD) {
    config.devtool = 'source-map';
  } else {
    config.devtool = 'eval';
  }

  config.resolve = {
    alias: {
      'aws-sdk': 'aws-sdk/dist/aws-sdk.min.js',
      'better-dom' : path.resolve(__dirname, './bower_components/better-dom/dist/better-dom.js'),
      'better-timeinput-polyfill-js' : path.resolve(__dirname, './bower_components/better-timeinput-polyfill/dist/better-timeinput-polyfill.js'),
      'better-timeinput-polyfill-css' : path.resolve(__dirname, './bower_components/better-timeinput-polyfill/dist/better-timeinput-polyfill.css')
    }
  };

  config.module = {
    preLoaders: [],
    noParse: [
      /aws\-sdk/,
    ],
    loaders: [{
      test: /\.js$/,
      loader: 'babel-loader',
      exclude: /node_modules/,
      query: pkg.babel
    },
    {
      test: /\.(png|jpg|jpeg|gif)$/,
      loader: 'file',
      query: {
        name: BUILD ? `${dist}/[name].[ext]` : `${dist}/[hash].[name].[ext]`
      }
    },
    {
      test: /\.(ttf|eot|svg|woff(2)?)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
      loader: "file",
      query: {
        name: BUILD ? `${dist}/[name].[ext]` : `${dist}/[hash].[name].[ext]`
      }
    },
    {
      test: /\.(json)$/,
      loader: 'json'
    },
    {
      test: /\.(html)$/,
      loader: 'raw'
    }]
  };

  if (TEST) {
    config.module.preLoaders.push({
      test: /\.js$/,
      exclude: [
        /node_modules/,
        /\.test\.js$/
      ],
      loader: 'isparta-instrumenter'
    })
  }

  var cssLoader = {
    test: /\.css$/,
    loader: ExtractTextPlugin.extract('style', 'css?sourceMap!postcss')
  };

  var lessLoader = {
    test: /\.less$/,
    loader: ExtractTextPlugin.extract('style', 'css!less!postcss')
  };

  if (TEST) {
    cssLoader.loader = 'null'
    lessLoader.loader = 'null'
  }

  config.module.loaders.push(cssLoader);
  config.module.loaders.push(lessLoader);

  config.postcss = [
    autoprefixer({
      browsers: ['last 2 version']
    })
  ];

  config.plugins = [
    new SplitByPathPlugin([{
      name: 'common',
      filename: `${dist}/common.js`,
      path: path.join(__dirname, 'src')
    }, {
      name: 'libs',
      filename: `${dist}/libs.js`,
      path: [
        path.join(__dirname, 'node_modules'),
        path.join(__dirname, 'bower_components')
      ]
    }]),
    new webpack.ProvidePlugin({
      $: "jquery",
      jQuery: "jquery",
      "window.jQuery": "jquery"
    }),
    new ExtractTextPlugin(BUILD ? `${dist}/[name].css` : `${dist}/[hash].[name].css`, {
      disable: !BUILD || TEST
    }),
    new webpack.DefinePlugin({
      ADMIN_DEBUG: !BUILD && !TEST,
      ADMIN_VERSION: `'${pkg.version}'`
    }),
    new webpack.IgnorePlugin(/^(socketcluster-client)$/)
  ];

  if (!TEST) {
    function templateProvider(name) {
      return (o, compilation) => {
        return jade.renderFile(`./src/views/${name}.jade`, {
          pretty: true
        });
      }
    }

    for (let p in config.entry) {
      config.plugins.push(
        new HtmlWebpackPlugin({
          chunks: ['libs', 'common', p],
          filename: `${p}.html`,
          inject: 'head',
          templateContent: templateProvider(p)
        })
      );
    }
  }

  if (BUILD) {
    config.plugins.push(
      new webpack.NoErrorsPlugin(),
      new webpack.optimize.DedupePlugin(),
      new webpack.optimize.UglifyJsPlugin()
    )
  }

  config.devServer = {
    contentBase: './public',
    stats: {
      modules: false,
      cached: false,
      colors: true,
      chunk: false
    }
  };

  return config;
};
