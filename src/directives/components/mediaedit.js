'use strict';

export default
  ['DialogManager', 'ShellManager', '$timeout',
  (DialogManager, ShellManager, $timeout) => {
  return {
    restrict: 'E',
    replace: true,
    scope: {
      ngModel: '=',
      ngChange: '=',
      mediawidth: '@?',
      mediaheight: '@?'
    },
    template: require('./mediaedit.html'),
    link: function(scope, attrs) {
      scope.edit = () => {
        DialogManager.showDialog(
          'MediaEditController',
          require('../../controllers/dialog/mediaedit.html'), {
            options: {
              media: scope.ngModel,
              mediawidth: scope.mediawidth,
              mediaheight: scope.mediaheight
            }
          })
        .then(result => $timeout(() => scope.ngModel = Boolean(result) ? result : null));
      };
    }
  };
}]
