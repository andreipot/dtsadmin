import Controller from '../../../../controller';

export default class MenuSetDuplicateController extends Controller {
  constructor($scope, $timeout, DialogManager, DtsApi, Logger, MenuManager, UserModel, close, data) {
    super($scope, $timeout, DialogManager, Logger, UserModel);

    this.DtsApi = DtsApi;
    this.MenuManager = MenuManager;
    this.close = close;

    this._defineMethod('save');
    this._defineMethod('cancel');
    this._defineMethod('onChange');
  }

  async getData() {
    const menus = await this.MenuManager.loadMenus();

    this.$timeout(() => {
      const menu = (menus[0] || {});
      this.$scope.menus = menus;
      this.$scope.menu = menu.token;
      this.$scope.title = menu.title + ' copy';
    });
  }

  async save() {
    const job = this.DialogManager.startJob();

    try {
      const menu = await this.MenuManager.loadMenu(this.$scope.menu);
      let copy = await this.MenuManager.duplicateMenu(menu);

      copy.title = this.$scope.title;

      let result = await this.MenuManager.createMenu(copy);
      this.DialogManager.closeDialog(this.close, result);
    }
    finally {
      this.DialogManager.endJob(job);
    }
  }

  cancel() {
    this.DialogManager.closeDialog(this.close);
  }

  onChange() {
    const menu = this.$scope.menus.filter(x => x.token === this.$scope.menu)[0];

    this.$scope.title = menu ? menu.title + ' copy' : '';
  }
}

MenuSetDuplicateController.$inject = ['$scope', '$timeout', 'DialogManager', 'DtsApi', 'Logger', 'MenuManager', 'UserModel', 'close', 'data'];
