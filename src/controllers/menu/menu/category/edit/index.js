import Controller from '../../../../controller';

export default class HomeController extends Controller {
  constructor($scope, $timeout, DialogManager, Logger, MenuManager, UserModel, data, close) {
    super($scope, $timeout, DialogManager, Logger, UserModel);

    this.MenuManager = MenuManager;
    this.$scope.category = data.category;
    this.$scope.menu = data.menu;
    this.close = close;

    this.$scope.cancel = () => {
      this.DialogManager.closeDialog(close);
    };

    this._defineMethod('save');
  }

  async save() {
    const job = this.DialogManager.startJob();

    try {
      if (this.$scope.category.token) {
        let result = await this.MenuManager.updateMenuCategory(this.$scope.menu, this.$scope.category);
        this.DialogManager.closeDialog(this.close, result);
      }
      else {
        let result = await this.MenuManager.createMenuCategory(this.$scope.menu, this.$scope.category);
        this.DialogManager.closeDialog(this.close, result);
      }
    }
    finally {
      this.DialogManager.endJob(job);
    }
  }
}

HomeController.$inject = ['$scope', '$timeout', 'DialogManager', 'Logger', 'MenuManager', 'UserModel', 'data', 'close'];
