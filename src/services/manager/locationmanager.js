import AbstractManager from './abstract';

export default class LocationManager extends AbstractManager {
  constructor($location, StorageProvider, Logger) {
    super(Logger);

    this.$$location = $location;
    this._callbackStore = StorageProvider.getSessionStorage('callback');
  }

  load(location) {
    window.location.assign(`/${location}.html`);
  }

  saveLocation() {
    this._callbackStore.write(location.href);
  }

  loadSavedLocation() {
    let loc = this._callbackStore.read();

    if (loc) {
      this._callbackStore.clear();
      window.location.assign(loc);
      return true;
    }

    return false;
  }
};
