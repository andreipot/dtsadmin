'use strict';

import angular from 'angular';
import angular_route from 'angular-route';

import routes from './routes';
import LoginController from './login';
import MainLoginController from './main';

export default angular.module('TouchAdmin.controllers.login', [ angular_route ])
  .config(routes)
  .controller('LoginController', LoginController)
  .controller('MainLoginController', MainLoginController)
  .name;
