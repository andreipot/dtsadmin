import angular from 'angular';
import applications from '../configs/applications';
import hosts from '../configs/hosts';

import 'raygun4js';

if (applications.raygun && !ADMIN_DEBUG) {
  Raygun
  .init(applications.raygun, {
  }, {
    version: ADMIN_VERSION
  })
  .setVersion(ADMIN_VERSION)
  .attach();
}

export default angular.module('TouchAdmin.configs', [])
  .constant('Applications', applications)
  .constant('Hosts', hosts)
  .name;
