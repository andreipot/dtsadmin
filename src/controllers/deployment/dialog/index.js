import Controller from '../../controller';

export default class DeploymentDialogController extends Controller {
  constructor($scope, $timeout, CommandSubmitSnap, DeploymentManager, DialogManager, Logger, MenuManager, UserModel, close) {
    super($scope, $timeout, DialogManager, Logger, UserModel);

    this.CommandSubmitSnap = CommandSubmitSnap;
    this.DeploymentManager = DeploymentManager;
    this.MenuManager = MenuManager;
    this.close = close;

    this._defineMethod('save');
    this._defineMethod('cancel');
    this._defineMethod('toggle');

    this._defineMethod('onElementDrag');
    this._defineMethod('onElementDragComplete');
  }

  async getData() {
    const menus = await this.MenuManager.loadMenus();
    const deployments = await this.DeploymentManager.loadDeployments();

    const digest = deployments.filter(data => data.status === 'active').map(x => x.digest)[0] || {};

    this.$timeout(() => {
      this.$scope.items = menus.map(menu => {
        return {
          selected: (digest.menus || []).indexOf(menu.token) !== -1,
          menu: menu
        }
      });

      this._refreshList();
    });
  }

  toggle(item) {
    item.selected = !item.selected;
  }

  onElementDrag(el, index) {
    el.item.position = index;
  };

  onElementDragComplete() {
    this.$timeout(() => this._refreshList());
  }

  async save() {
    let menus = this.$scope.items
    .filter(x => x.selected)
    .sort(this._sort)
    .map(x => x.menu.token);

    if (menus.length === 0) {
      return this.DialogManager.alert('Please select at least one menu to submit.');
    }

    const job = this.DialogManager.startJob();

    try {
      let digest = {
        menus: menus
      };

      let deployment = await this.CommandSubmitSnap(digest);
      this.DialogManager.closeDialog(this.close, deployment);
    }
    finally {
      this.DialogManager.endJob(job);
    }
  }

  cancel() {
    this.DialogManager.closeDialog(this.close);
  }

  _refreshList() {
    this.$scope.items = this.$scope.items
      .sort(this._sort)
      .map((entity, i) => {
        entity.position = i;
        return entity;
      });
  }
}

DeploymentDialogController.$inject = ['$scope', '$timeout', 'CommandSubmitSnap', 'DeploymentManager', 'DialogManager', 'Logger', 'MenuManager', 'UserModel', 'close'];
