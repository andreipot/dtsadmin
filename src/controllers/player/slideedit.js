'use strict';

export default class SlideEditController {
  constructor($scope, close, DialogManager, LegacyApi, entity) {
    $scope.entity = entity;
    $scope.entity.transition_delay = (entity.transition_delay || 0).toString();

    $scope.cancel = () => {
      DialogManager.closeDialog(close);
    };

    $scope.save = () => {
      DialogManager.closeDialog(close, $scope.entity);
    };

    $scope.delete = () => {
      DialogManager.closeDialog(close, 'delete');
    };
  }
}

SlideEditController.$inject = ['$scope', 'close', 'DialogManager', 'LegacyApi', 'entity'];
