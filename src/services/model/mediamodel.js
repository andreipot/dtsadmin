import AbstractModel from './abstract';

export default class MediaModel extends AbstractModel {
  constructor(StorageProvider) {
    super(StorageProvider);

    this._defineProperty('medias', undefined, []);
  }
};
