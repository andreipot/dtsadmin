'use strict';

import angular from 'angular';
import angular_route from 'angular-route';

import routes from './routes';
import SurveyController from './survey';
import SurveyQuestionEditController from './surveyquestionedit';

export default angular.module('TouchAdmin.controllers.survey', [ angular_route ])
  .config(routes)
  .controller('SurveyController', SurveyController)
  .controller('SurveyQuestionEditController', SurveyQuestionEditController)
  .name;
