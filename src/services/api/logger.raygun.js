import Logger from './logger';
import 'raygun4js';

export default class RaygunLogger extends Logger {
  constructor() {
    super();
  }

  warn(msg, e) {
    this._call('warn', msg, e);
  }

  error(msg, e) {
    this._call('error', msg, e);
  }

  fatal(msg, e) {
    this._call('fatal', msg, e);
  }

  setUser(location) {
    let user = { isAnonymous: true };

    if (location) {
      user.fullName = location.name;
      user.identifier = location.token;
      user.isAnonymous = false;
    }

    Raygun.resetAnonymousUser();

    if (!user.isAnonymous) {
      Raygun.setUser(user.identifier, false, undefined, user.firstName, user.fullName, user.uuid);
    }
  }

  _call(level, msg, err) {
    Raygun.send(err || new Error(msg), err ? { message: msg, level: level } : undefined, [ level ]);
  }
}
