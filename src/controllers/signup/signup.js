'use strict';

export default class SignupController {
  constructor($scope, $timeout, Options, ClientDetector, DialogManager, ShellManager, HardwareModel, UserModel) {
    var application = Options.app;

    $scope.application = application;
    $scope.device = HardwareModel.currentDevice;
    $scope.devices = HardwareModel.devices;
    $scope.location = UserModel.location;
    $scope.seats = HardwareModel.seats || [];

    HardwareModel.currentDeviceChanged.add(device => {
      $timeout(() => $scope.device = device || {});
    });

    HardwareModel.devicesChanged.add(() => {
      $timeout(() => $scope.devices = HardwareModel.devices);
    });

    if (application === 'snap') {
      HardwareModel.seatsChanged.add(() => $timeout(() => {
        $scope.seats = HardwareModel.seats;
        $scope.seats.push({
          token: null,
          name: "Don't assign to a table."
        });
      }));
    }

    UserModel.locationChanged.add(getData);

    if (UserModel.location) {
      getData();
    }

    function getData() {
      $timeout(() => $scope.location = UserModel.location);

      let job = DialogManager.startJob();

      function onError(e) {
        DialogManager.endJob(job);
        DialogManager.alert(e.message).then(() => HardwareModel.rejectAccess(e));
      }

      var task = application === 'snap' ?
        HardwareModel.loadSeats() :
        Promise.resolve();

      task
      .then(() => {
        let client = ClientDetector.detect(),
            device = {
              role: application === 'oms' ? 2 : 1,
              type: client.hardware,
              name: client.hardware_name,
              seat_token: null,
              password: generatePassword()
            };

        switch(application) {
          case 'snap':
            device.role = 1;
            break;
          case 'oms':
            device.role = 2;
            break;
          default:
            throw new Error('Invalid application: ' + application);
        }

        HardwareModel.currentDevice = device;

        DialogManager.endJob(job);
        $timeout(() => $scope.initialized = true);
      })
      .catch(onError);
    }

    function generatePassword() {
      var chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
      var length = 8;
      var result = '';
      for (var i = length; i > 0; --i) result += chars[Math.round(Math.random() * (chars.length - 1))];
      return result;
    }

    $scope.addSeat = function() {
      DialogManager.showDialog(
        'SeatEditController',
        require('../hardware/seatedit.html'), {
          seat: {}
        })
      .then(seat => {
        if (seat) {
          $timeout(() => {
            $scope.device.seat_token = seat.token;
            $scope.seats.push(seat);
          });
        }
      });
    };

    $scope.submitRegistration = function() {
      function proceed() {
        let job = DialogManager.startJob();

        let task = !$scope.device.token ?
          HardwareModel.registerDevice() :
          Promise.resolve();

        task
        .then(() => {
          HardwareModel.confirmAccess()
          .catch(e => {
            DialogManager.endJob(job);
            DialogManager.alert(e.message || e.error_description)
            .then(() => HardwareModel.rejectAccess(e));
          });
        })
        .catch(e => {
          DialogManager.endJob(job);
          DialogManager.alert(e.message);
        });
      }

      if (application === 'snap' && !$scope.device.seat_token) {
        DialogManager.confirm("Are you sure that you don't want to assign this device to any table?")
        .then(proceed);
      }
      else {
        proceed();
      }
    };

    $scope.cancelRegistration = function() {
      DialogManager.confirm('Are you sure that you want to cancel the registration?')
      .then(() => {
        DialogManager.startJob();
        HardwareModel.rejectAccess();
      });
    };
  }
}

SignupController.$inject = ['$scope', '$timeout', 'Options', 'ClientDetector', 'DialogManager', 'ShellManager', 'HardwareModel', 'UserModel'];
