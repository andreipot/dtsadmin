'use strict';

export default
  ['DialogManager', 'ModalService', 'ShellManager',
  (DialogManager, ModalService, ShellManager) => {
  return {
    restrict: 'E',
    replace: true,
    scope: {
    },
    template: require('./modal.html'),
    link: ($scope, attrs) => {
      var alertStack = [];
      var confirmStack = [];
      var alertIndex = -1;
      var confirmIndex = -1;
      var busyDialog, alertDialog, confirmDialog = null;

      ////////////////////////////////////////////
      // Alerts
      ////////////////////////////////////////////

      var showNextAlert = function() {
        if (alertDialog) {
          return;
        }

        alertIndex++;

        if (alertIndex === alertStack.length) {
          alertStack = [];
          alertIndex = -1;
          return;
        }

        let data = alertStack[alertIndex];

        ModalService.showModal({
          template: require('../../controllers/dialog/alert.html'),
          controller: 'AlertController',
          inputs: { data: data }
        }).then(modal => {
          modal.element.modal({
            backdrop: 'static',
            keyboard: false,
            replace: false
          });
          alertDialog = modal.close;
          alertDialog.then(result => {
            if (data.resolve) {
              data.resolve();
            }

            alertDialog = null;
            showNextAlert();
          });
        });
      };

      DialogManager.alertRequested.add((data, resolve) => {
        let title = data.title ? ShellManager.getString('dialog', data.title, data.titleArgs) : null,
            message = data.message ? ShellManager.getString('dialog', data.message, data.messageArgs) : null;

        alertStack.push({ title: title, message: message, resolve: resolve });
        showNextAlert();
      });

      ////////////////////////////////////////////
      // Confirmations
      ////////////////////////////////////////////

      var showNextConfirm = function() {
        if (confirmDialog) {
          return;
        }

        confirmIndex++;

        if (confirmIndex === confirmStack.length) {
          confirmStack = [];
          confirmIndex = -1;
          return;
        }

        let data = confirmStack[confirmIndex];

        ModalService.showModal({
          template: require('../../controllers/dialog/confirm.html'),
          controller: 'ConfirmController',
          inputs: { data: data }
        }).then(modal => {
          $(modal.element[0]).modal({
            backdrop: 'static',
            keyboard: false,
            replace: false
          });
          confirmDialog = modal;
          modal.close.then(result => {
            if (data.resolve) {
              data.resolve(result);
            }

            confirmDialog = null;
            showNextConfirm();
          });
        });
      };

      DialogManager.confirmRequested.add((data, resolve, reject) => {
        let message = data.message ? ShellManager.getString('dialog', data.message, data.messageArgs) : null;

        confirmStack.push({ message: message, resolve: resolve, reject: reject });
        showNextConfirm();
      });

      ////////////////////////////////////////////
      // Jobs
      ////////////////////////////////////////////

      DialogManager.jobStarted.add(() => {
        if (busyDialog) {
          return;
        }

        if (document.activeElement !== document.body) {
          document.activeElement.blur();
        }

        ModalService.showModal({
          template: require('../../controllers/dialog/progress.html'),
          controller: 'ProgressController'
        }).then(modal => {
          $(modal.element[0]).modal({
            backdrop: 'static',
            keyboard: false,
            replace: false
          });
          busyDialog = modal;
        });
      });

      DialogManager.jobEnded.add(() => {
        busyDialog = null;
      });

      ////////////////////////////////////////////
      // Modal dialog
      ////////////////////////////////////////////

      DialogManager.dialogRequested.add((data, resolve) => {
        ModalService.showModal({
          template: data.template,
          controller: data.controller,
          inputs: data.input
        }).then(modal => {
          $(modal.element[0]).modal({
            backdrop: 'static',
            keyboard: false,
            replace: false
          });
          modal.close
          .then(resolve)
          .then(() => $(modal.element[0]).modal('hide'));
        });
      });

      DialogManager.dialogClosed.add((handle, result) => {
        handle(result, 0);
      });
    }
  }
}];
