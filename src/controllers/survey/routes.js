'use strict';

export default function routes($routeProvider) {
  $routeProvider.when('/survey', { template: require('./survey.html'), controller: 'SurveyController' });
}

routes.$inject = ['$routeProvider'];
