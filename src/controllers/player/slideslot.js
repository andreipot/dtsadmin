'use strict';

export default
  ['LegacyApi', 'DialogManager', 'ShellManager', '$timeout',
  (LegacyApi, DialogManager, ShellManager, $timeout) => {
  return {
    restrict: 'E',
    replace: false,
    scope: {
      slot: '@',
      slide: '=',
      layout: '@',
      type: '@'
    },
    template: require('./slideslot.html'),
    link: function(scope, attrs) {
      scope.entity = {
        layout: scope.layout || 'wide',
        type: scope.type || scope.slide[`slot${scope.slot}_type`],
        content: scope.slide[`slot${scope.slot}_content`]
      };

      scope.edit = () => {
        DialogManager.showDialog(
          'SlotEditController',
          require('./slotedit.html'), {
            entity: {
              type: scope.entity.type,
              content: scope.entity.content
            }
          })
        .then(result => {
          if (result) {
            $timeout(() => {
              scope.entity.type = scope.slide[`slot${scope.slot}_type`] = result.type;
              scope.entity.content = scope.slide[`slot${scope.slot}_content`] = result.content;
            });
          }
        });
      };
    }
  };
}]
