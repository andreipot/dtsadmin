'use strict';

import angular from 'angular';
import angular_route from 'angular-route';

import routes from './routes';
import PresentationController from './presentation';
import PresentationEditController from './presentationedit';
import SlideController from './slide';
import SlideEditController from './slideedit';
import SlotEditController from './slotedit';

import slideslot from './slideslot';

export default angular.module('TouchAdmin.controllers.player', [ angular_route ])
  .config(routes)
  .controller('PresentationController', PresentationController)
  .controller('PresentationEditController', PresentationEditController)
  .controller('SlideController', SlideController)
  .controller('SlideEditController', SlideEditController)
  .controller('SlotEditController', SlotEditController)
  .directive('slideslot', slideslot)
  .name;
