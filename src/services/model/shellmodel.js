import AbstractModel from './abstract';

export default class ShellModel extends AbstractModel {
  constructor(Logger, StorageProvider) {
    super(StorageProvider);

    this._Logger = Logger;

    this._locales = {
      'en_US': require('../locale/en_US.json')
    };
  }

  getString(name, args) {
    if (!this._locales[this.locale]) {
      return name;
    }

    let value = this._locales[this.locale][name];

    if (!value) {
      return name;
    }

    return value;
  }
};
