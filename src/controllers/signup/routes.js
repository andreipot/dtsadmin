'use strict';

export default function routes($routeProvider) {
  $routeProvider.when('/', { template: require('./signup.html'), controller: 'SignupController' });
}

routes.$inject = ['$routeProvider'];
