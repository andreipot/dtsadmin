'use strict';

import angular from 'angular';
import angular_route from 'angular-route';

import routes from './routes';
import SignupController from './signup';
import MainSignupController from './main';

export default angular.module('TouchAdmin.controllers.signup', [ angular_route ])
  .config(routes)
  .controller('SignupController', SignupController)
  .controller('MainSignupController', MainSignupController)
  .name;
