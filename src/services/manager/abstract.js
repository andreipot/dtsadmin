export default class AbstractManager {
  constructor(Logger) {
    this._Logger = Logger;
  }

  initialize() {
    this._Logger.debug(`Initializing ${this.constructor.name}...`);

    return Promise.resolve();
  }
};
