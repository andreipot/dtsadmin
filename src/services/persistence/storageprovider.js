import LocalStorageStore from './store.localstorage';

export default class StorageProvider {
  constructor() {
  }

  getStorage(id) {
    return new LocalStorageStore(this._getName(id));
  }

  getSessionStorage(id) {
    return new LocalStorageStore(this._getName(id), true);
  }

  _getName(id) {
    return `admin_${id}`;
  }
};
