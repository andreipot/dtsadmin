'use strict';

import Application from './application';

import controllers_login from '../controllers/login/';

class LoginApplication extends Application {
  constructor() {
    super([
      controllers_login
    ]);
  }
};

export default new LoginApplication();
