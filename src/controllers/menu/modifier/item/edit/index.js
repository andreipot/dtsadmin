import Controller from '../../../../controller';

export default class MenuModifierItemEditController extends Controller {
  constructor($scope, $timeout, DialogManager, Logger, MenuManager, UserModel, data, close) {
    super($scope, $timeout, DialogManager, Logger, UserModel);

    this.MenuManager = MenuManager;
    this.$scope.modifier = data.modifier;
    this.$scope.item = data.item;
    this.close = close;

    this.$scope.currency = this.MenuManager.model.currency;

    this._defineMethod('save');
    this._defineMethod('cancel');
    this._defineMethod('delete');
  }

  async save() {
    const job = this.DialogManager.startJob();

    try {
      const item = $.extend(true, {
        order: {
          name: this.$scope.item.title,
          price: 0
        }
      }, this.$scope.item);

      if (!this.$scope.modifier.items) {
        this.$scope.modifier.items = [];
      }

      if (item.token) {
        let i = this.$scope.modifier.items.reduce((res, it, i) => {
          if (it.token === item.token) {
            res = i;
          }

          return res;
        }, -1);

        if (i !== -1) {
          this.$scope.modifier.items[i] = item;
        }

        let result = await this.MenuManager.updateModifier(this.$scope.modifier);
        this.DialogManager.closeDialog(this.close, result);
      }
      else {
        this.$scope.modifier.items.push(item);
        let result = await this.MenuManager.updateModifier(this.$scope.modifier);
        this.DialogManager.closeDialog(this.close, result);
      }
    }
    finally {
      this.DialogManager.endJob(job);
    }
  }

  async cancel() {
    this.DialogManager.closeDialog(this.close);
  }

  async delete() {
    const confirmed = await this.DialogManager.confirm('Are you sure that you want to delete this modifier?');

    if (!confirmed) {
      return;
    }

    const job = this.DialogManager.startJob();

    try {
      let i = this.$scope.modifier.items.reduce((res, item, i) => {
        if (item.token === this.$scope.item.token) {
          res = i;
        }

        return res;
      }, -1);

      if (i !== -1) {
        this.$scope.modifier.items.splice(i, 1);
      }

      await this.MenuManager.updateModifier(this.$scope.modifier);
      this.DialogManager.closeDialog(this.close, this.$scope.modifier);
    }
    finally {
      this.DialogManager.endJob(job);
    }
  }
}

MenuModifierItemEditController.$inject = ['$scope', '$timeout', 'DialogManager', 'Logger', 'MenuManager', 'UserModel', 'data', 'close'];
