'use strict';

export default class StreamsController {
  constructor($scope, $timeout, $location, DtsApi, DialogManager, UserModel) {
    var getData = function() {
      if (!UserModel.location) {
        return;
      }

      let job = DialogManager.startJob();

      DtsApi.api.request('waiter', 'waiterStreamList')
      .then(streams => {
        DialogManager.endJob(job);
        $timeout(() => $scope.streams = streams);
      })
      .catch(e => {
        DialogManager.endJob(job);
        DialogManager.alert(e.message);
      });
    };

    UserModel.locationChanged.add(getData);
    getData();

    $scope.$on('$destroy', () => UserModel.locationChanged.remove(getData));

    $scope.add = function() {
      DialogManager.showDialog(
        'StreamEditController',
        require('./streamedit.html'), {
        entity: {}
      })
      .then(function(result) {
        if (result) {
          getData();
        }
      });
    };

    $scope.edit = function(entity) {
      DialogManager.showDialog(
        'StreamEditController',
        require('./streamedit.html'), {
          entity: jQuery.extend({}, entity)
      })
      .then(function(result) {
        if (result) {
          getData();
        }
      });
    };
  }
}

StreamsController.$inject = ['$scope', '$timeout', '$location', 'DtsApi', 'DialogManager', 'UserModel'];
