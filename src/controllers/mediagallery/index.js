'use strict';

import angular from 'angular';
import angular_route from 'angular-route';

import routes from './routes';
import MediaGalleryController from './mediagallery';
import MediaGalleryMultiUploadController from './multiupload';
export default angular.module('TouchAdmin.controllers.mediagallery', [ angular_route ])
    .config(routes)
    .controller('MediaGalleryController', MediaGalleryController)
    .controller('MediaGalleryMultiUploadController', MediaGalleryMultiUploadController)
    .name;
