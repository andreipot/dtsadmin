'use strict';

export default ['ShellManager', (ShellManager) => {
  return (token, width, height, extension) => {
    if (!token)
      return null;

    return ShellManager.getMediaUrl(token, width, height, extension);
  };
}];
