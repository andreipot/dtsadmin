require('babel-register');

var pkg = require('./package.json');
module.exports = require('./webpack.make')({
  BUILD: true,
  TEST: false
});
