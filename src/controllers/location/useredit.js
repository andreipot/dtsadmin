'use strict';

export default class UserEditController {
  constructor($scope, close, DialogManager, LegacyApi, user) {
    $scope.user = user;

    $scope.cancel = function() {
      DialogManager.closeDialog(close);
    };

    $scope.save = function() {
      var job = DialogManager.startJob();

      var task = user.token ?
        LegacyApi.admin.updateUser(user.token, $scope.user) :
        LegacyApi.admin.createUser($scope.user);

      task.then(function(result) {
        if (result.token) {
          $scope.user.token = result.token;
        }

        DialogManager.endJob(job);
        DialogManager.closeDialog(close, $scope.user);
      }, function(e) {
        DialogManager.endJob(job);
        DialogManager.alert(e.message);
      });
    };

    $scope.delete = function() {
      DialogManager.confirm('Are you sure that you want to remove the user?').then(function() {
        var job = DialogManager.startJob();

        LegacyApi.admin.deleteUser(user.token).then(function() {
          DialogManager.endJob(job);
          DialogManager.closeDialog(close, $scope.user);
        }, function(e) {
          DialogManager.endJob(job);
          DialogManager.alert(e.message);
        });
      });
    };
  }
}

UserEditController.$inject = ['$scope', 'close', 'DialogManager', 'LegacyApi', 'user'];
