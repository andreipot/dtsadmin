'use strict';

import angular from 'angular';
import angular_route from 'angular-route';

import routes from './routes';
import HardwareController from './hardware';
import DeviceEditController from './deviceedit';
import SeatEditController from './seatedit';

export default angular.module('TouchAdmin.controllers.hardware', [ angular_route ])
  .config(routes)
  .controller('HardwareController', HardwareController)
  .controller('DeviceEditController', DeviceEditController)
  .controller('SeatEditController', SeatEditController)
  .name;
