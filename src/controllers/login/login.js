'use strict';

export default class LoginController {
  constructor($scope, $timeout, AccessManager, CommandStartup, DialogManager, LocationManager, ShellManager) {
    let job = DialogManager.startJob();

    CommandStartup().then(() => {
      job = DialogManager.endJob(job);
    });

    $scope.credentials = {};

    $scope.login = () => {
      $scope.error = null;
      $scope.busy = DialogManager.startJob();

      AccessManager
      .login($scope.credentials.username, $scope.credentials.password)
      .then(() => {
        if (!LocationManager.loadSavedLocation()) {
          LocationManager.load('panel');
        }
      }).catch(e => {
        DialogManager.endJob($scope.busy);

        $timeout(() => {
          $scope.busy = null;

          let str = 'LOGIN_ERROR_GENERIC';

          switch (e) {
            case 'invalid_grant':
              str = 'LOGIN_ERROR_INVALID_GRANT';
              break;
          }

          $scope.error = ShellManager.getString('ui', str);
        });
      });
    };
  }
}

LoginController.$inject = ['$scope', '$timeout', 'AccessManager', 'CommandStartup', 'DialogManager', 'LocationManager', 'ShellManager'];
