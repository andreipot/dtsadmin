'use strict';

export default ['$timeout', '$location', 'CommandLogout', 'CommandSwitchLocation', 'CommandSubmitSnap', 'UserModel', 'DialogManager', 'LegacyApi',
  ($timeout, $location, CommandLogout, CommandSwitchLocation, CommandSubmitSnap, UserModel, DialogManager, LegacyApi) => {
  return {
    restrict: 'E',
    replace: true,
    scope: {
      extended: '@'
    },
    template: require('./navigation.html'),
    link: ($scope, attrs) => {

      function applyData() {
        $timeout(() => {
          $scope.profile = UserModel.profile;
          $scope.location = UserModel.location;
          $scope.parentLocation = UserModel.locations.filter(loc => loc.token === UserModel.profile.location)[0];

          if ($scope.location === $scope.parentLocation)
            $scope.parentLocation = null;

          $scope.locations = UserModel.locations.filter(location => {
            return location.parent_token === $scope.location.token;
          });
        });
      };

      UserModel.profileChanged.add(applyData);
      applyData();

      $scope.switchLocation = function(location) {
        var job = DialogManager.startJob();

        $location.path('/');

        CommandSwitchLocation(location.token)
        .then(() => DialogManager.endJob(job))
        .catch(e => DialogManager.endJob(job));
      };

      $scope.userLogout = function() {
        DialogManager.startJob();
        CommandLogout();
      };

      $scope.submitSnap = () => {
        const deployment = DialogManager.showDialog(
          'DeploymentDialogController',
          require('../../controllers/deployment/dialog/template.html'),
          {}
        )
        .then(deployment => {
          if (!deployment) {
            return;
          }


        });
      };
    }
  }
}];
