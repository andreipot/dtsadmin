'use strict';

export default class AlertController {
  constructor($scope, $element, close, data) {
    $scope.data = data;

    $scope.close = () => {
      $element.modal('hide');
      close(null, 500);
    };
  }
}

AlertController.$inject = ['$scope', '$element', 'close', 'data'];
