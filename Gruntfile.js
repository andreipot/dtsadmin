module.exports = function(grunt) {
  require('load-grunt-tasks')(grunt);

  var resources = grunt.file.readJSON('resources.json');

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    bwr: grunt.file.readJSON('bower.json'),
    root: 'public',
    dist: '<%=root%>/dist/<%=pkg.version%>',
    temp: 'temp',
    concurrent: {
      dev: {
        tasks: ['exec:nodemon', 'watch'],
        options: { logConcurrentOutput: true }
      }
    },
    exec: {
      nodemon: 'nodemon'
    },
    watch: {
      assets: {
        files: ['assets/**/*'],
        tasks: ['buildassets'],
        options: {
          interrupt: true,
        }
      },
      css: {
        files: 'src/less/**/*.less',
        tasks: ['buildcss'],
        options: {
          interrupt: true,
        }
      },
      js: {
        files: ['src/js/**/*.js'],
        tasks: ['buildjs'],
        options: {
          interrupt: true,
        }
      },
      views: {
        files: ['src/views/**/*.jade'],
        tasks: ['buildviews'],
        options: {
          interrupt: true,
        }
      }
    },
    copy: {
      assets: {
        files: [
          { expand: true, cwd: 'assets/', src: ['**/*', '!*.*'], dest: '<%=dist%>/' },
          { expand: true, flatten: true, src: resources.fonts, dest: '<%=dist%>/fonts/' },
          { expand: true, cwd: 'assets/', src: ['*.*'], dest: '<%=root%>/' },
        ]
      }
    },
    clean: {
      root: '<%=root%>',
      temp: '<%=temp%>'
    },
    concat: {
      js: {
        files: {
          '<%=temp%>/admin.js': [
            'src/js/_base.js',
            'src/js/**/panel/**/*.js',
            'src/js/**/panel.js',
            'src/js/**/signup/**/*.js',
            'src/js/**/signup.js'
          ],
          '<%=temp%>/shared.js': [
            'src/js/*.js',
            'src/js/**/shared/**/*.js',
            'src/js/**/shared.js'
          ]
        }
      },
      css: {
        files: {
          '<%=temp%>/admin.less': 'src/less/*.less'
        }
      },
      libs: {
        files: {
          '<%=dist%>/js/libs.js': resources.js,
          '<%=dist%>/css/libs.css': resources.css
        }
      }
    },
    eslint: {
      options: {
        configFile: 'src/.eslintrc'
      },
      target: ['src/js/**/*.js']
    },
    babel: {
      options: {
        sourceMap: 'inline'
      },
      client: {
        files: {
          '<%=dist%>/js/shared.js': '<%=temp%>/shared.js',
          '<%=dist%>/js/admin.js': '<%=temp%>/admin.js'
        }
      }
    },
    uglify: {
      options: {
        mangle: false,
        sourceMap: true
      },
      dist: {
        files: {
          '<%=dist%>/js/admin.min.js': '<%=dist%>/js/admin.js',
          '<%=dist%>/js/shared.min.js': '<%=dist%>/js/shared.js',
          '<%=dist%>/js/libs.min.js': '<%=dist%>/js/libs.js'
        }
      }
    },
    less: {
      options: {
        cleancss: true,
        modifyVars: {
          'images': '"../images"',
          'fa-font-path': '"../fonts"'
        }
      },
      all: {
        files: {
          '<%=dist%>/css/admin.css': '<%=temp%>/admin.less'
        }
      }
    },
    jade: {
      views: {
        options: {
          pretty: true,
          data: {
            version: '<%=pkg.version%>'
          }
        },
        files: [{
          expand: true,
          cwd: 'src/views',
          src: ['*.jade', '!_*'],
          dest: '<%=root%>',
          ext: '',
          rename: function(dir, file) {
            if (file === 'index') {
              grunt.file.mkdir(dir);
              return dir + '/index.html';
            }

            grunt.file.mkdir(dir + '/' + file);
            return dir + '/' + file + '/index.html';
          }
        }]
      }
    },
    cssmin: {
      dist: {
        files: [{
          expand: true,
          cwd: '<%=dist%>/css/',
          src: ['*.css', '!*.min.css'],
          dest: '<%=dist%>/css/',
          ext: '.min.css'
        }]
      }
    },
    aws_s3: {
      options: {
        region: 'us-west-2',
        bucket: 'admin.managesnap.com',
        accessKeyId: '<% process.env.AWS_ACCESS_KEY_ID %>',
        secretAccessKey: '<% process.env.AWS_SECRET_ACCESS_KEY %>'
      },
      dist: {
  			files: [{
          expand: true,
          action: 'upload',
          cwd: '<%=root%>',
          src: ['**'],
  				dest: ''
  			}]
  		}
    },
    bump: {
      options: {
        files: ['package.json','bower.json'],
        updateConfigs: ['pkg', 'bwr'],
        commitFiles: ['-a'],
        commitMessage: 'Build #<%=pkg.version%>',
        tagName: '<%=pkg.version%>',
        commit: true,
        push: true,
        pushTo: 'origin',
        createTag: true
      }
    }
  });

  grunt.registerTask('default', []);
  grunt.registerTask('validate', ['eslint']);
  grunt.registerTask('build', ['clean', 'buildassets', 'buildlibs', 'buildcss_release', 'buildjs_release', 'buildviews']);
  grunt.registerTask('buildassets', ['copy:assets']);
  grunt.registerTask('buildlibs', ['concat:libs']);
  grunt.registerTask('buildcss_release', ['buildcss', 'cssmin']);
  grunt.registerTask('buildjs_release', ['buildjs', 'uglify']);
  grunt.registerTask('buildcss', ['concat:css', 'less']);
  grunt.registerTask('buildjs', ['concat:js', 'babel:client']);
  grunt.registerTask('buildviews', ['jade']);
  grunt.registerTask('run', ['concurrent']);
  grunt.registerTask('dev', ['build', 'concurrent']);
  grunt.registerTask('aws', ['aws_s3']);
  grunt.registerTask('release', ['validate', 'build', 'bump', 'aws']);
};
