'use strict';

import moment from 'moment';

export default class MediaGalleryController {
  constructor($scope, $timeout, $element, DtsApi, UserModel, options, close) {
    $scope.select = media => {
      $element.modal('hide');
      close({
        token: media.token,
        mime: media.mime
      }, 500);
    };

    $scope.cancel = () => {
      $element.modal('hide');
      close(null, 500);
    };

    const existing = UserModel.medias;

    if (existing && existing.length > 0) {
      return $timeout(() => $scope.medias = existing);
    }

    DtsApi.api.request('media', 'mediaList')
    .then(medias => $timeout(() => {
      $scope.medias = UserModel.medias = medias
        .filter(m => m.mime.startsWith('image/'))
        .sort((m1, m2) => {
          m1 = moment(m1.created);
          m2 = moment(m2.created);

          if (m1.isAfter(m2)) {
            return -1;
          }

          if (m2.isAfter(m1)) {
            return 1;
          }

          return 0;
        });
    }));
  }
}

MediaGalleryController.$inject = ['$scope', '$timeout', '$element', 'DtsApi', 'UserModel', 'options', 'close'];
