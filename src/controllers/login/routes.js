'use strict';

export default function routes($routeProvider) {
  $routeProvider.when('/', { template: require('./login.html'), controller: 'LoginController' });
}

routes.$inject = ['$routeProvider'];
