'use strict';

export default class MainSignupController {
  constructor(AccessManager, CommandStartup, CommandLoadProfile, CommandLogout, DialogManager) {
    let job = DialogManager.startJob();

    CommandStartup(true)
    .then(() => CommandLoadProfile())
    .then(() => DialogManager.endJob(job))
    .catch(e => DialogManager.endJob(job) && DialogManager.alert('STARTUP_ERROR'));

    AccessManager.accessDenied.add(CommandLogout);
  }
}

MainSignupController.$inject = ['AccessManager', 'CommandStartup', 'CommandLoadProfile', 'CommandLogout', 'DialogManager'];
