'use strict';

import angular from 'angular';

import bitwise from './bitwise';
import not from './not';
import placeholder from './placeholder';
import schedule from './schedule';
import thumbnail from './thumbnail';

export default angular.module('TouchAdmin.filters', [])
  .filter('bitwise', bitwise)
  .filter('not', not)
  .filter('placeholder', placeholder)
  .filter('schedule', schedule)
  .filter('thumbnail', thumbnail)
  .name;
