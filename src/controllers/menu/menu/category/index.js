import Controller from '../../../controller';

export default class MenuCategoryController extends Controller {
  constructor($scope, $timeout, $location, $routeParams, AssetManager, DialogManager, Logger, MenuManager, UserModel) {
    super($scope, $timeout, DialogManager, Logger, UserModel);

    this.$location = $location;
    this.$routeParams = $routeParams;
    this.AssetManager = AssetManager;
    this.MenuManager = MenuManager;

    $scope.onElementDrag = (el, index) => {
      // el.element.entity.position = index;
    };

    this._defineMethod('onElementDragComplete');

    this._defineMethod('deleteCategory');
    this._defineMethod('editCategory');
    this._defineMethod('addCategory');
    this._defineMethod('addItem');
    this._defineMethod('getDeviceName');
    this._defineMethod('editElement');
  }

  async getData() {
    const category_token = this.$routeParams.category_id;
    const menu = await this.MenuManager.loadMenu(this.$routeParams.id);
    const category = (menu.categories || []).filter(category => category.token === category_token)[0];
    const devices = await this.AssetManager.loadHardware();

    this.$timeout(() => {
      this.$scope.menu = menu;
      this.$scope.category = category;
      this.$scope.devices = devices;
      this.$scope.is_editable = menu.location === this.UserModel.location.token;
      this.$scope.is_subscribed = menu.parent != null && menu.location === this.UserModel.location.token;

      const categories = (menu.categories || [])
        .filter(category => category.parent === category_token)
        .sort(this._sort);
      const items = (menu.items || [])
        .filter(item => item.parent === category_token)
        .sort(this._sort);

      this.$scope.elements = categories
      .map(entity => {
        return {
          type: 'category',
          entity: entity
        }
      })
      .concat(
        items.map(entity => {
          return {
            type: 'item',
            entity: entity
          }
        })
      )
      .sort((a,b) => this._sort(a.entity, b.entity));

      const breadcrumbs = [{
        title: category.title
      }];

      let parent = category.parent;

      while (parent) {
        let data = menu.categories.filter(x => x.token === parent)[0];
        parent = data ? data.parent : null;

        if (data) {
          breadcrumbs.push({
            title: data.title,
            url: '/menu/set/' + menu.token + '/category/' + data.token
          });
        }
      }

      breadcrumbs.push({
        title: menu.title,
        url: '/menu/set/' + menu.token
      });

      this.$scope.breadcrumbs = breadcrumbs.reverse();
    });
  }

  async onElementDragComplete() {
    // if (!this.$scope.is_editable) {
    //   return;
    // }
    //
    // const tokens = this.$scope.elements
    //   .sort((a,b) => this._sort(a.entity, b.entity))
    //   .map(element => element.entity.token);
  }

  async deleteCategory() {
    const confirmed = await this.DialogManager.confirm('Are you sure that you want to remove the category?');

    if (!confirmed) {
      return;
    }

    const job = this.DialogManager.startJob();

    try {
      await this.MenuManager.deleteMenuCategory(this.$scope.menu, this.$scope.category);

      if (this.$scope.category.parent) {
        this.$location.path('/menu/set/' + this.$scope.menu.token + '/category/' + this.$scope.category.parent);
      }
      else {
        this.$location.path('/menu/set/' + this.$scope.menu.token);
      }
    }
    finally {
      this.DialogManager.endJob(job);
    }
  }

  async editCategory() {
    const category = await this.DialogManager.showDialog(
      'MenuCategoryEditController',
      require('./edit/template.html'), {
        data: {
          menu: this.$scope.menu,
          category: Object.assign({}, this.$scope.category)
        }
      });

    if (category) {
      this.$timeout(() => this.$scope.category = category);
    }
  }

  async addItem() {
    const category_token = this.$routeParams.category_id;
    const item = await this.DialogManager.showDialog(
      'MenuItemEditController',
      require('../item/edit/template.html'),
      {
        data: {
          menu: this.$scope.menu,
          item: {
            parent: category_token,
            order: { price: 0 }
          }
        }
      }
    );

    if (item) {
      await this.getData();
    }
  }

  async addCategory() {
    const category_token = this.$routeParams.category_id;
    const category = await this.DialogManager.showDialog(
      'MenuCategoryEditController',
      require('../category/edit/template.html'),
      {
        data: {
          menu: this.$scope.menu,
          category: {
            parent: category_token
          }
        }
      }
    );

    if (category) {
      await this.getData();
    }
  }

  getDeviceName(element) {
    if (!element || !this.$scope.devices) {
      return;
    }

    return this.$scope.devices
      .filter(x => x.token === element.entity.dmc_device)
      .map(x => x.name)[0] || 'not defined';
  }

  editElement(element) {
    if (!element || !element.type || !this.$scope.menu) {
      return;
    }

    this.$location.path(`/menu/set/${this.$scope.menu.token}/${element.type}/${element.entity.token}`);
  }
}

MenuCategoryController.$inject = ['$scope', '$timeout', '$location', '$routeParams', 'AssetManager', 'DialogManager', 'Logger', 'MenuManager', 'UserModel'];
