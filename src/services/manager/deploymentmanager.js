import AbstractManager from './abstract';

export default class DeploymentManager extends AbstractManager {
  constructor(DeploymentModel, DtsApi, Logger, UserModel) {
    super(Logger);

    this._DtsApi = DtsApi;
    this._DeploymentModel = DeploymentModel;
    this._UserModel = UserModel;
  }

  async initialize() {
    await super.initialize();
  }

  async reset() {
    this.model.deployments = {};
  }

  get model() {
    return this._DeploymentModel;
  }

  //-----------------------------------------------
  //    Deployment methods
  //-----------------------------------------------

  async loadDeployments() {
    return await this._DtsApi.api.request('deployment', 'deploymentList');
  }

  async loadDeployment(token) {
    if (!token) {
      throw new Error('Argument error: token');
    }

    const location = this._UserModel.location.token;
    const deployment = this.model.deployments[token] = this.model.deployments[token] ||
      (this.model.deployments[token] = JSON.parse(await this._DtsApi.cdn.load(`deployment/${location}/${token}.json`)));

    return deployment;
  }

  async createDeployment(digest) {
    if (!digest) {
      throw new Error('Argument error: digest');
    }

    return await this._DtsApi.api.request('deployment', 'deploymentCreate', { body: { digest: digest } });
  }

  async updateDeployment(deployment) {
    if (!deployment || !deployment.token) {
      throw new Error('Argument error: deployment');
    }

    return await this._DtsApi.api.request('deployment', 'deploymentUpdate', {
      token: deployment.token,
      body: deployment
    });
  }
};
