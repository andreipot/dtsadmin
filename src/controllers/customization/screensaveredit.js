'use strict';

export default class ScreensaverEditController {
  constructor($scope, close, DialogManager, LegacyApi, screensaver) {
    $scope.screensaver = screensaver;

    $scope.imageSelected = function(token) {
      $scope.screensaver.image = token;
    };

    $scope.videoSelected = function(token) {
      $scope.screensaver.video = token;
    };

    $scope.cancel = function() {
      DialogManager.closeDialog(close);
    };

    $scope.save = function() {
      var job = DialogManager.startJob();

      var task = screensaver.token ?
        LegacyApi.customization.updateScreensaver($scope.screensaver, screensaver.token) :
        LegacyApi.customization.saveScreensaver($scope.screensaver);

      task.then(function() {
        DialogManager.endJob(job);
        DialogManager.closeDialog(close, $scope.screensaver);
      }, function(e) {
        DialogManager.endJob(job);
        DialogManager.alert(e.message);
      });
    };

    $scope.delete = function() {
      DialogManager.confirm('Are you sure that you want to delete the screensaver?').then(function() {
        var job = DialogManager.startJob();

        LegacyApi.customization.deleteBackground(screensaver.token).then(function() {
          DialogManager.endJob(job);
          DialogManager.closeDialog(close);
        }, function(e) {
          DialogManager.endJob(job);
          DialogManager.alert(e.message);
        });
      });
    };
  }
}

ScreensaverEditController.$inject = ['$scope', 'close', 'DialogManager', 'LegacyApi', 'screensaver'];
