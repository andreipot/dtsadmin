import Controller from '../../controller';

export default class MenuAllergenController extends Controller {
  constructor($scope, $timeout, DialogManager, Logger, MenuManager, UserModel) {
    super($scope, $timeout, DialogManager, Logger, UserModel);

    this.MenuManager = MenuManager;

    this._defineMethod('createAllergen');
    this._defineMethod('editAllergen');
  }

  async getData() {
    const allergens = await this.MenuManager.loadAllergens();

    this.$timeout(() => {
      this.$scope.allergens = allergens;
    });
  }

  async createAllergen() {
    const result = await this.DialogManager.showDialog(
      'MenuAllergenEditController',
      require('./edit/template.html'), {
        data: {
          allergen: {}
        }
      }
    );

    if (result) {
      await this.getData();
    }
  }

  async editAllergen(allergen) {
    const result = await this.DialogManager.showDialog(
      'MenuAllergenEditController',
      require('./edit/template.html'), {
        data: {
          allergen: Object.assign({}, allergen)
        }
      }
    );

    if (result) {
      await this.getData();
    }
  }
}

MenuAllergenController.$inject = ['$scope', '$timeout', 'DialogManager', 'Logger', 'MenuManager', 'UserModel'];
