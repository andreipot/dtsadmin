import AbstractManager from './abstract';

export default class ShellManager extends AbstractManager {
  constructor(DtsApi, ShellModel, Logger) {
    super(Logger);

    this._DtsApi = DtsApi;
    this._ShellModel = ShellModel;

    this.locale = 'en_US';
  }

  get locale() {
    return this.model.locale;
  }

  set locale(value) {
    if (this.locale === value) {
      return;
    }

    this.model.locale = value;
  }

  get model() {
    return this._ShellModel;
  }

  getString(bundle, name, args) {
    return this.model.getString(name, args);
  }

  getMediaUrl(media, width, height, extension) {
    if (!media) {
      return null;
    }

    let token = media.token || media;

    switch (media.mime) {
      case 'image/jpeg':
        extension = 'jpg';
        break;
      case 'image/png':
        extension = 'png';
        break;
    }

    if (width && height) {
      token += '_' + width + '_' + height;
    }

    if (extension) {
      token += '.' + extension;
    }
    else {
      token += '.jpg';
    }

    return this._DtsApi.cdn.getUrl('version/' + token);
  }
};
