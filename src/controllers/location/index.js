'use strict';

import angular from 'angular';
import angular_route from 'angular-route';

import routes from './routes';
import LocationController from './location';
import LocationEditController from './locationedit';
import StreamsController from './streams';
import StreamEditController from './streamedit';
import UserEditController from './useredit';
import UsersController from './users';

export default angular.module('TouchAdmin.controllers.location', [ angular_route ])
  .config(routes)
  .controller('LocationController', LocationController)
  .controller('LocationEditController', LocationEditController)
  .controller('StreamsController', StreamsController)
  .controller('StreamEditController', StreamEditController)
  .controller('UserEditController', UserEditController)
  .controller('UsersController', UsersController)
  .name;
