import Controller from '../../../controller';

export default class MenuItemController extends Controller {
  constructor($scope, $timeout, $location, $routeParams, AssetManager, DtsApi, DialogManager, Logger, MenuManager, ShellManager, UserModel) {
    super($scope, $timeout, DialogManager, Logger, UserModel);

    this.token = $routeParams.id;
    this.item_token = $routeParams.item_id;

    this.$location = $location;
    this.AssetManager = AssetManager;
    this.DtsApi = DtsApi;
    this.MenuManager = MenuManager;
    this.ShellManager = ShellManager;

    this.$scope.currency = this.MenuManager.model.currency;

    this._defineMethod('save');
    this._defineMethod('delete');
    this._defineMethod('editAllergens');
    this._defineMethod('editModifiers');
  }

  async getData() {
    const menu = await this.MenuManager.loadMenu(this.token);
    const item = (menu.items || []).filter(item => item.token === this.item_token)[0];

    if (!item) {
      return;
    }

    const streams = item.type === 'orderable' ? await this.MenuManager.loadStreams() : null;
    const devices = item.type === 'orderable' || item.type === 'dmc' ? await this.AssetManager.loadHardware() : null;
    const allergens = item.type === 'orderable' || item.type === 'dmc' ? await this.MenuManager.loadAllergens() : [];
    const modifiers = item.type === 'orderable' || item.type === 'dmc' ? await this.MenuManager.loadModifiers() : [];

    this.$timeout(() => {
      let menuitem = $.extend(true, {}, item);

      if (!menuitem.order) {
        menuitem.order = { price: 0 };
      }

      if (!menuitem.order_refill) {
        menuitem.order_refill = { price: 0 };
      }
      else {
        menuitem.refill_enabled = true;
      }

      this.$scope.menu = menu;
      this.$scope.item = menuitem;
      this.$scope.streams = streams || [];
      this.$scope.devices = (devices || []).filter(x => x.role === 'snap');
      this.$scope.allergens = allergens;
      this.$scope.modifiers = modifiers;

      const breadcrumbs = [];

      let parent = item.parent;

      while (parent) {
        let data = menu.categories.filter(x => x.token === parent)[0];
        parent = data ? data.parent : null;

        if (data) {
          breadcrumbs.push({
            title: data.title,
            url: '/menu/set/' + menu.token + '/category/' + data.token
          });
        }
      }

      breadcrumbs.push({
        title: menu.title,
        url: '/menu/set/' + menu.token
      });

      this.$scope.breadcrumbs = breadcrumbs.reverse();
    });
  }

  async save() {
    const job = this.DialogManager.startJob();
    const item = this.$scope.item,
          menu = this.$scope.menu;

    try {
      if (item.dmc_device === '') {
        item.dmc_device = null;
      }

      if (!item.refill_enabled) {
        item.order_refill = null;
      }
      else if (!item.order_refill.name) {
        item.order_refill.name = item.title + ' (refill)';
      }

      if (!item.order.name) {
        item.order.name = item.title;
      }

      if (item.order && item.order.stream === '') {
        item.order.stream = null;
      }

      if (item.order && item.order_refill) {
        item.order_refill.stream = item.order.stream;
      }

      await this.MenuManager.updateMenuItem(menu, item);
      await this.getData();
    }
    finally {
      this.DialogManager.endJob(job);
    }
  }

  async delete() {
    const confirmed = await this.DialogManager.confirm('Are you sure that you want to remove the item?');

    if (!confirmed) {
      return;
    }

    const job = this.DialogManager.startJob();

    try {
      await this.MenuManager.deleteMenuItem(this.$scope.menu, this.$scope.item);

      if (this.$scope.item.parent) {
        this.$location.path('/menu/set/' + this.token + '/category/' + this.$scope.item.parent);
      }
      else {
        this.$location.path('/menu/set/' + this.token);
      }
    }
    finally {
      this.DialogManager.endJob(job);
    }
  }

  async editAllergens() {
    const result = await this.DialogManager.showDialog('AllergensEditController', require('../../../dialog/allergensedit.html'), {
      data: {
        allergens: this.$scope.allergens,
        item: $.extend(true, { allergens: [] }, this.$scope.item)
      }
    });

    if (result) {
      this.$scope.item.allergens = result.allergens;
    }
  }

  async editModifiers() {
    const result = await this.DialogManager.showDialog('ModifiersEditController', require('../../../dialog/modifiersedit.html'), {
      data: {
        modifiers: this.$scope.modifiers,
        item: $.extend(true, { modifiers: [] }, this.$scope.item)
      }
    });

    if (result) {
      this.$scope.item.modifiers = result.modifiers;
    }
  }
}

MenuItemController.$inject = ['$scope', '$timeout', '$location', '$routeParams', 'AssetManager', 'DtsApi', 'DialogManager', 'Logger', 'MenuManager', 'ShellManager', 'UserModel'];
