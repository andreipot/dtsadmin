var pattern = /(SNAP|OMS)\/([0-9.]+) \(([^;]+);([^;]+);([^;]+)\)/;

//------------------------------------------------------------------------
//
//  ClientDetector
//
//------------------------------------------------------------------------

var ClientDetector = function() {
  this._parameters = (function(params) {
    var result = {};

    if (params === '') {
      return result;
    }

    for (var i = 0; i < params.length; i++) {
      var p = params[i].split('=', 2);
      result[p[0]] = p.length !== 1 ?
      decodeURIComponent(p[1].replace(/\+/g, " ")) :
      '';
    }

    return result;
  })(window.location.search.substr(1).split('&'));
};

ClientDetector.prototype.detect = function(userAgent) {
  var agent = userAgent || navigator.userAgent || '',
      result = {
        application: 0,
        hardware: 31, //Web browser
        version: null,
        hardware_name: 'New Device',
        hardware_code: this._parameters.hardware_code || agent
      },
      match = pattern.exec(agent);

  if (match) {
    //Application
    switch (match[1]) {
      case 'SNAP':
        result.application = 1;
        break;
      case 'OMS':
        result.application = 2;
        break;
    }

    //Version
    result.version = match[2];

    //Hardware type
    switch (match[3].toLowerCase()) {
      case 'windows':
        result.hardware = 1;
        break;
      case 'iosphone':
        result.hardware = 11;
        break;
      case 'iostablet':
        result.hardware = 12;
        break;
      case 'androidphone':
        result.hardware = 21;
        break;
      case 'androidtablet':
        result.hardware = 22;
        break;
      case 'androidpanel':
        result.hardware = 23;
        break;
    }

    //Hardware info
    result.hardware_name = match[4];
    result.hardware_code = match[5];
  }
  else {
    if (/(iPhone|iPod)/gi.test(agent)) {
      result.hardware = 11;
    }
    else if (/(iPad)/gi.test(agent)) {
      result.hardware = 12;
    }
    else if (/(Android)/gi.test(agent)) {
      result.hardware = 22; //Android tablet
    }
  }

  result.hardware_code = result.hardware_code.substring(0, 128);

  return result;
};

export default ClientDetector;
