import Controller from '../../../controller';

export default class MenuModifierCategoryController extends Controller {
  constructor($scope, $timeout, $location, $routeParams, DialogManager, Logger, MenuManager, UserModel) {
    super($scope, $timeout, DialogManager, Logger, UserModel);

    this.$location = $location;
    this.$routeParams = $routeParams;
    this.MenuManager = MenuManager;

    this._defineMethod('editModifier');
    this._defineMethod('addItem');
    this._defineMethod('editItem');
    this._defineMethod('deleteModifier');
  }

  async getData() {
    const token = this.$routeParams.id;
    const modifiers = await this.MenuManager.loadModifiers();
    const modifier = modifiers.filter(x => x.token === token)[0];

    if (!modifier) {
      return;
    }

    this.$timeout(() => {
      this.$scope.modifier = modifier;
    });
  }

  async addItem() {
    const modifier = await this.DialogManager.showDialog(
      'MenuModifierItemEditController',
      require('../item/edit/template.html'), {
        data: {
          modifier: $.extend(true, {}, this.$scope.modifier),
          item: {
            order: { price: 0 },
            selection: 'toggle'
          }
        }
      });

    if (modifier) {
      await this.getData();
    }
  }

  async editItem(item) {
    const modifier = await this.DialogManager.showDialog(
      'MenuModifierItemEditController',
      require('../item/edit/template.html'), {
        data: {
          modifier: $.extend(true, {}, this.$scope.modifier),
          item: $.extend(true, {}, item)
        }
      });

    if (modifier) {
      await this.getData();
    }
  }

  async editModifier() {
    const modifier = await this.DialogManager.showDialog(
      'MenuModifierCategoryEditController',
      require('./edit/template.html'), {
        data: {
          modifier: $.extend(true, {}, this.$scope.modifier)
        }
      });

    if (modifier) {
      await this.getData();
    }
  }

  async deleteModifier() {
    const confirmed = await this.DialogManager.confirm('Are you sure that you want to remove the modifier category?');

    if (!confirmed) {
      return;
    }

    const job = this.DialogManager.startJob();

    try {
      await this.MenuManager.deleteModifier(this.$scope.modifier);
      this.$location.path('/menu/modifier');
    }
    finally {
      this.DialogManager.endJob(job);
    }
  }
}

MenuModifierCategoryController.$inject = ['$scope', '$timeout', '$location', '$routeParams', 'DialogManager', 'Logger', 'MenuManager', 'UserModel'];
