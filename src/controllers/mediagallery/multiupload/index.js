import Controller from '../../controller';

export default class MediaGalleryMultiUploadController extends Controller {
    constructor($scope, $timeout, $location, $routeParams, AssetManager, DialogManager, Logger, MenuManager, UserModel, Upload, ShellManager, DtsApi) {
        super($scope, $timeout, DialogManager, Logger, UserModel);

        this.$location = $location;
        this.$routeParams = $routeParams;
        this.AssetManager = AssetManager;
        this.MenuManager = MenuManager;

        //set some media types
        $scope.mediatype = '*/*';
        $scope.category = '';
        //empty array of files
        $scope.files = [];

        //upload selected & allowed files
        $scope.uploadFiles = (files, errFiles) => {

            let category = ($scope.category.match("^[a-zA-Z0-9]*$") && $scope.category != null ) ? $scope.category : null;

            if(files && files.length){
                var job = DialogManager.startJob();
                job.progress = 0;
                Promise.all(files.map(file => {
                    return DtsApi.upload.put(file, progress => job.progress = progress / files.length)
                        .then(uploadKey => DtsApi.api.request('media', 'mediaCreate', {
                            body: { upload: uploadKey, category: category }
                        }))
                        .then(media => new Promise((resolve, reject) => {
                            let i = 0;
                            const limit = 10;

                            function checkStatusLater() {
                                $timeout(() => {
                                    i++;
                                    if (i === limit) {
                                        return reject();
                                    }
                                    DtsApi.api.request('media', 'mediaRead', { token: media.token })
                                        .then((media) => {
                                            if (media.status === 'ready') {
                                                resolve({
                                                    token: media.token,
                                                    mime: media.mime,
                                                    category:media.category
                                                });
                                            }
                                            else if (media.status === 'error') {
                                                reject();
                                            }
                                            else {
                                                checkStatusLater();
                                            }
                                        })
                                        .catch(reject);
                                }, 500);
                            }
                            checkStatusLater();
                        }))
                        .catch(e => {
                            console.log('individual error'+ e);
                            reject(e);
                        });
                }))
                .then(medias => {
                    DialogManager.endJob(job);
                   /* $timeout(() => {
                        $scope.binary = media.mime.indexOf('application') !== -1;
                        $scope.media = media;
                        $scope.mediatoken = media.token;
                        $scope.loaded = false;

                        if ($scope.onmediaselected) {
                            $scope.onmediaselected(media.token);
                        }

                        if ($scope.onmedia) {
                            $scope.onmedia(media);
                        }
                    });*/
                })
                .catch(e => {
                    DialogManager.endJob(job);
                    DialogManager.alert(`Upload error: ${JSON.stringify(e)}`);
                });
            }
        };

        $scope.cancel = function() {
            DialogManager.closeDialog(close);
        };

        $scope.save = function() {
            var job = DialogManager.startJob();

            DialogManager.endJob(job);
            DialogManager.closeDialog(close, $scope.advertisement);
        };
        $scope.onImageLoaded = () => {
            $scope.loaded = true;
        };

        $scope.onImageError = () => {
            $scope.loaded = true;
        };

        if (!$scope.mediatype) {
            $scope.mediatype = 'image/*';
        }

        if ($scope.mediatype.indexOf('application') !== -1) {
            $scope.mediawidth = $scope.mediaheight = undefined;
            $scope.binary = true;
        }
        else {
            $scope.binary = false;
        }
    }

}

MediaGalleryMultiUploadController.$inject = ['$scope', '$timeout', '$location', '$routeParams', 'AssetManager', 'DialogManager', 'Logger', 'MenuManager', 'UserModel', 'Upload', 'ShellManager', 'DtsApi'];
