'use strict';

export default class SurveyQuestionEditController {
  constructor($scope, close, DialogManager, LegacyApi, UserModel, question) {
    $scope.question = question;

    $scope.cancel = function() {
      DialogManager.closeDialog(close);
    };

    $scope.save = function() {
      var job = DialogManager.startJob();

      var task = question.token ?
        undefined :
        LegacyApi.advertisement.createSurveyQuestion(question.survey_token, $scope.question);

      task.then(function(result) {
        if (result.token) {
          $scope.question.token = result.token;
        }

        DialogManager.endJob(job);
        DialogManager.closeDialog(close, $scope.question);
      }, function(e) {
        DialogManager.endJob(job);
        DialogManager.alert(e.message);
      });
    };
  }
}

SurveyQuestionEditController.$inject = ['$scope', 'close', 'DialogManager', 'LegacyApi', 'UserModel', 'question'];
