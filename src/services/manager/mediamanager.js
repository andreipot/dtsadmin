import AbstractManager from './abstract';
import ObjectId from './../model/objectid';

export default class MediaManager extends AbstractManager {
  constructor(DtsApi, Logger, MediaModel) {
    super(Logger);

    this._DtsApi = DtsApi;
    this._MediaModel = MediaModel;
  }

  async initialize() {
    await super.initialize();
  }

  async reset() {
    this.model.medias = {};
  }

  get model() {
    return this._MediaModel;
  }

  generateID() {
    return (new ObjectId()).toString();
  }

  //-----------------------------------------------
  //    media methods
  //-----------------------------------------------

  async loadMedias() {
    return await this._DtsApi.api.request('media', 'mediaList');
  }

  async loadMedia(token) {
    const media = this.model.medias[token] = this.model.medias[token] ||
      (this.model.medias[token] = await this._DtsApi.api.request('media', 'mediaRead', { token: token }));

    return media;
  }



  async createMedia(media,category) {
    this._Logger.debug(`Creating media...`);

    try {
      let result = await this._DtsApi.api.request('media', 'mediaCreate', { body: media , category: category});
      //this.model.medias[result.token] = result;
      return result;
    }
    catch (e) {
      this._Logger.warn(`Unable to create media.`, e);
      throw e;
    }
  }

  async uploadMedia(media, category){
    this._Logger.debug(`upload media...`);

    try {
      this._DtsApi.upload.put(media, process => {})
      .then(uploadKey => this.createMedia(media, category))
      let result = await this._DtsApi.api.request('media', 'mediaCreate', { body: media });
      this.model.medias[result.token] = result;

      return result;
    }
    catch (e) {
      this._Logger.warn(`Unable to upload media.`, e);
      throw e;
    }
  }
  async updateMedia(media) {
    this._Logger.debug(`Updating media ${media.token}...`);

    if (media.items) {
      await this._syncEntries(media.items, []);
    }

    try {
      let result = await this._DtsApi.api.request('media', 'mediaUpdate', { body: media, token: media.token });
      this.model.medias[result.token] = result;

      return result;
    }
    catch (e) {
      this._Logger.warn(`Unable to update media ${media.token}.`, e);
      throw e;
    }
  }

  async duplicateMedia(media) {
    this._Logger.debug(`Duplicating media ${media.token}...`);

    let copy = $.extend(true, {}, media);

    delete copy.token;

    return copy;
  }

  async deleteMedias(tokens) {
    this._Logger.debug(`Deleting medias ...`);

    try {
      let items = (tokens).map(x => this.deleteMedia({token: x}));

      await Promise.all(items);
    }
    catch (e) {
      this._Logger.warn(`Unable to delete childs of medias.`, e);
      throw e;
    }
  }

  async deleteMedia(media) {
    this._Logger.debug(`Deleting media item ${media.token} from media ${media.token}...`);

    try {
      await this._DtsApi.api.request('media', 'mediaDelete', {  token: media.token });
      this.model.medias = this.model.medias.filter(i => i.token !== media.token);
    }
    catch (e) {
      this._Logger.warn(`Unable to delete media ${media.token}.`, e);
      throw e;
    }
  }
};
