import Controller from '../controller';

export default class AllergensEditController extends Controller {
  constructor($scope, $timeout, DialogManager, Logger, MenuManager, UserModel, data, close) {
    super($scope, $timeout, DialogManager, Logger, UserModel);

    this.MenuManager = MenuManager;
    this.$scope.allergens = data.allergens;
    this.$scope.item = data.item;
    this.close = close;

    this.$scope.items = data.allergens.map(allergen => {
      return {
        selected: data.item.allergens.indexOf(allergen.token) !== -1,
        allergen: allergen
      }
    })

    this.$scope.cancel = () => {
      this.DialogManager.closeDialog(close);
    };

    this._defineMethod('save');
    this._defineMethod('toggle');
  }

  async save() {
    this.$scope.item.allergens = this.$scope.items
    .filter(x => x.selected)
    .map(x => x.allergen.token);

    this.DialogManager.closeDialog(this.close, this.$scope.item);
  }

  toggle(item) {
    item.selected = !item.selected;
  }
}

AllergensEditController.$inject = ['$scope', '$timeout', 'DialogManager', 'Logger', 'MenuManager', 'UserModel', 'data', 'close'];
