'use strict';

export default class SeatEditController {
  constructor($scope, $timeout, close, DialogManager, UserModel, LegacyApi, seat) {
    $scope.seat = seat;

    LegacyApi.customization.getElements().then(function(elements) {
      $timeout(function() {
        var element = elements.filter(function(el) { return el.type === 10; })[0];

        if (element) {
          $scope.locationMap = element.image;
        }
      });
    });

    $scope.onMapClick = function(event) {
      $scope.seat.map_position_x = event.offsetX / 512;
      $scope.seat.map_position_y = event.offsetY / 384;
    };

    $scope.cancel = function() {
      DialogManager.closeDialog(close);
    };

    $scope.save = function() {
      var job = DialogManager.startJob();

      var task = $scope.seat.token ?
        LegacyApi.location.updateSeat($scope.seat.token, $scope.seat) :
        LegacyApi.location.createSeat($scope.seat);

      task.then(function(result) {
        if (!$scope.seat.token) {
          $scope.seat.token = result.token;
        }

        DialogManager.endJob(job);
        DialogManager.closeDialog(close, $scope.seat);
      }, function(e) {
        DialogManager.endJob(job);
        DialogManager.alert(e.message);
      });
    };

    $scope.delete = function() {
      DialogManager.confirm('Are you sure that you want to delete the table?').then(function() {
        var job = DialogManager.startJob();

        LegacyApi.location.deleteSeat($scope.seat.token).then(function() {
          DialogManager.endJob(job);
          DialogManager.closeDialog(close, $scope.seat);
        }, function(e) {
          DialogManager.endJob(job);
          DialogManager.alert(e.message);
        });
      });
    };
  }
};

SeatEditController.$inject = ['$scope', '$timeout', 'close', 'DialogManager', 'UserModel', 'LegacyApi', 'seat'];
