'use strict';

export default function routes($routeProvider) {
  $routeProvider.when('/', { template: require('./home.html'), controller: 'HomeController' });
}

routes.$inject = ['$routeProvider'];
