'use strict';

export default function routes($routeProvider) {
  $routeProvider.when('/menu/set', { template: require('./menu/template.html'), controller: 'MenuController' });
  $routeProvider.when('/menu/loader', { template: require('./spreadsheet/template.html'), controller: 'MenuSpreadsheetController' });
  $routeProvider.when('/menu/loader/:id', { template: require('./spreadsheet/template.html'), controller: 'MenuSpreadsheetController' });
  $routeProvider.when('/menu/set/:id', { template: require('./menu/set/template.html'), controller: 'MenuSetController' });
  $routeProvider.when('/menu/set/:id/promos', { template: require('./menu/promo/template.html'), controller: 'MenuPromoController' });
	$routeProvider.when('/menu/set/:id/category/:category_id', { template: require('./menu/category/template.html'), controller: 'MenuCategoryController' });
	$routeProvider.when('/menu/set/:id/item/:item_id', { template: require('./menu/item/template.html'), controller: 'MenuItemController' });
	$routeProvider.when('/menu/modifier', { template: require('./modifier/template.html'), controller: 'MenuModifiersController' });
	$routeProvider.when('/menu/modifier/:id', { template: require('./modifier/category/template.html'), controller: 'MenuModifierCategoryController' });
  $routeProvider.when('/menu/allergen', { template: require('./allergen/template.html'), controller: 'MenuAllergenController' });
}

routes.$inject = ['$routeProvider'];
