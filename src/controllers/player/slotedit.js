'use strict';

export default class SlotEditController {
  constructor($scope, close, DialogManager, LegacyApi, entity) {
    $scope.entity = entity;

    $scope.cancel = () => {
      DialogManager.closeDialog(close);
    };

    $scope.save = () => {
      DialogManager.closeDialog(close, $scope.entity);
    };

    $scope.clear = () => {
      DialogManager.closeDialog(close, {});
    };
  }
}

SlotEditController.$inject = ['$scope', 'close', 'DialogManager', 'LegacyApi', 'entity'];
