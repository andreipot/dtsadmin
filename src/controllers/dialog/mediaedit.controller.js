'use strict';

export default class MediaEditController {
  constructor($scope, $element, close, options) {
    var newMedia, oldMedia;

    $scope.options = options;
    oldMedia = options.media;

    $scope.onMedia = media => {
      newMedia = media;
    };

    $scope.save = () => {
      $element.modal('hide');
      close(newMedia, 500);
    };

    $scope.cancel = () => {
      $element.modal('hide');
      close(oldMedia, 500);
    };

    $scope.clear = () => {
      $element.modal('hide');
      close(null, 500);
    };
  }
}

MediaEditController.$inject = ['$scope', '$element', 'close', 'options'];
