const config = {
  "socket": false
};

if (ADMIN_DEBUG) {
  config.api = { path: '/backend-v4/' };
}
else {
  config.api = { spec: require('./swagger.json') };
}

module.exports = config;
