'use strict';

export default function routes($routeProvider) {
    $routeProvider.when('/media-gallery', { template: require('./mediagallery.html'), controller: 'MediaGalleryController' });
}

routes.$inject = ['$routeProvider'];
