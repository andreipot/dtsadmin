'use strict';

export default class PresentationController {
  constructor($scope, $timeout, $location, LegacyApi, DialogManager, ShellManager, UserModel) {
    var getData = function() {
      if (!UserModel.location) {
        return;
      }

      $scope.initialized = false;

      var job = DialogManager.startJob();

      var failure = function(e) {
        DialogManager.endJob(job);
        DialogManager.alert(e.message);
      };

      LegacyApi.advertisement.getPresentations().then(presentations => {
        DialogManager.endJob(job);
        $timeout(() => {
          $scope.initialized = true;
          $scope.presentations = presentations || [];
        });
      }, failure);
    };

    UserModel.locationChanged.add(getData);
    getData();

    $scope.$on('$destroy', () => UserModel.locationChanged.remove(getData));

    $scope.create = () => {
      DialogManager.showDialog(
        'PresentationEditController',
        require('./presentationedit.html'), {
          presentation: {}
        })
      .then(result => {
        if (result) {
          $timeout(() => $location.path(`/presentation/${result.token}`));
        }
      });
    };
  }
}

PresentationController.$inject = ['$scope', '$timeout', '$location', 'LegacyApi', 'DialogManager', 'ShellManager', 'UserModel'];
