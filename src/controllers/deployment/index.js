'use strict';

import angular from 'angular';
import angular_route from 'angular-route';

import routes from './routes';
import DeploymentController from './list';
import DeploymentDialogController from './dialog';

export default angular.module('TouchAdmin.controllers.deployment', [ angular_route ])
  .config(routes)
  .controller('DeploymentController', DeploymentController)
  .controller('DeploymentDialogController', DeploymentDialogController)
  .name;
