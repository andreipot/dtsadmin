'use strict';

export default function routes($routeProvider) {
  $routeProvider.when('/customization', { template: require('./customization.html'), controller: 'CustomizationController' });
}

routes.$inject = ['$routeProvider'];
