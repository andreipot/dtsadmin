'use strict';

import './style.less';

import binaryUrl from './binary.png';

export default
  ['DtsApi', 'DialogManager', 'ShellManager', '$timeout',
  (DtsApi, DialogManager, ShellManager, $timeout) => {
  return {
    restrict: 'E',
    replace: true,
    scope: {
      mediatype: '@',
      media: '=',
      mediatoken: '=',
      mediaurl: '@?',
      mediawidth: '@?',
      mediaheight: '@?',
      onmediaselected: '=',
      onmedia: '='
    },
    template: require('./template.html'),
    link: (scope, attrs) => {
      scope.onFileSelect = ($files) => {
        if (!$files || $files.length !== 1) {
          return;
        }

        var job = DialogManager.startJob();
        job.progress = 0;

        DtsApi.upload.put($files[0], progress => job.progress = progress)
        .then(uploadKey => DtsApi.api.request('media', 'mediaCreate', {
          body: { upload: uploadKey }
        }))
        .then(media => new Promise((resolve, reject) => {
          let i = 0;
          const limit = 120;

          function checkStatusLater() {
            $timeout(() => {
              i++;

              if (i === limit) {
                return reject();
              }

              DtsApi.api.request('media', 'mediaRead', { token: media.token })
              .then((media) => {
                if (media.status === 'ready') {
                  resolve({
                    token: media.token,
                    mime: media.mime
                  });
                }
                else if (media.status === 'error') {
                  reject();
                }
                else {
                  checkStatusLater();
                }
              })
              .catch(reject);
            }, 500);
          }

          checkStatusLater();
        }))
        .then(media => {
          DialogManager.endJob(job);

          $timeout(() => {
            scope.binary = media.mime.indexOf('application') !== -1;
            scope.media = media;
            scope.mediatoken = media.token;
            scope.loaded = false;

            if (scope.onmediaselected) {
              scope.onmediaselected(media.token);
            }

            if (scope.onmedia) {
              scope.onmedia(media);
            }
          });
        })
        .catch(e => {
          DialogManager.endJob(job);
          DialogManager.alert(`Upload error: ${JSON.stringify(e)}`);
        });
      };

      scope.showGallery = () => {
        DialogManager.showDialog(
          'MediaGalleryController',
          require('../../../controllers/dialog/mediagallery.html'), {
            options: {
              mediawidth: scope.mediawidth,
              mediaheight: scope.mediaheight,
              mediatype: scope.mediatype
            }
          })
        .then(media => {
          if (!media) {
            return;
          }

          $timeout(() => {
            scope.binary = media.mime.indexOf('application') !== -1;
            scope.media = media;
            scope.mediatoken = media ? media.token : null;
            scope.loaded = false;

            if (scope.onmediaselected) {
              scope.onmediaselected(scope.mediatoken);
            }

            if (scope.onmedia) {
              scope.onmedia(media);
            }
          });
        });
      };

      scope.onImageLoaded = () => {
        scope.loaded = true;
      };

      scope.onImageError = () => {
        scope.loaded = true;
      };

      if (!scope.mediatype) {
        scope.mediatype = 'image/*';
      }

      if (scope.mediatype.indexOf('application') !== -1) {
        scope.mediawidth = scope.mediaheight = undefined;
        scope.binary = true;
      }
      else {
        scope.binary = false;
      }

      function showPreview() {
        let newUrl;

        if (scope.media) {
          if (scope.binary) {
            scope.mediaurl = binaryUrl;
          }
          else {
            newUrl = ShellManager.getMediaUrl(scope.media.token, scope.mediawidth, scope.mediaheight);
          }
        }
        else if (scope.mediatoken) {
          if (scope.binary) {
            scope.mediaurl = binaryUrl;
          }
          else {
            newUrl = ShellManager.getMediaUrl(scope.mediatoken, scope.mediawidth, scope.mediaheight);
          }
        }
        else {
          scope.mediaurl = null;
        }

        if (newUrl !== scope.mediaurl) {
          scope.loaded = false;
          scope.mediaurl = newUrl;
        }
      }

      showPreview();
      scope.$watch('media', showPreview);

      function updateRatio() {
        if (isNaN(scope.mediawidth)) {
          scope.mediawidth = 300;
        }

        if (isNaN(scope.mediaheight)) {
          scope.mediaheight = 300;
        }

        const ratio = (scope.mediaheight / scope.mediawidth) || 1;

        scope.previewStyle = {
          'width': '300px',
          'height': `${Math.round(300 * ratio * 0.9)}px`,
          'line-height': `${Math.round(300 * ratio * 0.9)}px`
        };
      }

      scope.$watch('mediawidth', mediawidth => updateRatio());
      scope.$watch('mediaheight', mediawidth => updateRatio());
      updateRatio();
    }
  };
}]
