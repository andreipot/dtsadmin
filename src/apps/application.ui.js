import 'normalize.css';

import $ from 'jquery';
import 'jquery-ui';
import 'jquery-lazyload';

import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-timepicker';
import 'bootstrap-timepicker/css/bootstrap-timepicker.css';

import 'metismenu';
import 'metismenu/dist/metisMenu.css';

import 'startbootstrap-sb-admin-2/dist/js/sb-admin-2';
import 'startbootstrap-sb-admin-2/dist/css/sb-admin-2.css';

import 'datatables.net';
import 'datatables.net-bs';
import 'datatables.net-autofill';
import 'datatables.net-autofill-bs';

import 'font-awesome-webpack';

import 'awesomplete/awesomplete.css';

import '../less/index.less';

$('img.lazy').lazyload({});
