var Store = function() {
  this._storage = null;
};

Store.prototype.clear = function() {
  this._storage = null;
};

Store.prototype.read = function() {
  return this._storage;
};

Store.prototype.write = function(value) {
  this._storage = value;
};

export default Store;
