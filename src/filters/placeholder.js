const placeholder_160 = require('../less/images/placeholder_160.png');
const placeholder_470 = require('../less/images/placeholder_470.png');

const url_160 = new loader(placeholder_160),
      url_470 = new loader(placeholder_470);

export default [() => {
  return (file, width) => {
    switch (width) {
      case 160:
        return url_160.url;
      case 470:
        return url_470.url;
    }

    return null;
  };
}];

function loader(url) {
  var xhr = new XMLHttpRequest();
  xhr.open('GET', url, true);
  xhr.responseType = 'arraybuffer';

  const self = this;
  xhr.onload = function(e) {
    var arrayBufferView = new Uint8Array(this.response);
    var blob = new Blob([ arrayBufferView ], { type: 'image/png' });
    var urlCreator = window.URL || window.webkitURL;

    self.url = urlCreator.createObjectURL(blob);
  };

  xhr.send();
}
