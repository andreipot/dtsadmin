import Logger from './logger';

export default class ConsoleLogger extends Logger {
  debug(msg) {
    console.log(msg);
  }

  info(msg) {
    console.log(msg);
  }

  warn(msg, e) {
    console.warn(msg, e);
  }

  error(msg, e) {
    console.error(msg, e);
  }

  fatal(msg, e) {
    console.error(msg, e);
  }
}
