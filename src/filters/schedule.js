'use strict';

import moment from 'moment';
import later from 'later';
later.date.localTime();

function formatCron(txt) {
  const cron = later.parse.cron(txt);
  return moment(later.schedule(cron).next(1)).format('hh:mm A');
}

export default [() => {
  return (schedule) => {
    try {
      if (schedule) {
        let result = '';

        if (schedule.start) {
          result += 'From ' + formatCron(schedule.start);
        }

        if (schedule.end) {
          if (schedule.start) {
            result += ' to ';
          }
          else {
            result += 'Ends ';
          }

          result += formatCron(schedule.end);
        }

        if (result !== '') {
          return result;
        }
      }
    }
    catch (e) {
      console.error(e);
    }

    return 'Not defined';
  };
}];
