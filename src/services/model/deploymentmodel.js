import AbstractModel from './abstract';

export default class DeploymentModel extends AbstractModel {
  constructor(StorageProvider) {
    super(StorageProvider);

    this._defineProperty('deployments', undefined, {});
  }
};
