'use strict';

export default
  ['AccessManager', 'LocationManager', 'Logger',
  (AccessManager, LocationManager, Logger) => {
  return function() {
    Logger.debug(`CommandLogout started...`);

    return AccessManager.logout()
    .then(() => {
      LocationManager.load('login');
    });
  };
}];
