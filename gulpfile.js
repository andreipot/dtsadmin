'use strict';

var gulp = require('gulp');
var bump = require('gulp-bump');
var git = require('gulp-git');
var tag_version = require('gulp-tag-version');
var gulpCopy = require('gulp-copy');
var del = require('del');
var runSequence = require('run-sequence');
var s3 = require('gulp-s3');
var webpack = require('webpack-stream');
var pkg = require('./package.json');
var exec = require('child_process').exec;

var aws = {
  "key": process.env.AWS_ACCESS_KEY_ID,
  "secret": process.env.AWS_SECRET_ACCESS_KEY,
  "bucket": "admin.snap.menu",
  "region": "us-west-2"
};

var aws_staging = {
  "key": process.env.AWS_ACCESS_KEY_ID,
  "secret": process.env.AWS_SECRET_ACCESS_KEY,
  "bucket": "admin.managesnap.com",
  "region": "us-west-2"
};

gulp.task('clean', function() {
  return del(['./public']);
});

gulp.task('copy', function() {
  return gulp.src('./assets/*.*')
    .pipe(gulpCopy('./public', { prefix: 1 }));
});

gulp.task('build', function() {
  return gulp.src('src/')
    .pipe(webpack(require('./webpack.build.js')))
    .pipe(gulp.dest('public/'));
});

gulp.task('prepare', function() {
  return del(['./public/**/libs.js.map']);
});

gulp.task('upload', function() {
  return gulp.src('./public/**/*')
    .pipe(s3(aws, {
      uploadPath: '/'
    }));
});

gulp.task('upload-staging', function() {
  return gulp.src('./public/**/*')
    .pipe(s3(aws_staging, {
      uploadPath: '/'
    }));
});

gulp.task('cloudfront', function (cb) {
  exec('aws cloudfront create-invalidation --cli-input-json \'{"DistributionId":"E2YO866HLH4MNC","InvalidationBatch":{"Paths":{"Quantity":2,"Items":["/","/panel.html"]},"CallerReference":"' + pkg.version + '"}}\'', function (err, stdout, stderr) {
    console.log(stdout);
    console.log(stderr);
    cb(err);
  });
});

gulp.task('cloudfront-staging', function (cb) {
  exec('aws cloudfront create-invalidation --cli-input-json \'{"DistributionId":"E2SNS2QIZES639","InvalidationBatch":{"Paths":{"Quantity":2,"Items":["/","/panel.html"]},"CallerReference":"' + pkg.version + '"}}\'', function (err, stdout, stderr) {
    console.log(stdout);
    console.log(stderr);
    cb(err);
  });
});

gulp.task('tag', function() {
  return gulp.src('./package.json')
  .pipe(tag_version({ prefix: '' }))
  .pipe(gulp.dest('./'));
});

gulp.task('bump', function() {
  return gulp.src(['./bower.json', './package.json'])
  .pipe(bump({ type: 'version' }))
  .pipe(gulp.dest('./'));
});

gulp.task('commit', function(){
  return gulp.src(['./bower.json', './package.json'])
  .pipe(git.add())
  .pipe(git.commit('Build #' + pkg.version))
  .pipe(gulp.dest('./'));
});

gulp.task('stage', function(callback) {
  return runSequence(
    ['clean'],
    ['build', 'copy'],
    ['prepare'],
    ['upload-staging'],
    ['cloudfront-staging'],
    callback
  )
});

gulp.task('production', function(callback) {
  return runSequence(
    ['clean'],
    ['build', 'copy'],
    ['prepare'],
    ['upload'],
    ['cloudfront'],
    callback
  )
});

gulp.task('beta', function(callback) {
  return runSequence(
    ['stage'],
    ['tag'],
    ['bump'],
    ['commit'],
    callback
  )
});

gulp.task('release', function(callback) {
  return runSequence(
    ['production'],
    ['tag'],
    ['bump'],
    ['commit'],
    callback
  )
});
