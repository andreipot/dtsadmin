'use strict';

export default function() {
  return {
    restrict: 'C',
    replace: false,
    scope: {
      ondrag: '=',
      ondragcomplete: '='
    },
    link: function (scope, elem) {
      var list = $(elem);

      list.sortable({
        handle: '.draglist-handle',

        update: function() {
          $('li', list).each(function(index, elem) {
            var item = $(elem);
            var elementScope = item.scope();
            var newIndex = $(elem).index();

            if (scope.ondrag) {
              scope.ondrag(elementScope, newIndex);
            }
          });

          if (scope.ondragcomplete) {
            scope.ondragcomplete();
          }
        }
      });

      list.disableSelection();
    }
  };
};
