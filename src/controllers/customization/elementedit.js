'use strict';

export default class ElementEditController {
  constructor($scope, close, DialogManager, LegacyApi, element) {
		$scope.element = element;

		$scope.cancel = function() {
			DialogManager.closeDialog(close);
		};

		$scope.imageSelected = function(token) {
			$scope.element.image = token;
		};

		$scope.save = function() {
			var job = DialogManager.startJob();

			var task = element.token ?
				LegacyApi.customization.updateElement($scope.element, element.token) :
				LegacyApi.customization.saveElement($scope.element);

			task.then(function() {
				DialogManager.endJob(job);
				DialogManager.closeDialog(close, $scope.element);
			}, function(e) {
				DialogManager.endJob(job);
				DialogManager.alert(e.message);
			});
		};

		$scope.delete = function() {
			DialogManager.confirm('Are you sure that you want to delete the UI element?').then(function() {
				var job = DialogManager.startJob();

				LegacyApi.customization.deleteElement(element.token).then(function() {
					DialogManager.endJob(job);
					DialogManager.closeDialog(close);
				}, function(e) {
					DialogManager.endJob(job);
					DialogManager.alert(e.message);
				});
			});
		};
  }
}

ElementEditController.$inject = ['$scope', 'close', 'DialogManager', 'LegacyApi', 'element'];
