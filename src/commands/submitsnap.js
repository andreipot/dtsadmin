'use strict';

export default
  ['$location', '$timeout', 'DeploymentManager', 'Logger',
  ($location, $timeout, DeploymentManager, Logger) => {
  return function(digest) {
    digest = digest || {};

    return new Promise((resolve, reject) => {
      var timer, attempt = 0;
      function watch(deployment) {
        $timeout(() => check(deployment), 1000);
      }

      function check(deployment) {
        if (attempt > 10) {
          return reject('Unable to submit to SNAP');
        }

        attempt++;
        DeploymentManager.loadDeployments()
        .then(list => {
          let dep = list.filter(x => x.token === deployment.token)[0];

          if (!dep) {
            watch(deployment);
          }
          else {
            if (dep.status === 'error') {
              return reject('Unable to submit to SNAP');
            }
            else if (dep.status === 'ready') {
              let request = Object.assign({}, deployment);
              request.status = 'active';

              return submit(request);
            }

            watch(deployment);
          }
        })
        .catch(e => {
          Logger.warn(e);
          reject(e);
        });
      }

      function submit(deployment) {
        DeploymentManager.updateDeployment(deployment)
        .then(resolve)
        .catch(e => {
          Logger.warn(e);
          reject(e);
        });
      }

      DeploymentManager.createDeployment(digest)
      .then(watch)
      .catch(e => {
        Logger.warn(e);
        reject(e);
      });
    })
  };
}];
