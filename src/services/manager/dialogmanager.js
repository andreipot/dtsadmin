import AbstractManager from './abstract';
import signals from 'signals';

class DialogJob {
  constructor() {
    this._progress = NaN;
    this.onProgress = new signals.Signal();
  }

  get progress() {
    return this._progress;
  }

  set progress(value) {
    if (!(value >= 0 && value <= 1)) {
      value = NaN;
    }

    this._progress = value;
    this.onProgress.dispatch(value);
  }
}

export default class DialogManager extends AbstractManager {
  constructor(Logger) {
    super(Logger);

    this.alertRequested = new signals.Signal();
    this.notificationRequested = new signals.Signal();
    this.confirmRequested = new signals.Signal();
    this.dialogRequested = new signals.Signal();
    this.dialogClosed = new signals.Signal();
    this.jobStarted = new signals.Signal();
    this.jobEnded = new signals.Signal();
    this.jobProgressChanged = new signals.Signal();

    this._jobs = [];
  }

  get jobs() { return this._jobs; }

  alert(message, title, messageArgs, titleArgs) {
    return new Promise((resolve) => {
      this.alertRequested.dispatch({
        message: message,
        title: title,
        messageArgs: messageArgs,
        titleArgs: titleArgs
      }, resolve);
    });
  }

  notification(message, messageArgs) {
    return new Promise((resolve) => {
      this.notificationRequested.dispatch({
        message: message,
        messageArgs: messageArgs
      }, resolve);
    });
  }

  confirm(message, messageArgs) {
    return new Promise((resolve, reject) => {
      this.confirmRequested.dispatch({
        message: message,
        messageArgs: messageArgs
      }, resolve, reject);
    });
  }

  startJob() {
    let job = new DialogJob();
    job.onProgress.add(progress => this._onProgress(progress, job));

    this.jobs.push(job);

    if (this.jobs.length === 1) {
      this.jobStarted.dispatch();
    }

    return job;
  }

  endJob(job) {
    let i = this.jobs.indexOf(job);
    this.jobs.splice(i, 1);

    job.onProgress.removeAll();

    if (this.jobs.length === 0) {
      this.jobEnded.dispatch();
    }

    this.jobProgressChanged.dispatch(this.jobProgress);
  }

  get hasJobs() {
    return this.jobs.length > 0;
  }

  get jobProgress() {
    return this.jobs
      .filter(job => !isNaN(job.progress))
      .reduce((total, job, i, jobs) => {
        if (isNaN(total)) {
          total = 0;
        }

        return total + (job.progress / jobs.length);
      }, NaN);
  }

  showDialog(controller, template, input) {
    return new Promise((resolve) => {
      this.dialogRequested.dispatch({
        controller: controller,
        template: template,
        input: input
      }, resolve);
    });
  }

  closeDialog(handle, result) {
    this.dialogClosed.dispatch(handle, result);
  }

  _onProgress(progress, job) {
    this.jobProgressChanged.dispatch(this.jobProgress);
  }
};
