'use strict';

import angular from 'angular';

import ClientDetector from './api/clientdetector';
import DtsApi from 'dts-api-client';

import ConsoleLogger from './api/logger.console';
import CompositeLogger from './api/logger.composite';
import RaygunLogger from './api/logger.raygun';

import AccessManager from './manager/accessmanager';
import AssetManager from './manager/assetmanager';
import DeploymentManager from './manager/deploymentmanager';
import DialogManager from './manager/dialogmanager';
import LocationManager from './manager/locationmanager';
import MenuManager from './manager/menumanager';
import ShellManager from './manager/shellmanager';
import MediaManager from './manager/mediamanager';

import DeploymentModel from './model/deploymentmodel';
import HardwareModel from './model/hardwaremodel';
import MenuModel from './model/menumodel';
import ShellModel from './model/shellmodel';
import UserModel from './model/usermodel';
import MediaModel from './model/mediamodel';

import StorageProvider from './persistence/storageprovider';

export default angular.module('TouchAdmin.services', [])
  .factory('AccessManager', ['DtsApi', 'DtsApiManager', 'Applications', 'StorageProvider', 'Logger', (DtsApi, DtsApiManager, Applications, StorageProvider, Logger) => {
    return new AccessManager(DtsApi, DtsApiManager, Applications, StorageProvider, Logger);
  }])
  .factory('AssetManager', ['DtsApi', 'Logger', 'HardwareModel', 'MenuModel', (DtsApi, Logger, HardwareModel, MenuModel) => {
    return new AssetManager(DtsApi, Logger, HardwareModel, MenuModel);
  }])
  .factory('DtsApi', ['Hosts', (Hosts) => {
    return new DtsApi.Client(Hosts);
  }])
  .factory('DtsApiManager', ['Applications', (Applications) => {
    return new DtsApi.Manager([
      Applications.main
    ]);
  }])
  .factory('LegacyApi', ['DtsApi', (DtsApi) => {
    return DtsApi.legacy;
  }])
  .factory('ClientDetector', () => {
    return new ClientDetector();
  })
  .factory('DeploymentManager', ['DeploymentModel', 'DtsApi', 'Logger', 'UserModel', (DeploymentModel, DtsApi, Logger, UserModel) => {
    return new DeploymentManager(DeploymentModel, DtsApi, Logger, UserModel);
  }])
  .factory('DeploymentModel', ['StorageProvider', (StorageProvider) => {
    return new DeploymentModel(StorageProvider);
  }])
  .factory('DialogManager', ['Logger', (Logger) => {
    return new DialogManager(Logger);
  }])
  .factory('LocationManager', ['$location', 'StorageProvider', 'Logger', ($location, StorageProvider, Logger) => {
    return new LocationManager($location, StorageProvider, Logger);
  }])
  .factory('Logger', [() => {
    const loggers = [
      new ConsoleLogger()
    ];

    if (!ADMIN_DEBUG) {
      loggers.push(new RaygunLogger());
    }

    let logger = new CompositeLogger(loggers);

    window.onerror = (message, url, lineNumber) => {
      logger.fatal(message + '\n' + url + ':' + lineNumber);
      return true;
    };

    window.addEventListener('unhandledrejection', e => {
      e.preventDefault();
      logger.error(e.detail.reason, e);
    });

    return logger;
  }])
  .factory('$exceptionHandler', ['Logger', (Logger) => {
    return (exception, cause) => {
      Logger.fatal(exception + '\n' + cause + '\n' + exception.stack);
      throw exception;
    };
  }])
  .factory('MenuModel', ['StorageProvider', (StorageProvider) => {
    return new MenuModel(StorageProvider);
  }])
  .factory('MediaModel', ['StorageProvider', (StorageProvider) => {
    return new MediaModel(StorageProvider);
  }])
  .factory('MenuManager', ['DtsApi', 'Logger', 'MenuModel', (DtsApi, Logger, MenuModel) => {
    return new MenuManager(DtsApi, Logger, MenuModel);
  }])
  .factory('MediaManager', ['DtsApi', 'Logger', 'MediaModel', (DtsApi, Logger, MediaModel) => {
    return new MediaManager(DtsApi, Logger, MediaModel);
  }])
  .factory('ShellManager', ['DtsApi', 'ShellModel', 'Logger', (DtsApi, ShellModel, Logger) => {
    return new ShellManager(DtsApi, ShellModel, Logger);
  }])
  .factory('ShellModel', ['Logger', 'StorageProvider', (Logger, StorageProvider) => {
    return new ShellModel(Logger, StorageProvider);
  }])
  .factory('StorageProvider', [() =>  {
    return new StorageProvider();
  }])
  .factory('UserModel', ['StorageProvider', (StorageProvider) => {
    return new UserModel(StorageProvider);
  }])
  .factory('HardwareModel', ['DtsApi', 'LegacyApi', 'StorageProvider', (DtsApi, LegacyApi, StorageProvider) => {
    return new HardwareModel(DtsApi, LegacyApi, StorageProvider);
  }])
  .name;
