import './styles.less';

export default
  ['ShellManager', '$timeout',
  (ShellManager, $timeout) => {
  return {
    restrict: 'E',
    replace: true,
    scope: {
      deployment: '='
    },
    template: require('./template.html'),
    link: ($scope) => {
      function draw(deployment) {
        if (!deployment) {
          return;
        }

        $scope.backgrounds = deployment.theme.backgrounds;
        $scope.menus = deployment.menus;
      }

      $scope.$watch('deployment', draw);
      draw($scope.deployment);
    }
  };
}]
