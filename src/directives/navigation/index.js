'use strict';

import angular from 'angular';

import navigation from './navigation';
import breadcrumbs from './breadcrumbs';

export default angular.module('TouchAdmin.directives.navigation', [])
  .directive('adminnavigation', navigation)
  .directive('breadcrumbs', breadcrumbs)
  .name;
