'use strict';

export default
  ['DialogManager', 'ShellManager', '$timeout',
  (DialogManager, ShellManager, $timeout) => {
  return {
    restrict: 'E',
    replace: true,
    scope: {
      ngModel: '=',
      ngDisabled: '=?'
    },
    template: require('./template.html'),
    link: function(scope, attrs) {
      scope.edit = () => {
        DialogManager.showDialog(
          'ScheduleController',
          require('../../../controllers/dialog/schedule.html'), {
            schedule: scope.ngModel
          })
        .then(result => $timeout(() => {
          scope.ngModel = Boolean(result) ? result : null;
        }));
      };
    }
  };
}]
