'use strict';

export default class MainPanelController {
  constructor(AccessManager, CommandStartup, CommandLoadProfile, CommandLogout, DialogManager) {
    let job = DialogManager.startJob();

    CommandStartup(true)
    .then(result => result || CommandLoadProfile())
    .then(result => result || DialogManager.endJob(job))
    //.catch(e => DialogManager.alert('STARTUP_ERROR').then(CommandLogout));

    AccessManager.accessDenied.add(CommandLogout);
  }
}

MainPanelController.$inject = ['AccessManager', 'CommandStartup', 'CommandLoadProfile', 'CommandLogout', 'DialogManager'];
