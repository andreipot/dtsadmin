'use strict';

import angular from 'angular';
import angular_modal from 'angular-modal-service';
import angular_animate from 'angular-animate';
import angular_resource from 'angular-resource';
import angular_route from 'angular-route';
import angular_sanitize from 'angular-sanitize';
import angular_upload from 'ng-file-upload';

import './application.ui';
import application_config from './application.config';

import shared_commands from '../commands/';
import shared_filters from '../filters/';
import shared_directives from '../directives/';
import shared_services from '../services/';

import controllers_dialog from '../controllers/dialog/';

export default class Application {
  constructor(dependencies, options) {
    this.dependencies = [
      'angularModalService',
      angular_animate,
      angular_resource,
      angular_route,
      angular_sanitize,
    	angular_upload,
      application_config,
      shared_commands,
      shared_filters,
      shared_directives,
      shared_services,
      controllers_dialog
    ];

    if (dependencies) {
      this.dependencies = this.dependencies.concat(dependencies);
    }

    this.options = options || {};

    angular.module('app', this.dependencies)
      .constant('Options', this.options)
      .config(['$locationProvider', '$routeProvider', ($locationProvider, $routeProvider) => {
        $locationProvider.html5Mode(false);
        $routeProvider.otherwise({ redirectTo: '/' });
      }]);
  }
};
