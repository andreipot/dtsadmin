export default ['ShellManager', (ShellManager) => {
  return {
    restrict: 'A',
    link: (scope, element, attrs) => {
      element.bind('load', () => {
        const func = attrs['onimageload'];

        if (func) {
          scope.$apply(func);
        }
      });
      element.bind('error', () => {
        const func = attrs['onimageerror'];

        if (func) {
          scope.$apply(func);
        }
      });
    }
  };
}];
