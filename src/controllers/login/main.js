'use strict';

export default class MainLoginController {
  constructor(CommandStartup, DialogManager) {
    let job = DialogManager.startJob();

    CommandStartup()
    .then(() => DialogManager.endJob(job))
    .catch(e => DialogManager.endJob(job) && DialogManager.alert('STARTUP_ERROR'));
  }
}

MainLoginController.$inject = ['CommandStartup', 'DialogManager'];
