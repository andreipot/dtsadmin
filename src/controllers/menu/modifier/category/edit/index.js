import Controller from '../../../../controller';

export default class MenuModifierCategoryEditController extends Controller {
  constructor($scope, $timeout, DialogManager, Logger, MenuManager, UserModel, data, close) {
    super($scope, $timeout, DialogManager, Logger, UserModel);

    this.MenuManager = MenuManager;
    this.$scope.modifier = data.modifier;
    this.close = close;

    if (this.$scope.modifier.limit === 1) {
      this.$scope.modifier._mode = 'single';
    }
    else {
      this.$scope.modifier._mode = 'multiple';
    }

    if (isNaN(this.$scope.modifier.limit) || this.$scope.modifier.limit === 0) {
      this.$scope.modifier.limit = 0;
      this.$scope.modifier._nolimit = true;
    }

    this._defineMethod('save');
    this._defineMethod('cancel');
  }

  async save() {
    const job = this.DialogManager.startJob();

    try {
      let modifier = Object.assign({}, this.$scope.modifier);

      if (modifier._mode === 'single') {
        modifier.limit = 1;
      }
      else if (modifier._nolimit) {
        modifier.limit = 0;
      }

      delete modifier.items;
      delete modifier._mode;
      delete modifier._nolimit;

      if (this.$scope.modifier.token) {
        let result = await this.MenuManager.updateModifier(modifier);
        this.DialogManager.closeDialog(this.close, result);
      }
      else {
        let result = await this.MenuManager.createModifier(modifier);
        this.DialogManager.closeDialog(this.close, result);
      }
    }
    finally {
      this.DialogManager.endJob(job);
    }
  }

  async cancel() {
    this.DialogManager.closeDialog(this.close);
  }
}

MenuModifierCategoryEditController.$inject = ['$scope', '$timeout', 'DialogManager', 'Logger', 'MenuManager', 'UserModel', 'data', 'close'];
