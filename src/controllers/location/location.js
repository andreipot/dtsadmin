'use strict';

export default class LocationController {
  constructor($scope, $timeout, $location, CommandSwitchLocation, LegacyApi, DialogManager, ShellManager, UserModel) {
    var getData = function() {
      if (!UserModel.location) {
        return;
      }

      let job = DialogManager.startJob();

      LegacyApi.location.getOptions()
      .then(options => {
        DialogManager.endJob(job);
        $timeout(() => {
          options.screensaver_timeout = options.screensaver_timeout.toString();
          options.screensaver_interval = options.screensaver_interval.toString();
          $scope.options = options;
        });
      })
      .catch(e => {
        DialogManager.endJob(job);
        DialogManager.alert(e.message);
      });
    };

    UserModel.locationChanged.add(getData);
    getData();

    $scope.$on('$destroy', () => UserModel.locationChanged.remove(getData));

    $scope.save = function() {
      var job = DialogManager.startJob();

      LegacyApi.location.updateOptions($scope.options)
      .then(() => {
        DialogManager.endJob(job);
      })
      .catch(e => {
        DialogManager.endJob(job);
        DialogManager.alert(e.message);
      });
    };

    $scope.addLocation = function() {
      DialogManager.showDialog(
        'LocationEditController',
        require('./locationedit.html'), {
        location: {}
      })
      .then(token => {
        if (token) {
          $timeout(() => {
            let job = DialogManager.startJob();

            CommandSwitchLocation(token)
            .then(() => DialogManager.endJob(job))
            .catch(e => {
              DialogManager.endJob(job);
              DialogManager.alert(e.message);
            });
          });
        }
      });
    };

    $scope.editUsers = function() {
      $location.path('/users');
    };

    $scope.editStreams = function() {
      $location.path('/streams');
    };
  }
}

LocationController.$inject = ['$scope', '$timeout', '$location', 'CommandSwitchLocation', 'LegacyApi', 'DialogManager', 'ShellManager', 'UserModel'];
