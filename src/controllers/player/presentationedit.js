'use strict';

export default class PresentationEditController {
  constructor($scope, close, DialogManager, LegacyApi, presentation) {
    $scope.presentation = presentation;

    $scope.cancel = () => {
      DialogManager.closeDialog(close);
    };

    $scope.save = () => {
      let job = DialogManager.startJob();

      let task = presentation.token ?
        LegacyApi.advertisement.savePresentation(presentation.token, $scope.presentation) :
        LegacyApi.advertisement.createPresentation($scope.presentation);

      task.then(result => {
        if (result.token) {
          $scope.presentation.token = result.token;
        }

        DialogManager.endJob(job);
        DialogManager.closeDialog(close, $scope.presentation);
      })
      .catch(e => {
        DialogManager.endJob(job);
        DialogManager.alert(e.message);
      });
    };
  }
}

PresentationEditController.$inject = ['$scope', 'close', 'DialogManager', 'LegacyApi', 'presentation'];
