'use strict';

export default class LocationEditController {
  constructor($scope, close, DialogManager, LegacyApi, location) {
    $scope.location = location;

    $scope.cancel = function() {
      DialogManager.closeDialog(close);
    };

    $scope.save = function() {
      var job = DialogManager.startJob();

      LegacyApi.location.createLocation($scope.location).then(function(result) {
        if (result.token) {
          $scope.location.token = result.token;
        }

        DialogManager.endJob(job);
        DialogManager.closeDialog(close, result.token);
      }, function(e) {
        DialogManager.endJob(job);
        DialogManager.alert(e.message);
      });
    };
  }
}

LocationEditController.$inject = ['$scope', 'close', 'DialogManager', 'LegacyApi', 'location'];
