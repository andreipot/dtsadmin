'use strict';

export default function routes($routeProvider) {
  $routeProvider.when('/hardware', { template: require('./hardware.html'), controller: 'HardwareController' });
}

routes.$inject = ['$routeProvider'];
