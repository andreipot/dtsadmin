'use strict';

export default ['$timeout', '$location',
  ($timeout, $location) => {
  return {
    restrict: 'E',
    replace: true,
    scope: {
      ngModel: '='
    },
    template: require('./breadcrumbs.html'),
    link: ($scope, attrs) => {

      $scope.loadItem = (item) => {
        $location.path(item.url);
      };
    }
  }
}];
