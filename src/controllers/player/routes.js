'use strict';

export default function routes($routeProvider) {
  $routeProvider.when('/presentation', { template: require('./presentation.html'), controller: 'PresentationController' });
  $routeProvider.when('/presentation/:id', { template: require('./slide.html'), controller: 'SlideController' });
}

routes.$inject = ['$routeProvider'];
