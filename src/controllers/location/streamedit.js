'use strict';

export default class StreamEditController {
  constructor($scope, close, DialogManager, DtsApi, entity) {
    $scope.allKinds = [
      { id: 'order', name: 'Orders' },
      { id: 'assistance', name: 'Assistance' },
      { id: 'closeout', name: 'Closeout' }
    ];
    $scope.entity = entity;

    if (!$scope.entity.kinds) {
      $scope.entity.kinds = [];
    }

    $scope.toggleSelection = function(kind) {
      let i = $scope.entity.kinds.indexOf(kind.id);

      if (i > -1) {
        $scope.entity.kinds.splice(i, 1);
      }
      else {
        $scope.entity.kinds.push(kind.id);
      }
    };

    $scope.cancel = function() {
      DialogManager.closeDialog(close);
    };

    $scope.save = function() {
      let job = DialogManager.startJob();

      let task = entity.token ?
        DtsApi.api.request('waiter', 'waiterStreamUpdate', { token: entity.token, body: $scope.entity }) :
        DtsApi.api.request('waiter', 'waiterStreamCreate', { body: $scope.entity });

      task.then(result => {
        if (result.token) {
          $scope.entity.token = result.token;
        }

        DialogManager.endJob(job);
        DialogManager.closeDialog(close, $scope.entity);
      })
      .catch(e => {
        DialogManager.endJob(job);
        DialogManager.alert(e.message);
      });
    };

    $scope.delete = function() {
      DialogManager.confirm('Are you sure that you want to remove the location?')
      .then(() => {
        let job = DialogManager.startJob();

        DtsApi.api.request('waiter', 'waiterStreamDelete', { token: entity.token })
        .then(() => {
          DialogManager.endJob(job);
          DialogManager.closeDialog(close, $scope.entity);
        })
        .catch(e => {
          DialogManager.endJob(job);
          DialogManager.alert(e.message);
        });
      });
    };
  }
}

StreamEditController.$inject = ['$scope', 'close', 'DialogManager', 'DtsApi', 'entity'];
