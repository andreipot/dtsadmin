import Controller from '../../../../controller';

export default class MenuSetEditController extends Controller {
  constructor($scope, $timeout, DialogManager, DtsApi, Logger, MenuManager, UserModel, close, data) {
    super($scope, $timeout, DialogManager, Logger, UserModel);

    this.DtsApi = DtsApi;
    this.MenuManager = MenuManager;
    this.close = close;

    this.$scope.menu = data.menu;
    this.$scope.menu.type = this.$scope.menu.token ? (this.$scope.menu.type || '') : 'orderable';

    this.$scope.cancel = () => {
      this.DialogManager.closeDialog(close);
    };

    this._defineMethod('save');
  }

  async save() {
    const job = this.DialogManager.startJob();

    try {
      if (this.$scope.menu.token) {
        let menu = Object.assign({}, this.$scope.menu);
        delete menu.items;
        delete menu.categories;

        let result = await this.MenuManager.updateMenu(menu);
        this.DialogManager.closeDialog(this.close, result);
      }
      else {
        if (this.$scope.menu.type === '') {
          this.$scope.menu.type = undefined;
        }

        let result = await this.MenuManager.createMenu(this.$scope.menu);
        this.DialogManager.closeDialog(this.close, result);
      }
    }
    finally {
      this.DialogManager.endJob(job);
    }
  }
}

MenuSetEditController.$inject = ['$scope', '$timeout', 'DialogManager', 'DtsApi', 'Logger', 'MenuManager', 'UserModel', 'close', 'data'];
