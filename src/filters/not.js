'use strict';

export default function() {
  return (value) => {
    return !value;
  };
};
