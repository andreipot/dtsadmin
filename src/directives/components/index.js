'use strict';

import angular from 'angular';

import checklistmodel from './checklistmodel';
import draglist from './draglist';
import imageonload from './imageonload';
import mediaedit from './mediaedit';
import mediaselect from './mediaselect';
import schedule from './schedule';
import snapmenu from './snapmenu';
import tooltip from './tooltip';

export default angular.module('TouchAdmin.directives.components', [])
  .directive('checklistModel', checklistmodel)
  .directive('draglist', draglist)
  .directive('imageonload', imageonload)
  .directive('mediaedit', mediaedit)
  .directive('mediaselect', mediaselect)
  .directive('schedule', schedule)
  .directive('snapmenu', snapmenu)
  .directive('tooltip', tooltip)
  .name;
