'use strict';

export default class SurveyController {
  constructor($scope, $timeout, LegacyApi, DialogManager, ShellManager, UserModel) {
    var getData = function() {
      if (!UserModel.location) {
        return;
      }

      $scope.initialized = false;

      var job = DialogManager.startJob();

      var failure = function(e) {
        DialogManager.endJob(job);
        DialogManager.alert(e.message);
      };

      LegacyApi.advertisement.getSurveys().then(function(surveys) {
        DialogManager.endJob(job);
        $timeout(function() {
          $scope.initialized = true;

          $scope.survey = surveys.length > 0 ? surveys[0] : null;
          $scope.parent_survey = null;
        });
      }, failure);
    };

    UserModel.locationChanged.add(getData);
    getData();

    $scope.$on('$destroy', () => UserModel.locationChanged.remove(getData));

    $scope.createSurvey = function() {
      var job = DialogManager.startJob();
      var survey = {
        questions: []
      };

      LegacyApi.advertisement.createSurvey(survey).then(function(result) {
        $timeout(function() {
          survey.token = result.token;
          survey.location_token = UserModel.location.token;
          $scope.survey = survey;
        });
        DialogManager.endJob(job);
      }, function(e) {
        DialogManager.endJob(job);
        DialogManager.alert(e.message);
      });
    };

    $scope.deleteSurvey = function() {
      DialogManager.confirm('Are you sure that you want to remove the survey?').then(function() {
        var job = DialogManager.startJob();

        LegacyApi.advertisement.deleteSurvey($scope.survey.token).then(function() {
          $timeout(function() {
            $scope.survey = null;
          });
          DialogManager.endJob(job);
        }, function(e) {
          DialogManager.endJob(job);
          DialogManager.alert(e.message);
        });
      });
    };

    $scope.createQuestion = function() {
      DialogManager.showDialog(
        'SurveyQuestionEditController',
        require('./surveyquestionedit.html'), {
          question: {
            survey_token: $scope.survey.token
          }
        })
      .then(function(question) {
        if (question) {
          $timeout(function() {
            $scope.survey.questions.push(question);
          });
        }
      });
    };

    $scope.deleteQuestion = function(question) {
      DialogManager.confirm('Are you sure that you want to remove the question?').then(function() {
        var job = DialogManager.startJob();

        LegacyApi.advertisement.deleteSurveyQuestion($scope.survey.token, question.token).then(function() {
          $timeout(function() {
            $scope.survey.questions.splice($scope.survey.questions.indexOf(question), 1);
          });
          DialogManager.endJob(job);
        }, function(e) {
          DialogManager.endJob(job);
          DialogManager.alert(e.message);
        });
      });
    };

    $scope.onQuestionDrag = function(element, index) {
      var entity = element.question;
      entity.position = index;
    };

    $scope.onQuestionDragComplete = function() {
      var tokens = $scope.survey.questions.sort(function(a, b) {
        if (a.position > b.position) {
          return 1;
        }
        else if (a.position < b.position) {
          return -1;
        }

        return 0;
      }).map(function(entity) {
        return entity.token;
      });

      var job = DialogManager.startJob();

      LegacyApi.advertisement.saveSurveyQuestionOrder($scope.survey.token, tokens).then(function() {
        DialogManager.endJob(job);
      }, function(e) {
        DialogManager.endJob(job);
        DialogManager.alert(e.message);
      });
    };
  }
}

SurveyController.$inject = ['$scope', '$timeout', 'LegacyApi', 'DialogManager', 'ShellManager', 'UserModel'];
