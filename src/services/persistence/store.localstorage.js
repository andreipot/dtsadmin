import Store from './store';
import storejs from 'store2';

export default class LocalStorageStore extends Store {
  constructor(id, session) {
    super();

    this._id = id;
    this._session = Boolean(session);
  }

  clear() {
    this._store.remove(this._id);
  }

  read() {
    return this._store.get(this._id);
  }

  write(value) {
    this._store.set(this._id, value);
  }

  get _store() {
    return this._session ? storejs.session : storejs.local;
  }
};
