'use strict';

export default
  ['$location', 'AccessManager', 'AssetManager', 'DeploymentManager', 'MenuManager', 'UserModel',
  ($location, AccessManager, AssetManager, DeploymentManager, MenuManager, UserModel) => {
  return function(location_id) {
    return AccessManager.reauthenticate(location_id)
    .then(() => MenuManager.reset())
    .then(() => AssetManager.reset())
    .then(() => DeploymentManager.reset())
    .then(() => {
      $location.path('/');

      UserModel.location = UserModel.locations.filter(loc => loc.token === location_id)[0];
      AssetManager.updateCurrentLocation(UserModel.location);

      let profile = UserModel.profile;
      profile.current_location = location_id;
      UserModel.profile = profile;
    });
  };
}];
