'use strict';

import angular from 'angular';
import angular_route from 'angular-route';

import routes from './routes';
import HomeController from './home';
import MainPanelController from './main';

export default angular.module('TouchAdmin.controllers.home', [ angular_route ])
  .config(routes)
  .controller('HomeController', HomeController)
  .controller('MainPanelController', MainPanelController)
  .name;
