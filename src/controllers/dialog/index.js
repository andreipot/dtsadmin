'use strict';

import angular from 'angular';

import AlertController from './alert.controller';
import AllergensEditController from './allergensedit.controller';
import ConfirmController from './confirm.controller';
import MediaEditController from './mediaedit.controller';
import MediaGalleryController from './mediagallery.controller';
import ModifiersEditController from './modifiersedit.controller';
import ProgressController from './progress.controller';
import ScheduleController from './schedule.controller';

export default angular.module('TouchAdmin.controllers.dialog', [])
  .controller('AlertController', AlertController)
  .controller('AllergensEditController', AllergensEditController)
  .controller('ConfirmController', ConfirmController)
  .controller('MediaEditController', MediaEditController)
  .controller('MediaGalleryController', MediaGalleryController)
  .controller('ModifiersEditController', ModifiersEditController)
  .controller('ProgressController', ProgressController)
  .controller('ScheduleController', ScheduleController)
  .name;
