'use strict';

import angular from 'angular';
import angular_route from 'angular-route';

import routes from './routes';
import AdvertisementController from './advertisement';
import AdvertisementEditController from './advertisementedit';

export default angular.module('TouchAdmin.controllers.advertisement', [ angular_route ])
  .config(routes)
  .controller('AdvertisementController', AdvertisementController)
  .controller('AdvertisementEditController', AdvertisementEditController)
  .name;
