'use strict';

export default class UsersController {
  constructor($scope, $timeout, $location, LegacyApi, DialogManager, ShellManager, UserModel) {
    var getData = function() {
      if (!UserModel.location) {
        return;
      }

      var job = DialogManager.startJob();

      var failure = function(e) {
        DialogManager.endJob(job);
        DialogManager.alert(e.message);
      };

      LegacyApi.admin.getUsers().then(function(users) {
        DialogManager.endJob(job);
        $timeout(function() {
          $scope.users = users;
        });
      }, failure);
    };

    UserModel.locationChanged.add(getData);
    getData();

    $scope.$on('$destroy', () => UserModel.locationChanged.remove(getData));

    $scope.addUser = function() {
      DialogManager.showDialog(
        'UserEditController',
        require('./useredit.html'), {
        user: {}
      })
      .then(function(result) {
        if (result) {
          getData();
        }
      });
    };

    $scope.editUser = function(user) {
      DialogManager.showDialog(
        'UserEditController',
        require('./useredit.html'), {
          user: jQuery.extend({}, user)
      })
      .then(function(result) {
        if (result) {
          getData();
        }
      });
    };
  }
}

UsersController.$inject = ['$scope', '$timeout', '$location', 'LegacyApi', 'DialogManager', 'ShellManager', 'UserModel'];
