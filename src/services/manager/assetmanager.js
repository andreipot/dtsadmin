import AbstractManager from './abstract';

export default class AssetManager extends AbstractManager {
  constructor(DtsApi, Logger, HardwareModel, MenuModel) {
    super(Logger);

    this._DtsApi = DtsApi;
    this._HardwareModel = HardwareModel;
    this._MenuModel = MenuModel;
  }

  async initialize() {
    await super.initialize();

    this._MenuModel.currency = '$';
  }

  async reset() {
    this._HardwareModel.hardware = [];
    this._MenuModel.currency = '$';
  }

  updateCurrentLocation(location) {
    const locale = location && location.locale ? location.locale : 'en_US';

    let currency;

    switch (locale) {
      case 'ro_MD':
        currency = 'Lei';
        break;
      case 'zh_MO':
        currency = 'MOP$';
        break;
      default:
        currency = '$';
        break;
    }

    this._MenuModel.currency = currency;
  }

  async loadHardware() {
    const cache = this._HardwareModel.hardware;

    if (cache && cache.length) {
      return cache;
    }

    const hardware = await this._DtsApi.api.request('asset', 'assetHardwareList');

    this._HardwareModel.hardware = hardware;

    return hardware;
  }

  async createEntry(entry) {
    return (await this.createEntries([ entry ]))[0];
  }

  async createEntries(entries) {
    return await this._DtsApi.api.request('asset', 'assetEntryCreate', { body: entries.map(x => {
      return {
        name: x.name,
        price: x.price || 0
      }
    }) });
  }
};
